Bluesmith is OIT's 3D print job mangement and billing app.
Runs on PHP/MySQL in the CodeIgniter framework
Includes: JQuery, JQueryUI, three.js
Needs: localized user management, branding/UI wrapper, payment gateways
Limited support: Matt Gatner, matt.gatner@duke.edu

SETUP
Edit, import, and remove database.mysql to setup the database structure
Update models/Company.php for user import and other site-specific scripts
Update controllers/Payments.php to handle desired payment methods