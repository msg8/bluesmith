function selectMethod(methodId) {
	$(".materials").hide();
	$("#material_"+methodId).show();
}

function addUser() {
	var term = encodeURIComponent($("#search").val());

	if (term=="") {
		return true;
	}
	
	$.get(siteUrl+'/jobs/users/'+term, function( data ) {
		$("#users").append( data );
		$("#search").val("");
	});
	
	return false;
}