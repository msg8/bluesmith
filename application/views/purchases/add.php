<div id="bluesmith-wrapper">
	<h2>New purchase</h2>
	
	<p>
		A purchase is a direct charge for materials or other services that are not directly
		connected to a single job. This should only be used to bill one user directly for
		one item (or rollup), and only with manager approval.
	</p>
	
	<form name="purchase-add" action="<?=site_url('purchases/add_commit') ?>" method="post" >
		
		<p>
			Short description:<br />
			<input name="name" type="text" required />
		</p>
		
		<p>
			Charge amount:<br />
			$<input name="quote" type="text" required />
		</p>
		
		<p>
			Material being purchased:
			<select name="material_id">
<?php
foreach ($materials as $material_id):
?>
				<option value="<?=$material_id ?>"><?=$this->Material->name($material_id) ?></option>
<?php
endforeach;
?>
				<option value="28">Other</option>
			</select>
		</p>
		
		<p>Charge to: <input name="netid" type="text" placeholder="NetID" /></p>

		
		<p>Notes:</p>
		<textarea name="instructions"></textarea><br />
		
		<input name="submit" type="submit" value="Submit" class="faux-button" />
	</form>

</div>