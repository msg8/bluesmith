<div id="bluesmith-wrapper">

	<div id="controls">
		<select name="active" onchange="window.location = siteUrl+'/users/active/'+encodeURIComponent(this.value);">
			<option value="">All statuses</option>
			<option value="1" <?=($active==1)? "selected":"" ?>>Active</option>
			<option value="0" <?=(is_numeric($active) && $active==0)? "selected":"" ?>>Inactive</option>
		</select>
		<br />
		
		<select name="role" onchange="window.location = siteUrl+'/users/role/'+encodeURIComponent(this.value);">
			<option value="">All roles</option>
<?php
foreach (array("User","Proctor","Administrator") as $myrole):
?>
			<option value="<?=$myrole ?>" <?=($role==$myrole)? "selected":"" ?>><?=$myrole ?></option>
<?php
endforeach;
?>
		</select>
		<br />

		<select name="client" onchange="window.location = siteUrl+'/users/client/'+encodeURIComponent(this.value);">
			<option value="1" <?=($client==1)? "selected":"" ?>>Clients</option>
			<option value="0" <?=empty($client)? "selected":"" ?>>Everyone</option>
		</select>
		<br />

		<form name="users-search" action="<?=site_url('users/search') ?>" method="POST">
			<input name="search" type="search" value="<?=$search ?>" placeholder="search" autocomplete="off" onchange="document.forms['users-search'].submit();" />
		</form>
	</div>

<?php
if (empty($users)):
?>
	<div class="content-wrapper">
		<p>No users found</p>
	</div>
<?php
else:
	$order_icon = " <img src='".base_url()."assets/img/".$order.".png' alt='".$order."' />";
?>
	<nav id="pagination">
		<p><?=number_format($total_rows,0) ?> user<?=($total_rows==1)? "":"s" ?></p>
		<select id="items_per_page" name="items_per_page" onchange="repaginate(this.value);">
<?php
	foreach (array(10,25,50,100,500,1000) as $value):
?>
			<option value="<?=$value ?>" <?=($value==$items_per_page)? "selected":"" ?>><?=$value ?></option>
<?php
	endforeach;
?>
		</select>
		<?=$this->pagination->create_links(); ?>
	</nav>


	<table class="data-table">
		<thead>
			<tr>
				<th><a href="<?=site_url("users/sort/firstname") ?>">First name<?=($sort=="firstname")? $order_icon:"" ?></a></th>
				<th><a href="<?=site_url("users/sort/lastname") ?>">Last name<?=($sort=="lastname")? $order_icon:"" ?></a></th>
				<th><a href="<?=site_url("users/sort/netid") ?>">NetID<?=($sort=="netid")? $order_icon:"" ?></a></th>
				<th><a href="<?=site_url("users/sort/role") ?>">Role<?=($sort=="role")? $order_icon:"" ?></a></th>
				<th><a href="<?=site_url("users/sort/affiliation") ?>">Affiliation<?=($sort=="affiliation")? $order_icon:"" ?></a></th>
				<th><a href="<?=site_url("users/sort/association") ?>">Association<?=($sort=="association")? $order_icon:"" ?></a></th>
				<th>Jobs</th>
			</tr>
		</thead>
		<tbody>
<?php
	foreach ($users as $user):
		// provide a clickable entry for blank first names
		if (empty($user['firstname'])):
			$user['firstname'] = "<em>no name</em>";
		endif;
		$jobs = $this->User->jobs($user['id']);
?>
			<tr>
				<td><a href="<?=site_url("users/show/".$user['id']) ?>"><?=$user['firstname'] ?></a></td>
				<td><?=$user['lastname'] ?></td>
				<td><?=$user['netid'] ?></td>
				<td><?=ucfirst($user['role']) ?></td>
				<td><?=ucfirst($user['affiliation']) ?></td>
				<td><?=$user['association'] ?></td>
				<td>
<?php
if (!empty($jobs)):
?>
					<a href="<?=site_url('jobs/user/'.$user['id']) ?>"><?=count($jobs) ?></a>
<?php
endif;
?>
				</td>
			</tr>
<?php
	endforeach;
?>
		</tbody>
	</table>
<?php
endif;
?>

</div>
