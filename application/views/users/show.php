<div id="controls">

	<form name="users-search" action="<?=site_url('users/search') ?>" method="POST">
		<input name="search" type="search" placeholder="search" autocomplete="off" onchange="document.forms['users-search'].submit();" />
	</form>
	<br />

<?php
if (has_access("administration")):
?>
	<p>
		<a href="<?=site_url("users/sync/".$user['id']) ?>" class="faux-button">Force user sync</a>
<?php
	if (has_access("impersonate",$user['id'])):
?>
		<a href="<?=site_url("users/impersonate/".$user['id']) ?>" class="faux-button">Impersonate</a>
<?php
	endif;
?>
	</p>
<?php
endif;

?>
</div>

<div class="content-wrapper">
	<h1><?=$this->User->name($user['id']) ?></h1>

	<div class="content-block">
		<h3>User info</h3>
		<table class="keyval-table">
			<tr>
				<th>ID</th>
				<td><?=$user['id'] ?></td>
			</tr>
			<tr>
				<th>Name</th>
				<td><?=$user['firstname']." ".$user['lastname'] ?></td>
			</tr>
			<tr>
				<th>NetID</th>
				<td><?=$user['netid'] ?></td>
			</tr>
			<tr>
				<th>Primary email</th>
				<td><?=$user['email'] ?></td>
			</tr>
			<tr>
				<th>Card #</th>
				<td>
					<?=substr($user['cardid'],0,-1) ?>
					<em style="font-size:14px; margin-left:8px; color:#999;"><?=substr($user['cardid'],-1) ?></em>
				</td>
			</tr>
			<tr>
				<th>UniqueID</th>
				<td><?=$user['uniqueid'] ?></td>
			</tr>
			<tr>
				<th>Role</th>
				<td>
<?php
if (has_access("administration")):
?>
					<form name="user-role" action="<?=site_url('users/role_commit') ?>" method="POST">
						<select name="role" onchange="document.forms['user-role'].submit();">
<?php
	$roles = array("User","Proctor","Administrator");
	foreach ($roles as $role):
?>
							<option value="<?=$role ?>" <?=($role==$user['role'])? "selected":"" ?>><?=$role ?></option>
<?php
	endforeach;
?>
						</select>
						<input name="user_id" type="hidden" value="<?=$user['id'] ?>" />
					</form>
					<input name="priority" type="checkbox" value="1" onchange="togglePriority(<?=$user['id'] ?>,this.checked);" <?=($user['priority'])? "checked":"" ?> />
					Priority
<?php
else:
?>
					<?=ucfirst($user['role']) ?>
<?php
endif;
?>
				</td>
			</tr>
			<tr>
				<th>Status</th>
				<td><?=($user['active'])? "Active":"Disabled" ?></td>
			</tr>
			<tr>
				<th>User since</th>
				<td><?=timespan(strtotime($user['created_at']),time()) ?></td>
			</tr>
			<tr>
				<th>Last update</th>
				<td><?=date("g:ia, n/j/Y",strtotime($user['created_at'])) ?></td>
			</tr>
		</table>
	</div>

	<div class="content-block">
		<h3>Classification</h3>
		<table class="keyval-table">
			<tr>
				<th>LDAP Key</th>
				<td><?=$user['ldapkey'] ?></td>
			</tr>
			<tr>
				<th>Affiliation</th>
				<td><?=ucfirst($user['affiliation']) ?></td>
			</tr>
			<tr>
				<th>Association</th>
				<td><?=$user['association'] ?></td>
			</tr>
<?php
if (!empty($user['career'])):
?>
			<tr>
				<th>Career</th>
				<td><?=$user['career'] ?></td>
			</tr>
			<tr>
				<th>Program</th>
				<td><?=$user['program'] ?></td>
			</tr>
			<tr>
				<th>Plan</th>
				<td><?=$user['plan'] ?></td>
			</tr>
<?php
endif;

if (!empty($user['ou'])):
?>
			<tr>
				<th>Organization</th>
				<td><?=$user['ou'] ?></td>
			</tr>
<?php
endif;
?>
			<tr>
				<th>Emails</th>
				<td><?=implode(", ",$emails) ?></td>
			</tr>
			<tr>
				<th>Title</th>
				<td><?=$user['title'] ?></td>
			</tr>
			<tr>
				<th>Phone</th>
				<td><?=$user['phone'] ?></td>
			</tr>
			<tr>
				<th>Address</th>
				<td><?=nl2br($user['address']) ?></td>
			</tr>
		</table>
	</div>
	
	<div class="content-block">
		<h3>Bluechips</h3>
		<p><strong>Current balance:</strong> <?=number_format($user['balance'],2) ?></p>
<?php
if (has_access("administration")):
?>
		<p>Credit user</p>
		<form name="user-credit" action="<?=site_url('users/credit_commit') ?>" method="post">
			$<input name="amount" type="number" placeholder="amount" min="0" step="any" /><br />
			<input name="user_id" type="hidden" value="<?=$user['id'] ?>"  />
			<input name="submit" type="submit" value="Submit" class="faux-button" />
		</form>
<?php
endif;
?>
	</div>
	
</div>
