<div id="bluesmith-wrapper">
	<h2>Manage clients</h2>
	
	"<?=$this->Job->name($job['id']) ?>"
	<form name="job-clients" action="<?=site_url('jobs/clients_commit') ?>" method="post" onsubmit="return addUser();">

		<p>Clients <em style="margin-left:20px; color:#aaaaaa;">You must have at least one client signed up for email notices</em></p>
		<table id="users" class="data-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>NetID</th>
					<th>Emails?</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
<?php
foreach ($users as $user_id):
	$user = $this->User->get($user_id);
?>
				<tr>
					<td><?=$this->User->name($user['id']) ?></td>
					<td>
						<?=$user['netid'] ?>
						<input name="users[]" type="hidden" value="<?=$user['id'] ?>" />
					</td>
					<td style="text-align:center;"><input name="emails[]" type="checkbox" value="<?=$user['id'] ?>" checked /></td>
					<td style="text-align:center;" onclick="$(this).parent().remove();"><a href="#" onclick="return false;">Remove</a></td>
				</tr>
<?php
endforeach;
?>
			</tbody>
		</table>
		
		<p id="search-wrapper">
			Add client: <input id="search" name="search" type="search" placeholder="Search"
				autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
			<input name="search" type="submit" value="Search" />
		</p>
		
		<input name="job_id" type="hidden" value="<?=$job['id'] ?>" />
		<input name="submit" type="submit" value="Submit" class="faux-button" />
	</form>