<div id="bluesmith-wrapper">
	<h2><?=$this->Job->name($job['id']) ?><span class="note">Job #<?=$job['id'] ?></span></h2>
	
	<h5>Job details</h5>
	<div class="subsection">
		<table class="keyval-table">
			<tr>
				<th>ID</th>
				<td>#<?=$job['id'] ?></td>
			</tr>
			<tr>
				<th>Name</th>
				<td><?=$job['name'] ?></td>
			</tr>
			<tr>
				<th>Status</th>
				<td><?=ucfirst($job['status']) ?></td>
			</tr>
			<tr>
				<th>Quote</th>
				<td><?=is_numeric($job['quote'])? '$'.number_format($job['quote'],2):"<em>not quoted</em>" ?></td>
			</tr>
			<tr>
				<th>Print method</th>
				<td><?=$this->Method->name($method['id']) ?></td>
			</tr>
			<tr>
				<th>Material</th>
				<td><?=$this->Material->name($material['id']) ?></td>
			</tr>
			<tr>
				<th>Services</th>
				<td>
<?php
$services = array();
if ($job['premium'])
	$services[] = "Premium service";
if ($job['inspection'])
	$services[] = "Inspection scan";
if (empty($services)):
	echo "<em>No extras</em>".PHP_EOL;
else:
	echo implode(", ",$services).PHP_EOL;
endif;
?>
				</td>
			</tr>
			<tr>
				<th>Submitted by</th>
				<td><?=$this->User->name($job['created_by']) ?></td>
			</tr>
			<tr>
				<th>Submitted at</th>
				<td><?=date("n/j/Y, g:ia",strtotime($job['created_at'])) ?></td>
			</tr>
			<tr>
				<th>Last update</th>
				<td><?=timespan(strtotime($job['updated_at']),time()) ?></td>
			</tr>
		</table>
		<p>
			<strong>Additional instructions:</strong><br />
<?php
if (empty($job['instructions']))
	echo "<em>None provided</em>".PHP_EOL;
else
	echo nl2br($job['instructions']).PHP_EOL;
?>
		</p>
	</div>

	<h5>Client details</h5>
	<div class="subsection">
<?php
foreach ($users as $user_id):
	$user = $this->User->get($user_id);
?>
		<table class="keyval-table">
			<tr>
				<th>Name</th>
				<td><?=$this->User->name($user['id']) ?></td>
			</tr>
			<tr>
				<th>Affiliation</th>
				<td><?=$user['affiliation'] ?> <?=$user['association'] ?></td>
			</tr>
			<tr>
				<th>Organization</th>
				<td><?=empty($user['career'])? $user['ou']:$user['career'] ?></td>
			</tr>
			<tr>
				<th>Email</th>
				<td><?=$user['email'] ?></td>
			</tr>
			<tr>
				<th>Phone</th>
				<td><?=$user['phone'] ?></td>
			</tr>
		</table>
<?php
endforeach;
?>
	</div>

	<h5>Billing</h5>
	<div class="subsection">
	
		<h6>Charges</h6>
<?php
if (empty($charges)):
	echo "No charges recorded yet.".PHP_EOL;
elseif (count($charges)==1):
	$charge = $this->Charge->get($charges[0]);
?>
		<p><?=$charge['name'] ?>: $<?=number_format($charge['amount'],2) ?></p>
<?php
else:
?>
		<table class="keyval-table">
<?php
	foreach ($charges as $charge_id):
		$charge = $this->Charge->get($charge_id);
?>
				<tr>
					<th><?=$charge['name'] ?></td>
					<td>$<?=number_format($charge['amount'],2) ?></td>
				</tr>
<?php
	endforeach;
?>
		</table>
<?php
endif;
?>
		<h6>Payments</h6>
<?php
if (empty($payments)):
	echo "No payments recorded yet.".PHP_EOL;
elseif (count($payments)==1):
	$payment = $this->Payment->get($payments[0]);
?>
	<?=date("g:ia, n/j/Y",strtotime($payment['created_at'])) ?> - <?=$this->User->name($payment['created_by']) ?> paid $<?=number_format($payment['amount'],2) ?> by <?=$payment['type'] ?>, reference #<?=$payment['reference_id'] ?>
<?php
else:
?>
		<table class="data-table">
			<thead>
				<tr>
					<th>Type</th>
					<th>Ref.#</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Submitted</th>
				</tr>
<?php
	foreach ($payments as $payment_id):
		$payment = $this->Payment->get($payment_id);
?>
				<tr>
					<td><?=$payment['type'] ?></td>
					<td><?=$payment['reference_id'] ?></td>
					<td>$<?=number_format($payment['amount'],2) ?></td>
					<td><?=empty($payment['processed_at'])? "Pending":"Received" ?></td>
					<td><?=date("n/j/Y, g:ia",strtotime($payment['created_at'])) ?></td>
				</tr>
<?php
	endforeach;
?>
		</table>
<?php
endif;
?>
	</div>
</div>