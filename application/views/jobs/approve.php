<div id="bluesmith-wrapper">
	<h2>Job quote</h2>
		
	<h5>Charges</h5>
	<div class="subsection">
<?php
if (empty($charges)):
	echo "No charges recorded yet.".PHP_EOL;
else:
?>
		<table class="data-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Amount</th>
					<th>Added</th>
				</tr>
			</thead>
<?php
	$shaded = "";
	foreach ($charges as $charge_id):
		$charge = $this->Charge->get($charge_id);
?>
				<tr class="<?=$shaded ?>">
					<td><?=$charge['name'] ?></td>
					<td style="text-align:right;">$<?=number_format($charge['amount'],2) ?></td>
					<td><?=date("n/j/Y, g:ia",strtotime($charge['created_at'])) ?></td>
				</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
		</table>
		<p style="font-weight:bold;">TOTAL: $<?=number_format($job['quote'],2) ?></p>

		<form name="job-approve" action="<?=site_url("jobs/approve_commit") ?>" method="post">
			<p><?=$this->Section->content("Approve") ?></p>
			<p><input name="agree" type="checkbox" value="1" /> I agree</p>
			<input name="job_id" type="hidden" value="<?=$job['id'] ?>" />
			<input name="submit" type="submit" value="Approve" class="faux-button" />
		</form>
<?php
endif;
?>
	</div>
	
	<h5>Job details</h5>
	<div class="subsection">
		<table class="keyval-table">
			<tr>
				<th>ID</th>
				<td>#<?=$job['id'] ?></td>
			</tr>
			<tr>
				<th>Name</th>
				<td><?=$job['name'] ?></td>
			</tr>
			<tr>
				<th>Status</th>
				<td><?=ucfirst($job['status']) ?></td>
			</tr>
			<tr>
				<th>Quote</th>
				<td><?=is_numeric($job['quote'])? '$'.number_format($job['quote'],2):"<em>not quoted</em>" ?></td>
			</tr>
			<tr>
				<th>Print method</th>
				<td><?=$this->Material->method($job['material_id']) ?></td>
			</tr>
			<tr>
				<th>Material</th>
				<td><?=$this->Material->name($job['material_id']) ?></td>
			</tr>
			<tr>
				<th>Services</th>
				<td>
<?php
$services = array();
if ($job['premium'])
	$services[] = "Premium service";
if ($job['inspection'])
	$services[] = "Inspection scan";
if (empty($services)):
	echo "<em>No extras</em>".PHP_EOL;
else:
	echo implode(", ",$services).PHP_EOL;
endif;
?>
				</td>
			</tr>
			<tr>
				<th>Submitted by</th>
				<td><?=$this->User->name($job['created_by']) ?></td>
			</tr>
			<tr>
				<th>Submitted at</th>
				<td><?=date("n/j/Y, g:ia",strtotime($job['created_at'])) ?></td>
			</tr>
			<tr>
				<th>Last update</th>
				<td><?=timespan(strtotime($job['updated_at']),time()) ?></td>
			</tr>
		</table>
	</div>
</div>
