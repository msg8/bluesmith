<div id="bluesmith-wrapper">
	<h2>Audit log</h2>
	
	<p><?=$this->Job->name($job['id']) ?></p>

	<table class="data-table">
		<thead>
			<tr>
				<th>Time</th>
				<th>User</th>
				<th>Event</th>
			</tr>
		</thead>
		<tbody>
<?php
$shaded = "";
foreach ($times as $i=>$time):
	$user_id = $users[$i];
	$event = $events[$i];
?>
			<tr class="<?=$shaded ?>">
				<td><?=date("g:ia, n/j/Y",$time) ?></td>
				<td><?=$this->User->name($user_id) ?></td>
				<td><?=$event ?></td>
			</tr>
<?php
	$shaded = ($shaded)? "":"shaded";
endforeach;
?>
		</tbody>
	</table>
</div>