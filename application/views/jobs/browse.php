<div id="bluesmith-wrapper">

	<div id="controls">
<?php
if (has_access("proctor")):
?>
		<select name="user_id" onchange="window.location = siteUrl+'/jobs/user/'+encodeURIComponent(this.value);">
			<option value="">All clients</option>
<?php
	foreach ($this->User->all() as $myuser):
?>
			<option value="<?=$myuser ?>" <?=($myuser==$user_id)? "selected":"" ?>><?=$this->User->name($myuser) ?></option>
<?php
	endforeach;
?>
		</select>
		<br />
<?php
endif;
?>
		
		<select name="status" onchange="window.location = siteUrl+'/jobs/status/'+encodeURIComponent(this.value);">
			<option value="">All statuses</option>
<?php
	foreach (array("Active","Archived","Cancelled") as $mystatus):
?>
			<option value="<?=$mystatus ?>" <?=($mystatus==$status)? "selected":"" ?>><?=$mystatus ?></option>
<?php
	endforeach;
?>
		</select>
		<br />

		<form name="jobs-search" action="<?=site_url('jobs/search') ?>" method="post">
			<input name="search" type="search" value="<?=$search ?>" placeholder="search" autocomplete="off" onchange="document.forms['jobs-search'].submit();" />
		</form>
	</div>

<?php
if (empty($jobs)):
?>
	<p>No jobs found</p>
	<p><a href="<?=site_url("jobs/add") ?>" class="button">Submit a job</a></p>
<?php
else:
	$order_icon = " <img src='".base_url()."assets/img/".$order.".png' alt='".$order."' />";
	$s = ($total_rows==1)? "":"s";
?>
	<nav id="pagination">
		<p><?=number_format($total_rows,0) ?> job<?=$s ?></p>
		<select id="items_per_page" name="items_per_page" onchange="repaginate(this.value);">
<?php
	foreach (array(10,25,50,100,500,1000) as $value):
?>
			<option value="<?=$value ?>" <?=($value==$items_per_page)? "selected":"" ?>><?=$value ?></option>
<?php
	endforeach;
?>
		</select>
		<?=$this->pagination->create_links(); ?>
	</nav>

	<table class="data-table">
		<thead>
			<tr>
				<th><a href="<?=site_url("jobs/sort/name") ?>">Name<?=($sort=="name")? $order_icon:"" ?></a></th>
				<th>Method</th>
				<th><a href="<?=site_url("jobs/sort/stage") ?>">Stage<?=($sort=="stage")? $order_icon:"" ?></a></th>
				<th><a href="<?=site_url("jobs/sort/created_by") ?>">Client<?=($sort=="created_by")? $order_icon:"" ?></a></th>
				<th><a href="<?=site_url("jobs/sort/created_at") ?>">Submitted<?=($sort=="created_at")? $order_icon:"" ?></a></th>
				<th><a href="<?=site_url("jobs/sort/updated_at") ?>">Updated<?=($sort=="updated_at")? $order_icon:"" ?></a></th>
			</tr>
		</thead>
		<tbody>
<?php
	$shaded = "";
	foreach ($jobs as $job):
		$material = $this->Material->get($job['material_id']);
?>
			<tr class="<?=$shaded ?>">
				<td><a href="<?=site_url('jobs/show/'.$job['id']) ?>"><?=$this->Job->name($job['id']) ?></a></td>
				<td><?=$this->Method->name($material['method_id']) ?> - <?=$this->Material->name($material['id']) ?></td>
				<td><?=stage_display($job['stage']) ?></td>
				<td><?=$this->User->name($job['created_by']) ?></td>
				<td><?=date("g:ia, n/j/Y",strtotime($job['created_at'])) ?></td>
				<td><?=empty($job['updated_at'])? "":timespan(strtotime($job['updated_at']),time()) ?></td>
			</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
		</tbody>
	</table>
<?php
endif;
?>
</div>