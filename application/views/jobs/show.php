<div id="bluesmith-wrapper">
	<h2><?=$this->Job->name($job['id']) ?><span class="note">Job #<?=$job['id'] ?></span></h2>
<?php
if (has_access("administration")):
?>
	<p><a href="<?=site_url('jobs/audit/'.$job['id']) ?>">Audit log</a></p>
<?php
endif;

if ($job['status']=="Active"):
?>
	<p><a href="<?=site_url('jobs/cancel/'.$job['id']) ?>" 
		onclick="return confirm('This will halt progress on your job and remove it from the work queue. Are you sure?');"
		class="faux-button">Cancel job</a></p>
	<h5>Job progress</h5>
	<table id="process" class="data-table">
		<tr>
			<td>Complete</td>
			<td>Client submits job files and print options</td>
			<td>
<?php
	if ($job['stage']<6):
		if (empty($job['quoted_at'])):
?>
				<a href="<?=site_url('jobs/edit/'.$job['id']) ?>">Edit settings</a>
<?php
		else:
?>
				<a href="<?=site_url('jobs/edit/'.$job['id']) ?>"
					onclick="return confirm('Changing job settings will require new estimate. Are you sure?');">Edit settings</a>
<?php
		endif;
	endif;
?>
			</td>
		</tr>
		<tr>
			<td>
<?php
	if ($job['stage']==2): echo "<strong>In progress</strong>";
	elseif ($job['stage']>2): echo "Complete";
	endif;
?>
			</td>
			<td>Staff assesses job to determine cost</td>
			<td>
<?php
	if (has_access("proctor") && $job['stage']<6):
		if (empty($charges)):
?>
				<a href="<?=site_url('charges/job/'.$job['id']) ?>">Add charges</a>
<?php
		else:
?>
				<a href="<?=site_url('charges/job/'.$job['id']) ?>">Edit charges</a>
<?php
		endif;
	endif;
?>
			</td>
		</tr>
		<tr>
			<td>
<?php
	if ($job['stage']==3): echo "<strong>In progress</strong>";
	elseif ($job['stage']>3): echo "Complete";
	endif;
?>
			<td class="<?=($job['stage']<3)? "pending":"" ?>">Staff issues an estimate</td>
			<td>
<?php
	if (has_access("proctor") && $job['stage']<6):
		if (!empty($charges)):
			if (empty($job['quoted_at'])):
?>
				<a href="<?=site_url('jobs/quote/'.$job['id']) ?>">
					Send quote ($<?=number_format($this->Job->cost($job['id']),2) ?>)
				</a>
<?php
			else:
?>
				<a href="<?=site_url('jobs/quote/'.$job['id']) ?>">
					Resend quote ($<?=number_format($this->Job->cost($job['id']),2) ?>)
				</a>
<?php
			endif;
		endif;
	endif;
?>
			</td>
		</tr>
		<tr>
			<td>
<?php
	if ($job['stage']==4): echo "<strong>In progress</strong>";
	elseif ($job['stage']>4): echo "Complete";
	endif;
?>
			</td>
			<td class="<?=($job['stage']<4)? "pending":"" ?>">Client reviews and approves the estimate</td>
			<td>
<?php
	if ($job['stage']>3 && $job['stage']<6):
		if (empty($job['approved_at'])):
?>
				<a href="<?=site_url('jobs/approve/'.$job['id']) ?>">Review estimate</a>
<?php
		else:
?>
				<a href="<?=site_url('jobs/rescind/'.$job['id']) ?>">Rescind approval</a>
<?php
		endif;
	endif;
?>
			</td>
		</tr>
		<tr>
			<td>
<?php
	if ($job['stage']==5): echo "<strong>In progress</strong>";
	elseif ($job['stage']>5): echo "Complete";
	endif;
?>
			</td>
			<td class="<?=($job['stage']<5)? "pending":"" ?>">Staff prints and post-processes the files</td>
			<td>
<?php
	if (has_access("proctor")):
		if (!empty($job['approved_at'])):
			if (empty($job['printed_at'])):
?>
				<a href="<?=site_url('jobs/printed/'.$job['id']) ?>">Mark printed</a>
<?php
			else:
?>
				<a href="<?=site_url('jobs/printed/'.$job['id']) ?>">Mark in progress</a>
<?php
			endif;
		endif;
	endif;
?>
			</td>
		</tr>
		<tr>
			<td>
<?php
	if ($job['stage']==6): echo "<strong>In progress</strong>";
	elseif ($job['stage']>6): echo "Complete";
	endif;
?>
			</td>
			<td class="<?=($job['stage']<6)? "pending":"" ?>">Client pays for services</td>
			<td>
<?php
	if ($job['stage']>5 && $job['stage']<8):
		if (empty($payments)):
?>
				<a href="<?=site_url('jobs/pay/'.$job['id']) ?>">Pay now</a>
<?php
		else:
?>
				<a href="<?=site_url('jobs/pay/'.$job['id']) ?>">Update payment</a>
<?php
		endif;
	endif;
?>
			</td>
		</tr>
		<tr>
			<td>
<?php
	if ($job['stage']==7): echo "<strong>In progress</strong>";
	elseif ($job['stage']>7): echo "Complete";
	endif;
?>
			</td>
			<td class="<?=($job['stage']<7)? "pending":"" ?>">Client picks up printed items</td>
			<td>
<?php
	if ($job['stage']>6 && empty($job['received_at'])):
?>
				<a href="<?=site_url('jobs/receive/'.$job['id']) ?>">Mark received</a>
<?php
	endif;
?>
			</td>
		</tr>
	</table>
<?php
elseif ($job['status']=="Archived"):
?>
	<p>
		This job has been archived.<br />
		<a href="<?=site_url('jobs/receive/'.$job['id']) ?>">Return to active</a>
	</p>
<?php
elseif ($job['status']=="Cancelled"):
?>
	<p>
		This job has been cancelled.<br />
		<a href="<?=site_url('jobs/cancel/'.$job['id']) ?>">Return to active</a>
	</p>
<?php
endif;
?>
	
	<h5>Comments</h5>
	<div class="subsection">
		<p><a href="#" onclick="$('#comments-add').toggle(200); return false;" class="faux-button">Add comment</a></p>
		<form id="comments-add" name="comments-add" action="<?=site_url("comments/add_commit") ?>" method="post" style="display:none;">
			<textarea name="content" placeholder="Comment..."></textarea><br />
<?php
if (has_access("proctor")):
?>
			<input name="internal" type="checkbox" value=1 /> hide from client<br />
<?php
endif;
?>
			<input name="job_id" type="hidden" value="<?=$job['id'] ?>" />
			<input name="submit" type="submit" value="Submit" />
		</form>
<?php
if (!empty($comments)):
	$shaded = "shaded";
	foreach ($comments as $comment_id):
		$comment = $this->Comment->get($comment_id);
?>
		<p class="" style="margin:0; padding: 2px 4px; border-bottom: 1px solid #eeeeee;">
			<?=($comment['internal'])? "<strong>[hidden]</strong>":"" ?>
			<?=nl2br($comment['content']) ?>
			<span class="note">-<?=$this->User->name($comment['created_by']) ?>,
			<?=date("n/j/Y, g:ia",strtotime($comment['created_at'])) ?></span>
		</p>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
else:
	echo "<p>No comments</p>".PHP_EOL;
endif;
?>
	</div>
	
	<h5>Job details</h5>
	<div class="subsection">
		<table class="keyval-table">
			<tr>
				<th>ID</th>
				<td>#<?=$job['id'] ?></td>
			</tr>
			<tr>
				<th>Name</th>
				<td><?=$job['name'] ?></td>
			</tr>
			<tr>
				<th>Status</th>
				<td><?=ucfirst($job['status']) ?></td>
			</tr>
			<tr>
				<th>Quote</th>
				<td><?=is_numeric($job['quote'])? '$'.number_format($job['quote'],2):"<em>not quoted</em>" ?></td>
			</tr>
			<tr>
				<th>Print method</th>
				<td><?=$this->Method->name($method['id']) ?></td>
			</tr>
			<tr>
				<th>Material</th>
				<td><?=$this->Material->name($material['id']) ?></td>
			</tr>
			<tr>
				<th>Services</th>
				<td>
<?php
$services = array();
if ($job['premium'])
	$services[] = "Premium service";
if ($job['inspection'])
	$services[] = "Inspection scan";
if (empty($services)):
	echo "<em>No extras</em>".PHP_EOL;
else:
	echo implode(", ",$services).PHP_EOL;
endif;
?>
				</td>
			</tr>
			<tr>
				<th>Submitted by</th>
				<td><?=$this->User->name($job['created_by']) ?></td>
			</tr>
			<tr>
				<th>Submitted at</th>
				<td><?=date("n/j/Y, g:ia",strtotime($job['created_at'])) ?></td>
			</tr>
			<tr>
				<th>Last update</th>
				<td><?=timespan(strtotime($job['updated_at']),time()) ?></td>
			</tr>
		</table>
		<p>
			<strong>Additional instructions:</strong><br />
<?php
if (empty($job['instructions']))
	echo "<em>None provided</em>".PHP_EOL;
else
	echo nl2br($job['instructions']).PHP_EOL;
?>
		</p>
	</div>

	<h5>Client details</h5>
	<p><a href="<?=site_url('jobs/clients/'.$job['id']) ?>" class="faux-button">Manage clients</a></p>
	
	<div class="subsection">
<?php
foreach ($users as $user_id):
	$user = $this->User->get($user_id);
?>
		<table class="keyval-table">
			<tr>
				<th>Name</th>
				<td><?=$this->User->name($user['id']) ?></td>
			</tr>
			<tr>
				<th>Affiliation</th>
				<td><?=$user['affiliation'] ?> <?=$user['association'] ?></td>
			</tr>
			<tr>
				<th>Organization</th>
				<td><?=empty($user['career'])? $user['ou']:$user['career'] ?></td>
			</tr>
			<tr>
				<th>Email</th>
				<td><?=$user['email'] ?></td>
			</tr>
			<tr>
				<th>Phone</th>
				<td><?=$user['phone'] ?></td>
			</tr>
		</table>
<?php
	if (has_access("proctor")):
?>
		<p><a href="<?=site_url('jobs/user/'.$user['id']) ?>" class="faux-button">Show all user jobs</a></p>
<?php
	endif;
endforeach;
?>
	</div>
	
	<h5>Files</h5>

<?php
if (empty($files)):
	echo "No files associated with this job.".PHP_EOL;
else:
?>
	<table class="data-table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Filename</th>
				<th>Type</th>
				<th>Size</th>
				<th>Created</th>
				<th><a href="<?=site_url('jobs/download/'.$job['id']) ?>" target="_blank">Download all</a></th>
			</tr>
		</thead>
		<tbody>
<?php
	$shaded = "";
	foreach ($files as $file_id):
		$file = $this->File->get($file_id);
		
		if ($file['filesize']>10000):
			$size = round($file['filesize']/1024,1);
			$unit = "mb";
		else:
			$size = $file['filesize'];
			$unit = "kb";
		endif;
?>
			<tr class="<?=$shaded ?>">
				<td><a href="<?=site_url('files/preview/'.$file['id']) ?>" target="_blank"><?=$file['name'] ?></a></td>
				<td><?=$file['clientname'] ?></td>
				<td><?=$file['filetype'] ?></td>
				<td><?=$size ?> <?=$unit ?></td>
				<td><?=date("n/j/Y, g:ia",strtotime($file['created_at'])) ?></td>
				<td><a href="<?=site_url("files/download/".$file['id']) ?>">Download</a></td>
			</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
		</tbody>
	</table>
<?php
endif;
?>

	<h5>Billing</h5>
	<div class="subsection">
	
		<h6>Charges</h6>
<?php
if (empty($charges)):
	echo "No charges recorded yet.".PHP_EOL;
elseif (count($charges)==1):
	$charge = $this->Charge->get($charges[0]);
?>
		<p><?=$charge['name'] ?>: $<?=number_format($charge['amount'],2) ?></p>
<?php
else:
?>
		<table class="keyval-table">
<?php
	foreach ($charges as $charge_id):
		$charge = $this->Charge->get($charge_id);
?>
				<tr>
					<th><?=$charge['name'] ?></td>
					<td>$<?=number_format($charge['amount'],2) ?></td>
				</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
		</table>
<?php
endif;
?>
		<h6>Payments</h6>
<?php
if (empty($payments)):
	echo "No payments recorded yet.".PHP_EOL;
elseif (count($payments)==1):
	$payment = $this->Payment->get($payments[0]);
?>
	<?=date("g:ia, n/j/Y",strtotime($payment['created_at'])) ?> - <?=$this->User->name($payment['created_by']) ?> paid $<?=number_format($payment['amount'],2) ?> by <?=$payment['type'] ?>, reference #<?=$payment['reference_id'] ?>
<?php
else:
?>
		<table class="data-table">
			<thead>
				<tr>
					<th>Type</th>
					<th>Ref.#</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Submitted</th>
				</tr>
<?php
	$shaded = "";
	foreach ($payments as $payment_id):
		$payment = $this->Payment->get($payment_id);
?>
				<tr class="<?=$shaded ?>">
					<td><?=$payment['type'] ?></td>
					<td><?=$payment['reference_id'] ?></td>
					<td>$<?=number_format($payment['amount'],2) ?></td>
					<td><?=empty($payment['processed_at'])? "Pending":"Received" ?></td>
					<td><?=date("n/j/Y, g:ia",strtotime($payment['created_at'])) ?></td>
				</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
		</table>
<?php
endif;
?>
	</div>
</div>