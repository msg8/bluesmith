<div id="bluesmith-wrapper">
	<h2>Update job</h2>

	<p><a href="<?=site_url('files') ?>" class="faux-button">Manage files</a></p>
<?php
if (empty($myfiles)):
?>
	<p>You don&rsquo;t have any files. Start by <a href="<?=site_url('files/add') ?>">adding new files</a>.</p>
<?php
else:
?>
	<p>Select files for this job:</p>
	<form name="job-add" action="<?=site_url('jobs/edit_commit') ?>" method="post" onsubmit="return addUser();">
		<table class="data-table">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>Filename</th>
					<th>Type</th>
					<th>Size</th>
					<th>Created</th>
				</tr>
			</thead>
			<tbody>
<?php
	$shaded = "";
	foreach ($myfiles as $file_id):
		$file = $this->File->get($file_id);
		
		if ($file['filesize']>10000):
			$size = round($file['filesize']/1024,1);
			$unit = "mb";
		else:
			$size = $file['filesize'];
			$unit = "kb";
		endif;
?>
			<tr class="<?=$shaded ?>">
				<td><input name="files[]" type="checkbox" value="<?=$file['id'] ?>" <?=in_array($file['id'],$files)? "checked":"" ?> /></td>
				<td><?=$file['name'] ?></td>
				<td><?=$file['clientname'] ?></td>
				<td><?=$file['filetype'] ?></td>
				<td><?=$size ?> <?=$unit ?></td>
				<td><?=date("n/j/Y, g:ia",strtotime($file['created_at'])) ?></td>
			</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
			</tbody>
		</table>
		
		<p>
			Provide a name for your job (just to help you recognize it):<br />
			<input name="name" type="text" value="<?=$job['name'] ?>" required />
		</p>
		
		<p>Choose your print method and material (<a href="<?=site_url('sections/options') ?>">more info</a>):</p>
		
		<table class="keyval-table">
			<tr>
				<th>Method</th>
				<td>
					<select name="method_id" onchange="selectMethod(this.value);" >
<?php
	foreach ($methods as $method_id):
?>
						<option value="<?=$method_id ?>" <?=($method_id==$method['id'])? "selected":"" ?>><?=$this->Method->name($method_id) ?></option>
<?php
	endforeach;
?>
					</select>
				</td>
			</tr>
			<tr>
				<th>Material</th>
				<td>
<?php
	foreach ($methods as $method_id):
		$materials = $this->Method->materials($method_id);
		if (empty($materials)):
			if ($method_id==$method['id']):
				echo "<span id='material_${method_id}' class='materials'>No material selections for this print method</span>".PHP_EOL;
			else:
				echo "<span id='material_${method_id}' class='materials' style='display:none;'>No material selections for this print method</span>".PHP_EOL;
			endif;
		else:
?>
					<select name="material_<?=$method_id ?>" id="material_<?=$method_id ?>" <?=($method_id==$method['id'])? "":"style='display:none;'" ?> class="materials">
<?php
			foreach ($materials as $material_id):
?>
						<option value="<?=$material_id ?>" <?=($material_id==$material['id'])? "selected":"" ?>><?=$this->Material->name($material_id) ?></option>
<?php
			endforeach;
?>
					</select>			
<?php
		endif;
		$first = FALSE;
	endforeach;
?>
				</td>
			</tr>
		</table>
		
		<p>
			Would you like premium service for additional support and priority printing?<br />
			<input name="premium" type="checkbox" value="1" <?=($job['premium'])? "checked":"" ?> /> Premium service
		</p>
		
		<p>
			Would you like an additional post-processing inspection scan (additional charge)?<br />
			<input name="inspection" type="checkbox" value="1" <?=($job['inspection'])? "checked":"" ?> /> Inspection scan
		</p>
		
		<p>
			We print in an open lab and cannot guarantee printing that is completely obscured
			from view. By request we can make extra efforts not to position your job in a way
			it can be seen by others.<br />
			<input name="confidential" type="checkbox" value="1" <?=($job['confidential'])? "checked":"" ?> /> Confidential job
		</p>

		<p>Clients <em style="margin-left:20px; color:#aaaaaa;">You must have at least one client signed up for email notices</em></p>
		<table id="users" class="data-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>NetID</th>
					<th>Emails?</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
<?php
	foreach ($users as $row):
		$user = $this->User->get($row['user_id']);
?>
				<tr>
					<td><?=$this->User->name($user['id']) ?></td>
					<td>
						<?=$user['netid'] ?>
						<input name="users[]" type="hidden" value="<?=$user['id'] ?>" />
					</td>
					<td style="text-align:center;"><input name="emails[]" type="checkbox" value="<?=$user['id'] ?>" <?=($row['email'])? "checked":"" ?> /></td>
					<td style="text-align:center;" onclick="$(this).parent().remove();"><a href="#" onclick="return false;">Remove</a></td>
				</tr>
<?php
	endforeach;
?>
			</tbody>
		</table>
		<p id="search-wrapper">
			Add client: <input id="search" name="search" type="search" placeholder="Search"
				autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
			<input name="search" type="submit" value="Search" />
		</p>

		<p>Any other instructions or things we should know about your job:</p>
		<textarea name="instructions"><?=$job['instructions'] ?></textarea><br />
			
		<input name="job_id" type="hidden" value="<?=$job['id'] ?>" />
		<input name="submit" type="submit" value="Submit" class="faux-button" />
	</form>
<?php
endif;
?>

</div>


