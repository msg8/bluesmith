<?php
$messages = get_messages();
if (!empty($messages)):
	foreach ($messages as $message):
		$message = each($message);
		$class = $message['key'];
		$text = $message['value'];
?>
		<div class="message message-<?=$class ?>" onclick="$(this).hide('blind',300); return false;"><?=$text ?></div>
<?php
	endforeach;
endif;
?>