<div id="bluesmith-wrapper">
	<h2>Pay with fundcode</h2>
	
	<p>$<?=number_format($cost,2) ?> for job #<?=$job['id'] ?>, &ldquo;<?=$this->Job->name($job['id']) ?>&rdquo;</p>
	
	<form name="fundcode-search" action="<?=site_url("payments/fundcode/".$job['id']) ?>" method="post">
		<input name="job_id" type="hidden" value="<?=$job['id'] ?>" />
		<input name="code" type="search" value="<?=$code ?>" placeholder="Fundcode" />
		<input name="submit" type="submit" value="Verify" />
	</form>
	
<?php
if (!empty($fundcode)):
?>
	<table class="keyval-table">
		<tr>
			<th>Name</th>
			<td><?=$fundcode['name'] ?></td>
		</tr>
		<tr>
			<th>Code</th>
			<td><?=$fundcode['code'] ?></td>
		</tr>
		<tr>
			<th>Valid to</th>
			<td><?=$fundcode['valid_to'] ?></td>
		</tr>
	</table>
	
	<form name="fundcode-pay" action="<?=site_url("payments/fundcode_commit") ?>" method="post">
		<p>
			<input name="agree" type="checkbox" value="1" />
			I certify that I am authorized to use this fundcode, and I agree to pay $<?=number_format($cost,2) ?>.
		</p>
		<input name="job_id" type="hidden" value="<?=$job['id'] ?>" />
		<input name="fundcode_id" type="hidden" value="<?=$fundcode['id'] ?>" />
		<input name="submit" type="submit" value="Submit" />
	</form>
<?php
endif;
?>

	<br />
	
	<div class="subsection">
		<h4>Charges</h4>
<?php
if (empty($charges)):
	echo "No charges recorded yet.".PHP_EOL;
elseif (count($charges)==1):
	$charge = $this->Charge->get($charges[0]);
?>
		<p><?=$charge['name'] ?>: $<?=number_format($charge['amount'],2) ?></p>
<?php
else:
?>
		<table class="keyval-table">
<?php
	foreach ($charges as $charge_id):
		$charge = $this->Charge->get($charge_id);
?>
				<tr>
					<th><?=$charge['name'] ?></td>
					<td>$<?=number_format($charge['amount'],2) ?></td>
				</tr>
<?php
	endforeach;
?>
		</table>
<?php
endif;
?>
		<h4>Payments</h4>
<?php
if (empty($payments)):
	echo "No payments recorded yet.".PHP_EOL;
elseif (count($payments)==1):
	$payment = $this->Payment->get($payments[0]);
?>
	<?=date("g:ia, n/j/Y",strtotime($payment['created_at'])) ?> - <?=$this->User->name($payment['created_by']) ?> paid $<?=number_format($payment['amount'],2) ?> by <?=$payment['type'] ?>, reference #<?=$payment['reference_id'] ?>
<?php
else:
?>
		<table class="data-table">
			<thead>
				<tr>
					<th>Type</th>
					<th>Ref.#</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Submitted</th>
				</tr>
<?php
	$shaded = "";
	foreach ($payments as $payment_id):
		$payment = $this->Payment->get($payment_id);
?>
				<tr class="<?=$shaded ?>">
					<td><?=$payment['type'] ?></td>
					<td><?=$payment['reference_id'] ?></td>
					<td>$<?=number_format($payment['amount'],2) ?></td>
					<td><?=empty($payment['processed_at'])? "Pending":"Received" ?></td>
					<td><?=date("n/j/Y, g:ia",strtotime($payment['created_at'])) ?></td>
				</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
		</table>
<?php
endif;
?>
	</div>
	
</div>