<div id="bluesmith-wrapper">
	<h2>Bluechips payments</h2>
	<p><strong>Current Bluechips balance:</strong> $<?=number_format($balance,2) ?></p>
<?php
if ($balance<$cost):
?>
	<p>You do not have sufficient Bluechips allocation to pay for this job.</p>
	<p><a href="<?=site_url('jobs/pay/'.$job['id']) ?>">Back to payment options</a></p>
<?php
else:
?>
	<form name="flexaccount-pay" action="<?=site_url("payments/bluechips_commit") ?>" method="post">
		<p>
			<input name="agree" type="checkbox" value="1" />
			I authorized the use of my Bluechips funds, and I agree to pay $<?=number_format($cost,2) ?>.
			I understand there are <strong>no refunds</strong> and issues of quality and completeness should be
			handled prior to payment.
		</p>
		<input name="job_id" type="hidden" value="<?=$job['id'] ?>" />
		<input name="submit" type="submit" value="Submit" />
	</form>
<?php
endif;
?>
</div>
