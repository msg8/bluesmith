<div id="bluesmith-wrapper">
	<h2>Payment canceled</h2>
	<p>
		Your payment has been canceled.<br />
		<a href="<?=site_url('jobs/user/'.me()) ?>">Back to my jobs</a>
	</p>
</p>