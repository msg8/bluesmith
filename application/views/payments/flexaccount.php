<div id="bluesmith-wrapper">
	<h2>FLEX payments</h2>
<?php
$status = $broker->status_info->status_code;
if ($status>=100):
?>
	<p>FLEX payments unavailable for this card: <?=broker_error($status) ?></p>
	<p><a href="<?=site_url('jobs/pay/'.$job['id']) ?>">Back to payment options</a></p>
<?php
else:
	$balance = (double) $broker->acct_balance;
	$cost = $this->Job->cost($job['id']);
?>
	<p><strong>Current FLEX balance:</strong> $<?=number_format($balance,2) ?></p>
<?php
	if ($balance<$cost):
?>
	<p>You do not have sufficient FLEX funds to pay for this job.</p>
	<p><a href="<?=site_url('jobs/pay/'.$job['id']) ?>">Back to payment options</a></p>
<?php
	else:
?>
	<form name="flexaccount-pay" action="<?=site_url("payments/flexaccount_commit") ?>" method="post">
		<p>
			<input name="agree" type="checkbox" value="1" />
			I authorized the use of my FLEX funds, and I agree to pay $<?=number_format($cost,2) ?>.
			I understand there are <strong>no refunds</strong> and issues of quality and completeness should be
			handled prior to payment.
		</p>
		<input name="job_id" type="hidden" value="<?=$job['id'] ?>" />
		<input name="submit" type="submit" value="Submit" />
	</form>
<?php
	endif;
endif;
?>
</div>
