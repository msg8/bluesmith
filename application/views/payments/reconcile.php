<div id="bluesmith-wrapper">
	<h2>Med Center payments</h2>
	
<?php
if (empty($payments)):
?>
	<p>No matching payments.</p>
<?php
else:
?>
	<table class="data-table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Job</th>
				<th>Submitter</th>
				<th>Amount</th>
				<th>Approved</th>
				<th>Billed</th>
				<th>Reconciled</th>
			</tr>
		</thead>
		<tbody>
<?php
	$total = 0; $tally = 0; $reconciled = 0;
	foreach ($payments as $payment_id):
		$payment = $this->Payment->get($payment_id);
		$total += $payment['amount'];
?>
			<tr>
				<td><?=$payment['id'] ?></td>
				<td><a href="<?=site_url('jobs/show/'.$payment['job_id']) ?>"><?=$this->Job->name($payment['job_id']) ?></a></td>
				<td><a href="<?=site_url('jobs/user/'.$payment['created_by']) ?>"><?=$this->User->name($payment['created_by']) ?></a></td>
				<td>$<?=number_format($payment['amount'],2) ?></td>
				<td><?=date("n/j/Y",strtotime($payment['created_at'])) ?></td>
<?php
		if (empty($payment['processed_at'])):
?>
				<td>Not billed</td>
				<td></td>
<?php
		else:
?>
				<td><?=date("n/j/Y",strtotime($payment['processed_at'])) ?></td>
				<td id="reconcile-<?=$payment['id'] ?>" >
<?php
			if (!empty($payment['reconciled_at'])):
				echo date("n/j/Y",strtotime($payment['processed_at']));
			else:
?>
					<a href="#" onclick="reconcile(<?=$payment['id'] ?>); return false;">Reconcile</a>
<?php
			endif;
?>
				</td>
<?php
		endif;
?>
			</tr>
<?php
	endforeach;
?>
		</tbody>
	</table>
	
	<p><strong>TOTAL:</strong> <?=count($payments) ?> payment<?=(count($payments)==1)? "":"s" ?>, $<?=number_format($total,2) ?></strong></p>
<?php
endif;
?>
</div>
