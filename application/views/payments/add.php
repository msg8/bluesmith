<div id="bluesmith-wrapper">
	<h2>Job payment</h2>
<?php
if (!empty($payments)):
?>
	<p style="font-weight:bold;">This job has already been paid for:</p>
	<ul>
<?php
	foreach ($payments as $payment_id):
		$payment = $this->Payment->get($payment_id);
?>
		<li>
			<?=date("g:ia, n/j/Y",strtotime($payment['created_at'])) ?> - <?=$this->User->name($payment['created_by']) ?> paid $<?=number_format($payment['amount'],2) ?> by <?=$payment['type'] ?>, reference #<?=$payment['reference_id'] ?>
<?php
		if (empty($payment['processed_at'])):
			if ($payment['type']=="fundcode"):
?>
			<a href="<?=site_url('payments/remove/'.$payment['id']) ?>" class="button">Cancel</a>
<?php
			else:
				echo " <span class='note'>Processing</span>";
			endif;
		endif;
?>
		</li>
<?php
	endforeach;
?>
	</ul>
<?php
endif;
?>
	<h5>Select payment method</h5>
	<div class="subsection">
		<p><strong>TOTAL COST:</strong> $<?=number_format($cost,2) ?></p>
		<table class="keyval-table">
			<tr>
				<td><a href="<?=site_url("payments/creditcard/".$job['id']) ?>" class="faux-button">Pay now credit card</a></td>
				<td>Pay using a credit card through DukePay</td>
			</tr>
			<tr>
				<td><a href="<?=site_url("payments/fundcode/".$job['id']) ?>" class="faux-button">Pay now fundcode</a></td>
				<td>Pay using a Duke departmental fundcode  (you must be eligible)</td>
			</tr>
			<tr>
				<td><a href="<?=site_url("payments/flexaccount/".$job['id']) ?>" class="faux-button">Pay now FLEX account</a></td>
				<td>Pay using a Duke FLEX account</td>
			</tr>
			<tr>
				<td><a href="<?=site_url("payments/bluechips/".$job['id']) ?>" class="faux-button">Pay now Bluechips</a></td>
				<td>Pay using your Bluesmith allocated credits</td>
			</tr>
<?php
if (has_access("priority")):
?>
			<tr>
				<td><a href="<?=site_url("payments/invoice/".$job['id']) ?>" class="faux-button">Med Center invoice</a></td>
				<td>Add to monthly invoice to be sent to Med Center</td>
			</tr>
<?php
endif;

if (has_access("administration")):
?>
			<tr>
				<td><a href="<?=site_url("payments/manual/".$job['id']) ?>" class="faux-button">Mark as paid</a></td>
				<td>Override payment and mark job as paid for manually</td>
			</tr>
<?php
endif;
?>
		</table>
		
		<p>
			<strong>Please note:</strong> We are unable to offer refunds for print jobs. All payments are considered final.
			If you have an issue with your charges please bring it up with us prior to paying.
		</p>
	</div>
			
	<h5>Charges</h5>
	<div class="subsection">
<?php
if (empty($charges)):
	echo "No charges recorded yet.".PHP_EOL;
else:
?>
		<table class="data-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Amount</th>
					<th>Added</th>
				</tr>
			</thead>
<?php
	$shaded = "";
	foreach ($charges as $charge_id):
		$charge = $this->Charge->get($charge_id);
?>
				<tr class="<?=$shaded ?>">
					<td><?=$charge['name'] ?></td>
					<td style="text-align:right;">$<?=number_format($charge['amount'],2) ?></td>
					<td><?=date("n/j/Y, g:ia",strtotime($charge['created_at'])) ?></td>
				</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
		</table>
<?php
endif;
?>
	</div>
	
	<h5>Job details</h5>
	<div class="subsection">
		<table class="keyval-table">
			<tr>
				<th>ID</th>
				<td>#<?=$job['id'] ?></td>
			</tr>
			<tr>
				<th>Name</th>
				<td><?=$job['name'] ?></td>
			</tr>
			<tr>
				<th>Status</th>
				<td><?=ucfirst($job['status']) ?></td>
			</tr>
			<tr>
				<th>Quote</th>
				<td><?=is_numeric($job['quote'])? '$'.number_format($job['quote'],2):"<em>not quoted</em>" ?></td>
			</tr>
			<tr>
				<th>Print method</th>
				<td><?=$this->Material->method($job['material_id']) ?></td>
			</tr>
			<tr>
				<th>Material</th>
				<td><?=$this->Material->name($job['material_id']) ?></td>
			</tr>
			<tr>
				<th>Services</th>
				<td>
<?php
$services = array();
if ($job['premium'])
	$services[] = "Premium service";
if ($job['inspection'])
	$services[] = "Inspection scan";
if (empty($services)):
	echo "<em>No extras</em>".PHP_EOL;
else:
	echo implode(", ",$services).PHP_EOL;
endif;
?>
				</td>
			</tr>
			<tr>
				<th>Submitted by</th>
				<td><?=$this->User->name($job['created_by']) ?></td>
			</tr>
			<tr>
				<th>Submitted at</th>
				<td><?=date("n/j/Y, g:ia",strtotime($job['created_at'])) ?></td>
			</tr>
			<tr>
				<th>Last update</th>
				<td><?=timespan(strtotime($job['updated_at']),time()) ?></td>
			</tr>
		</table>
	</div>
</div>
