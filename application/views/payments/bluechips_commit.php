<div id="bluesmith-wrapper">
	<h2>Payment received</h2>
	
	<p>Your Bluechips payment was accepted.</p>
<?php
if ($job['source']=="Purchase"):
?>
	<p>You may now pick up your purchase if you have not already.</p>
<?php
elseif (!empty($job['printed_at'])):
?>
	<p>You may now pick up your printed item(s) at any point.</p>
<?php
else:
?>
	<p>You will be notified once your job is printed and available for pickup.</p>
<?php
endif;
?>
	<p><a href="<?=site_url('jobs/show/'.$job['id']) ?>">Back to my job</a></p>
</div>