<div id="bluesmith-wrapper">
	<h2>Redirecting</h2>
	<p>
		If you are not redirected automatically, please
		<a href="#" onclick="document.forms.cybersource.submit(); return false;">click here</a>.
	</p>
	<form name="cybersource" action="<?=cyber_form_action() ?>" method="POST" style="display:none;">
<?php
foreach ($params as $field=>$value):
?>
		<input type="hidden" name="<?=$field ?>" value="<?=$value ?>" />
<?php
endforeach;
?>
		<input name="signature" type="hidden" value="<?=sign($params) ?>" />
	</form>
</div>