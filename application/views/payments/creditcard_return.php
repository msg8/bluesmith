<div id="bluesmith-wrapper">
	<h2>Payment processing</h2>
	<p>
		Thank you for submitting payment.<br />
		Credit card payments can take up to 24 hours to process.
		You will be notified when your purchase is complete.
	</p>
</div>