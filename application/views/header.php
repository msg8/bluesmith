<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->helper("asset");
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Bluesmith<?=empty($title)? "":" | ${title}" ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="<?=base_url() ?>assets/company/css/normalize.min.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/company/css/header-footer.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/company/css/main-navigation.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/company/css/content.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/company/css/tables.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/company/css/forms.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/company/css/print.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lato:400' rel='stylesheet' type='text/css'>
	
	<!--favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?=base_url() ?>assets/company/apple-touch-icon.png">
	<link rel="apple-touch-icon" href="<?=base_url() ?>assets/company/apple-touch-icon-114x114.png" sizes="114x114" />
	<link rel="apple-touch-icon" href="<?=base_url() ?>assets/company/apple-touch-icon-72x72.png" sizes="72x72" />
	<link rel="apple-touch-icon" href="<?=base_url() ?>assets/company/apple-touch-icon-precomposed.png" />
	<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url() ?>assets/company/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url() ?>assets/company/favicon-16x16.png">
	<link rel="manifest" href="<?=base_url() ?>assets/company/site.webmanifest">
	<link rel="mask-icon" href="<?=base_url() ?>assets/company/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="<?=base_url() ?>assets/company/favicon.ico">
	<meta name="msapplication-TileColor" content="#2d89ef">
	<meta name="msapplication-config" content="<?=base_url() ?>assets/company/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

	<!--[if lt IE 9]>
		<script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
	<![endif]-->

	<script src="<?=base_url() ?>assets/company/js/main.js"></script>

	<!--jQuery UI -->
	<script type="text/javascript" src="<?=base_url() ?>assets/jqueryui/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="<?=base_url() ?>assets/jqueryui/jquery-ui.min.js"></script>
	<link href="<?=base_url() ?>assets/jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?=base_url() ?>assets/jqueryui/jquery-ui.structure.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?=base_url() ?>assets/jqueryui/jquery-ui.theme.min.css" rel="stylesheet" type="text/css" media="all" />

	<!--auto-include appropriate JS & CSS files -->
	<?php include_assets(); ?>

	<!-- CMS stylesheet -->
	<link href="<?=site_url('sections/stylesheet') ?>" rel="stylesheet" type="text/css" media="all" />

	<script>
		var baseUrl = "<?=base_url() ?>";
		var siteUrl = "<?=site_url() ?>";
	</script>

	<!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
		var u="//pulse.oit.duke.edu/analytics/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', '1']);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<!-- End Piwik Code -->
</head>
<body>
	<!--[if lt IE 8]>
		<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<div id="l-ss-header-container">
		<header class="l-ss-wrapper clearfix">
			<div class="ss-header-first">
				<a class="ss-logo" href="<?=$this->Setting->get("Company URL") ?>"><img src="<?=base_url() ?>assets/company/images/logo.png" alt="logo" /></a>
				<a class="ss-sitename" href="/"><h1>Bluesmith</h1></a>
			</div>
			<div class="ss-header-second">
				<nav>
					<ul class="ss-menu">
<?php
if (is_authed()):
?>
						<li class="ss-menu-leaf">
							<a href="<?=site_url('users/show/'.me()) ?>" style="border:none;"><?=$this->User->name(me()) ?></a>
							<a href="<?=site_url('auth/logout') ?>">Logout</a>
						</li>
<?php
else:
?>
						<li class="ss-menu-leaf"><a href="<?=site_url('auth/login') ?>">Login</a></li>
<?php
endif;
?>
					</ul>
					<form method="post" action="<?=site_url('jobs/search') ?>" class="ss-nav-form">
						<label class="visuallyhidden" for="big-search">Search</label><input name="search" class="ss-mega-input" id="big-search" type="search" placeholder="Search jobs"><button type="submit" class="ss-nav-submit">
						<img src="<?=base_url() ?>assets/company/images/magnifying-glass-black.png" />
					  </button>
					</form>
				</nav>
			</div>
		</header>
	</div>
	
	<div id="l-ss-nav-container">
		<!-- JS Menu without jQuery - please see /js/main.js -->
		<div id="ss-toggle-button" onclick="toggleNav(); return false;">☰ Menu & Search</div>
		<div id="ss-toggle-target" class="l-ss-wrapper clearfix">
			<form method="post" action="<?=site_url('jobs/search') ?>" class="ss-nav-form">
				<label class="visuallyhidden" for="big-search">Search</label><input name="search" class="ss-mega-input" id="big-search" type="search" placeholder="Search jobs"><button type="submit" class="ss-nav-submit">
				<img src="<?=base_url() ?>assets/company/images/magnifying-glass-black.png" />
			  </button>
			</form>
			<div class="ss-nav">
				<nav>
<?php
// determine active menu item
$menu = "";
switch ($this->router->class):
	case "sections":
		if ($this->router->method=="index"): $menu = "Overview";
		elseif ($this->router->method=="options"): $menu = "Options";
		else: $menu = "CMS";
		endif;
	break;
	
	case "jobs":
		if ($this->router->method=="add"): $menu = "Submit";
		else: $menu = "Jobs";
		endif;
	break;
	
	case "users":
		$menu = "Users";
	break;
	
	case "reports":
		$menu = "Reports";
	break;
	
	case "purchases":
		$menu = "Purchases";
	break;
endswitch;
?>
					<ul class="ss-menu">
						<li class="ss-menu-leaf"><a href="<?=site_url('sections') ?>" <?=($menu=='Overview')? "class='active'":"" ?>>Overview</a></li>
						<li class="ss-menu-leaf"><a href="<?=site_url('sections/options') ?>" <?=($menu=='Options')? "class='active'":"" ?>>Print options</a></li>
						<li class="ss-menu-leaf"><a href="<?=site_url('jobs/add') ?>" <?=($menu=='Submit')? "class='active'":"" ?>>Submit a job</a></li>
<?php
if (has_access("proctor")):
	if ($actions = $this->Job->actions()):
		$actions = "<span id='actions'>".count($actions)."</span>";
	else:
		$actions = "";
	endif;
?>
						<li class="ss-menu-leaf"><a href="<?=site_url('jobs/browse') ?>" <?=($menu=='Jobs')? "class='active'":"" ?>>Browse jobs<?=$actions ?></a></li>
						<li class="ss-menu-leaf"><a href="<?=site_url('reports') ?>" <?=($menu=='Reports')? "class='active'":"" ?>>Reports</a></li>
						<li class="ss-menu-leaf"><a href="<?=site_url('purchases/add') ?>" <?=($menu=='Purchases')? "class='active'":"" ?>>Purchase</a></li>
<?php
	if (has_access("administration")):
?>

						<li class="ss-menu-leaf"><a href="<?=site_url('sections/cms') ?>" <?=($menu=='CMS')? "class='active'":"" ?>>Manage content</a></li>
						<li class="ss-menu-leaf"><a href="<?=site_url('users') ?>" <?=($menu=='Users')? "class='active'":"" ?>>Manage users</a></li>
<?php
	endif;

// regular users just see their own jobs
else:
?>
						<li class="ss-menu-leaf"><a href="<?=site_url('jobs/user/'.me()) ?>" <?=($menu=='Jobs')? "class='active'":"" ?>>My jobs</a></li>
<?php
endif;
?>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<section id="messages" onclick="hideMessages();">
		<?php $this->load->view("messages"); ?>
	</section>

	<div id="l-ss-main-container">
		<div id="bluesmith-logo">
			<img src="<?=base_url() ?>assets/img/bluesmith-horiz.png" alt="Bluesmith-logo" style="width:100%;" />
		</div>

		<div class="l-ss-main l-ss-wrapper clearfix">
			<article class="ss-main-content">

