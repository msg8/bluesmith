<div id="bluesmith-wrapper">
	<h2>Content Management</h2>

	<div id="tabs">
		<ul id="tabs-buttons">
			<li><a href="#tabs-methods">Print methods</a></li>
			<li><a href="#tabs-materials">Print materials</a></li>
			<li><a href="#tabs-sections">Sections</a></li>
			<li><a href="#tabs-emails">Emails</a></li>
		</ul>
		
		<div id="tabs-methods">
			<p><a href="<?=site_url('methods/add') ?>" class="faux-button">Add new</a></p>
<?php
if (empty($methods)):
	echo "<p>No methods</p>".PHP_EOL;
else:
?>
			<table class="data-table">
				<tr>
					<th>Method</th>
					<th>Materials</th>
					<th>Description</th>
					<th></th>
				</tr>
<?php
	$shaded = "";
	foreach($methods as $method_id):
		$method = $this->Method->get($method_id);
?>
				<tr class="<?=$shaded ?>">
					<td><a href="<?=site_url('methods/edit/'.$method['id']) ?>"><?=$this->Method->name($method['id']) ?></a></td>
					<td><?=count($this->Method->materials($method['id'])) ?></td>
					<td><?=trim(substr(strip_tags($method['description']),0,35)) ?>...</td>
					<td><a href="<?=site_url('methods/remove/'.$method['id']) ?>">Remove</a></td>
				</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
			</table>
<?php
endif;
?>
		</div>
		
		<div id="tabs-materials">
			<p><a href="<?=site_url('materials/add') ?>" class="faux-button">Add new</a></p>
<?php
if (empty($materials)):
	echo "<p>No materials</p>".PHP_EOL;
else:
?>
			<table class="data-table">
				<tr>
					<th>Material</th>
					<th>Method</th>
					<th>Description</th>
					<th></th>
				</tr>
<?php
	$shaded = "";
	foreach($materials as $material_id):
		$material = $this->Material->get($material_id);
?>
				<tr class="<?=$shaded ?>">
					<td><a href="<?=site_url('materials/edit/'.$material['id']) ?>"><?=$this->Material->name($material['id']) ?></a></td>
					<td><?=$this->Method->name($material['method_id']) ?></td>
					<td><?=trim(substr(strip_tags($material['description']),0,35)) ?>...</td>
					<td><a href="<?=site_url('materials/remove/'.$material['id']) ?>">Remove</a></td>
				</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
			</table>
<?php
endif;
?>
		</div>
		
		
		<div id="tabs-sections">
<?php
foreach ([ "Overview","Submit","Options","Approve","Stylesheet","Disclaimers" ] as $name):
	$section = $this->Section->get_by_name($name);
?>
			<h5><?=$section['name'] ?></h5>
			<div class="subsection">
				<form name="section-<?=$section['name'] ?>" action="<?=site_url('sections/edit_commit') ?>" method="post">
					<textarea name="content"><?=$section['content'] ?></textarea/>
					<br />
					<input name="section_id" type="hidden" value="<?=$section['id'] ?>" />
					<input name="submit" type="submit" value="Update" class="faux-button" />
				</form>
				<p class="note">
					Last updated by <?=$this->User->name($section['updated_by']) ?>,
					<?=timespan(strtotime($section['updated_at']),time()) ?> ago
				</p>
			</div>
<?php
endforeach;
?>
		</div>
		
		<div id="tabs-emails">
		<p>Emails will automatically include job details and relevant links.</p>
<?php
foreach ($emails as $email_id):
	$email = $this->Email->get($email_id);
?>
			<h5><?=$email['name'] ?></h5>
			<div class="subsection">
				<form name="email-<?=$email['name'] ?>" action="<?=site_url('emails/edit_commit') ?>" method="post">
					<p>
						Target:<br />
						<input name="target" type="radio" value="client" <?=($email['target']=="client")? "checked":"" ?> /> Client &amp; staff
						<input name="target" type="radio" value="staff" <?=($email['target']=="staff")? "checked":"" ?> /> Staff only
						<br />
						Subject: <input name="subject" type="text" value="<?=$email['subject'] ?>" />
					</p>
					Content:<br />
					<textarea name="content"><?=$email['content'] ?></textarea/>
					<br />
					<input name="email_id" type="hidden" value="<?=$email['id'] ?>" />
					<input name="submit" type="submit" value="Update" class="faux-button" />
				</form>
				<p class="note">
					Last updated by <?=$this->User->name($email['updated_by']) ?>,
					<?=timespan(strtotime($email['updated_at']),time()) ?> ago
				</p>
			</div>
<?php
endforeach;
?>
		</div>
			
		</div>
  </div>
  
</div>

<script>
	$(document).ready(function() {
		$("#tabs").tabs({ active: <?=$tab ?> });
	});
</script>
