<div id="bluesmith-wrapper">
	<h2>OIT 3D Printing Service</h2>

<?=$this->Section->content("Options"); ?>

	<h3>Methods and Materials</h3>

<?php
if (empty($methods)):
	echo "<p>No print methods available; contact an administrator for assistance.".PHP_EOL;
else:
	foreach ($methods as $method_id):
		$method = $this->Method->get($method_id);
?>
	<div class="method">
		<h4><?=$this->Method->name($method['id']) ?><span class="note"><?=$method['fullname'] ?></span></h4>

		<?=$method['description'] ?>
<?php
	$materials = $this->Method->materials($method['id']);
		if (!empty($materials)):
?>
		<h5>Available materials:</h5>
		<ul>
<?php
			foreach ($materials as $material_id):
				$material = $this->Material->get($material_id);
?>
			<li><strong><?=$this->Material->name($material['id']) ?></strong>: <?=$material['description'] ?></li>
<?php
			endforeach;
?>
		</ul>
<?php
		endif;
?>
	</div>
<?php
	endforeach;
endif;
?>
	
	<p><a href="<?=site_url('jobs/add') ?>" class="faux-button">Submit a job now</a></p>

</div>