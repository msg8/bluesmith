<?php
header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="Bluesmith-payments_report.csv";');
?>
"ID",Job name,Job created,User,Type,Reference,Submitted,Processed,Amount
<?php
$types = $this->Payment->types();
foreach ($payments as $payment):
	$job = $this->Job->get($payment['job_id']);
	echo $payment['id'].",";
	echo (str_replace(","," ",$job['name'])).",";
	echo date("n/j/Y",strtotime($job['created_at'])).",";
	echo $this->User->name($payment['created_by']).",";
	echo $types[$payment['type']].",";
	echo $payment['reference_id'].",";
	echo date("n/j/Y",strtotime($payment['created_at'])).",";
	echo date("n/j/Y",strtotime($payment['processed_at'])).",";
	echo "$".number_format($payment['amount'],2,".","");
	echo PHP_EOL;
endforeach;
