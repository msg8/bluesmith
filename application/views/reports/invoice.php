<?php
header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="bluesmith_invoice.csv";');

$headers = array_keys($rows[0]);
echo implode(",",$headers).PHP_EOL;
	
foreach ($rows as $row):
	echo implode(",",$row);
	echo PHP_EOL;	
endforeach;