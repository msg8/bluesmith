<div id="bluesmith-wrapper">
	<h2>Payments report</h2>
	
	<h3>Criteria</h3>
	<form name="report" action="<?=current_url() ?>" method="post">
		<table class="keyval-table">
			<tr>
				<th>Start date</th>
				<td><input name="start" type="text" value="<?=$form['start'] ?>" class="datepicker" /></td>
			</tr>
			<tr>
				<th>End date</th>
				<td><input name="end" type="text" value="<?=$form['end'] ?>" class="datepicker" /></td>
			</tr>
			<tr>
				<th>Date metric</th>
				<td>
					<label>
						<input name="metric" type="radio" value="processed_at" <?=($form['metric']=="processed_at")? "checked":"" ?> />
						Processed
					</label>
					<label>
						<input name="metric" type="radio" value="created_at" <?=($form['metric']=="created_at")? "checked":"" ?> />
						Submitted
					</label>
				</td>
			</tr>
			<tr>
				<th>Types</th>
				<td>
<?php
foreach ($this->Payment->types() as $type=>$display):
?>
					<label>
						<input name="types[]" type="checkbox" value="<?=$type ?>" <?=in_array($type,$form['types'])? "checked":"" ?> />
						<?=$display ?>
					</label>
<?php
endforeach;
?>
				</td>
			</tr>
		</table>
		
		<input name="submit" type="submit" value="Submit" class="faux-button" />
	</form>

<?php
if (!empty($form['submit'])):
?>
	<h3>Results</h3>
<?php
	if (empty($payments)):
?>
	<p>No matching payments.</p>
<?php
	else:
?>
	<table class="data-table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Job</th>
				<th>User</th>
				<th>Type</th>
				<th>Submitted</th>
				<th>Processed</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody>
<?php
		$total = 0;
		$types = $this->Payment->types();
		foreach ($payments as $payment):
			$total += $payment['amount'];
?>
			<tr>
				<td><?=$payment['id'] ?></td>
				<td><a href="<?=site_url('jobs/show/'.$payment['job_id']) ?>"><?=$this->Job->name($payment['job_id']) ?></a></td>
				<td><a href="<?=site_url('jobs/user/'.$payment['created_by']) ?>"><?=$this->User->name($payment['created_by']) ?></a></td>
				<td><?=$types[$payment['type']] ?></td>
				<td><?=date("n/j/Y",strtotime($payment['created_at'])) ?></td>
				<td><?=date("n/j/Y",strtotime($payment['processed_at'])) ?></td>
				<td>$<?=number_format($payment['amount'],2) ?></td>
			</tr>
<?php
		endforeach;
?>
		</tbody>
	</table>
	
	<p><strong>TOTAL:</strong> <?=count($payments) ?> payment<?=(count($payments)==1)? "":"s" ?>, $<?=number_format($total,2) ?></strong></p>
<?php
	endif;
endif;
?>
</div>
