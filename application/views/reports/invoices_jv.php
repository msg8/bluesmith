<?php
header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="bluesmith-invoices_jv-'.$stamp.'.csv";');

$headers = array_keys($rows[0]);
echo implode(",",$headers).PHP_EOL;
	
foreach ($rows as $row):
	echo implode(",",$row);
	echo PHP_EOL;	
endforeach;