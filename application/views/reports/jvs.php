<div id="bluesmith-wrapper">
	<h2>Past JVs</h2>
	
	<h3>Fundcode payments</h3>
	<ul>
<?php
foreach ($fundcodes as $stamp=>$count):
?>
		<li><a href="<?=site_url('reports/fundcodes_jv/'.$stamp) ?>"><?=date("n/j/Y",$stamp) ?> - <?=$count ?> payment<?=($count==1)? "":"s" ?></a></li>
<?php
endforeach;
?>
	</ul>
	
	<h3>Invoice (Med Center) payments</h3>
	<ul>
<?php
foreach ($invoices as $stamp=>$count):
?>
		<li><a href="<?=site_url('reports/invoices_jv/'.$stamp) ?>"><?=date("n/j/Y",$stamp) ?> - <?=$count ?> payment<?=($count==1)? "":"s" ?></a></li>
<?php
endforeach;
?>
	</ul>
	
</div>
