<div id="bluesmith-wrapper">
	<h2>Users report</h2>
	
	<h3>Criteria</h3>
	<form name="report" action="<?=current_url() ?>" method="post">
		<table class="keyval-table">
			<tr>
				<th>Start date</th>
				<td><input name="start" type="text" value="<?=$form['start'] ?>" class="datepicker" /></td>
			</tr>
			<tr>
				<th>End date</th>
				<td><input name="end" type="text" value="<?=$form['end'] ?>" class="datepicker" /></td>
			</tr>
		</table>
		
		<input name="submit" type="submit" value="Submit" class="faux-button" />
	</form>

<?php
if (!empty($form['submit'])):
?>
	<h3>Results</h3>
<?php
	if (empty($rows)):
?>
	<p>No matching users.</p>
<?php
	else:
?>
	<table class="data-table">
		<thead>
			<tr>
				<th>ID</th>
				<th>First name</th>
				<th>Last name</th>
				<th>NetID</th>
				<th>Email</th>
				<th>Affiliation</th>
				<th>Association</th>
			</tr>
		</thead>
		<tbody>
<?php

		foreach ($rows as $row):
			$user = $this->User->get($row['user_id']);
?>
			<tr>
				<td><?=$user['id'] ?></td>
				<td><?=$user['firstname'] ?></td>
				<td><?=$user['lastname'] ?></td>
				<td><?=$user['netid'] ?></td>
				<td><?=$user['email'] ?></td>
				<td><?=$user['affiliation'] ?></td>
				<td><?=$user['association'] ?></td>
			</tr>
<?php
		endforeach;
?>
		</tbody>
	</table>

<?php
	endif;
endif;
?>
</div>
