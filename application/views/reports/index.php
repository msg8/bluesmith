<div id="bluesmith-wrapper">
	<h2>Reports</h2>
	
	<ul>
		<li><a href="<?=site_url('reports/payments_table') ?>">Payments table</a></li>
		<li><a href="<?=site_url('reports/payments_export') ?>">Payments export</a></li>
		<li><a href="<?=site_url('reports/users_table') ?>">Users table</a></li>
		<li><a href="<?=site_url('reports/users_export') ?>">Users export</a></li>
		<!-- <li><a href="<?=site_url('reports/jobs_export') ?>">Jobs export</a></li> -->
<?php
if (has_access("administration")):
?>
		<li><a href="<?=site_url('payments/reconcile') ?>">Med Center Reconciliation</a></li>
		<li><a href="<?=site_url('reports/jvs') ?>">Past JVs</a></li>
<?php
endif;
?>
	</ul>

<!--	
	<h3>Request a report</h3>
	<form name="report-request" action="<?=site_url("reports/request_commit") ?>" method="post">
		<textarea name="request" placeholder="Describe in detail..."></textarea>
		<br />
		<input name="submit" type="submit" value="Submit" class="faux-button" />
	</form>
-->
</div>
