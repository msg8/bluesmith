<?php
header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="Bluesmith-users_report.csv";');
?>
"ID",First name,Last name,NetID,Email,Affiliation,Association
<?php
foreach ($rows as $row):
	$user = $this->User->get($row['user_id']);
	echo $user['id'].",";
	echo $user['firstname'].",";
	echo $user['lastname'].",";
	echo $user['netid'].",";
	echo $user['email'].",";
	echo $user['affiliation'].",";
	echo $user['association'];
	echo PHP_EOL;
endforeach;
