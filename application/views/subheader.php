<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->helper("asset");
$this->load->helper("traffic"); // logs ALL traffic! comment out to disable
?>
<!--jQuery UI -->
<script type="text/javascript" src="<?=base_url() ?>assets/jqueryui/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqueryui/jquery-ui.min.js"></script>
<link href="<?=base_url() ?>assets/jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?=base_url() ?>assets/jqueryui/jquery-ui.structure.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?=base_url() ?>assets/jqueryui/jquery-ui.theme.min.css" rel="stylesheet" type="text/css" media="all" />

<!--auto-include appropriate JS & CSS files -->
<?php include_assets(); ?>

<!-- CMS stylesheet -->
<link href="<?=site_url('sections/stylesheet') ?>" rel="stylesheet" type="text/css" media="all" />

<script>
	var baseUrl = "<?=base_url() ?>";
	var siteUrl = "<?=site_url() ?>";
</script>

	
<section id="messages" onclick="hideMessages();">
	<?php $this->load->view("messages"); ?>
</section>
	
<aside id="subheader">
	<div id="submenu">
		<p style="font-style:italic;"><a href="https://3dprint.duke.edu/3d-printing-service">&#8678; Back to OIT</a></p>
		<h2>3D Printing</h2>
		<ul>
			<li><a href="<?=site_url('sections') ?>">Overview</a></li>
			<li><a href="<?=site_url('sections/options') ?>">Print options</a></li>
			<li><a href="<?=site_url('jobs/add') ?>">Submit a job</a></li>
			<li><a href="<?=site_url('jobs/user/'.me()) ?>">My jobs</a></li>
			<li><a href="<?=site_url('auth/logout') ?>">Logout</a></li>
		</ul>
<?php
if (has_access("proctor")):
	if ($actions = $this->Job->actions()):
		$actions = "<span id='actions'>".count($actions)."</span>";
	else:
		$actions = "";
	endif;
?>
		<br />
		<h2>Administration</h2>
		<ul>
			<li><a href="<?=site_url('jobs/browse') ?>">Browse jobs</a><?=$actions ?></li>
<?php
	if (has_access("administration")):
?>
			<li><a href="<?=site_url('sections/cms') ?>">Manage content</a></li>
			<li><a href="<?=site_url('users/permissions') ?>">Manage users</a></li>
<?php
	endif;
?>
		</ul>
<?php
endif;
?>
	</div>
</aside>

<div id="bluesmith-logo">
	<img src="/assets/img/bluesmith-horiz.png" alt="Bluesmith-logo" style="width:100%;" />
</div>