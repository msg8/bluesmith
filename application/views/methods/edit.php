<div id="bluesmith-wrapper">
	<h2>Edit print method</h2>

	<form name="method-edit" action="<?=site_url('methods/edit_commit') ?>" method="post">
		<table class="keyval-table">
			<tr>
				<th>Name/Acronym</th>
				<td><input name="name" type="text" value="<?=$method['name'] ?>" /></td>
			</tr>
			<tr>
				<th>Full name</th>
				<td><input name="fullname" type="text" value="<?=$method['fullname'] ?>" /></td>
			</tr>
			<tr>
				<th>Display order</th>
				<td><input name="sortorder" type="number" value="<?=$method['sortorder'] ?>" /></td>
			</tr>
		</table>

		Description:<br />
		<textarea name="description"><?=$method['description'] ?></textarea/>
		<br />
		<input name="method_id" type="hidden" value="<?=$method['id'] ?>" />
		<input name="submit" type="submit" value="Update" class="faux-button" />
	</form>
</div>