<div id="bluesmith-wrapper">
	<h2>Add print method</h2>

	<form name="method-add" action="<?=site_url('methods/add_commit') ?>" method="post">
		<table class="keyval-table">
			<tr>
				<th>Name/Acronym</th>
				<td><input name="name" type="text" /></td>
			</tr>
			<tr>
				<th>Full name</th>
				<td><input name="fullname" type="text" /></td>
			</tr>
			<tr>
				<th>Display order</th>
				<td><input name="sortorder" type="number" value="0" /></td>
			</tr>
		</table>

		Description:<br />
		<textarea name="description"></textarea/>
		<br />
		<input name="submit" type="submit" value="Add" class="faux-button" />
	</form>
</div>