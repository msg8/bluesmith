<div id="bluesmith-wrapper">
	<h2>Edit print material</h2>

	<form name="material-edit" action="<?=site_url('materials/edit_commit') ?>" method="post">
		<table class="keyval-table">
			<tr>
				<th>Print method</th>
				<td>
					<select name="method_id">
<?php
foreach ($methods as $method_id):
?>
						<option value="<?=$method_id ?>" <?=($method_id==$material['method_id'])? "selected":"" ?>><?=$this->Method->name($method_id) ?></option>
<?php
endforeach;
?>
					</select>
				</td>
			</tr>
			<tr>
				<th>Name</th>
				<td><input name="name" type="text" value="<?=$material['name'] ?>" /></td>
			</tr>
			<tr>
				<th>Display order</th>
				<td><input name="sortorder" type="number" value="<?=$material['sortorder'] ?>" /></td>
			</tr>
		</table>

		Description:<br />
		<textarea name="description"><?=$material['description'] ?></textarea/>
		<br />
		<input name="material_id" type="hidden" value="<?=$material['id'] ?>" />
		<input name="submit" type="submit" value="Update" class="faux-button" />
	</form>
</div>