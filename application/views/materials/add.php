<div id="bluesmith-wrapper">
	<h2>Add print material</h2>

	<form name="material-add" action="<?=site_url('materials/add_commit') ?>" method="post">
		<table class="keyval-table">
			<tr>
				<th>Method</th>
				<td>
					<select name="method_id">
<?php
foreach ($methods as $method_id):
?>
						<option value="<?=$method_id ?>"><?=$this->Method->name($method_id) ?></option>
<?php
endforeach;
?>
					</select>
				</td>
			<tr>
				<th>Name</th>
				<td><input name="name" type="text" /></td>
			</tr>
			<tr>
				<th>Display order</th>
				<td><input name="sortorder" type="number" value="0" /></td>
			</tr>
		</table>

		Description:<br />
		<textarea name="description"></textarea/>
		<br />
		<input name="submit" type="submit" value="Add" class="faux-button" />
	</form>