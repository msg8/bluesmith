
				</div> <!--end #main-content -->
			</div> <!--end #main -->
		</div> <!-- end #main-container -->

        <div id="l-ss-footer-container" role="contentinfo">
            <footer class="l-ss-wrapper clearfix">
				<img src="<?=base_url() ?>assets/company/images/footer-logo.png" alt="footer-logo">
				<address class="ss-address">
					<?=$this->Setting->get("Company name") ?> &nbsp; 
					<?=$this->Setting->get("Company city") ?> &nbsp; 
					<a href="tel:<?=preg_replace('/\D/','',$this->Setting->get("Company phone")) ?>"><?=$this->Setting->get("Company phone") ?></a>
				</address>

				Bluesmith <?=$this->Setting->get("System version") ?>
            </footer>
        </div>

        <script src="<?=base_url() ?>assets/company/js/main.js"></script>
    </body>
</html>