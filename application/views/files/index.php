<div id="bluesmith-wrapper">
	<h2>My files</h2>
	<p><a href="<?=site_url('files/add') ?>" class="faux-button">Add files</a></p>

<?php
if (empty($files)):
?>
	<p>You don&rsquo;t have any files. <a href="<?=site_url("files/add") ?>">Add files now</a>.</p>
<?php
else:
?>
	<table class="data-table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Filename</th>
				<th>Type</th>
				<th>Size</th>
				<th>Created</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
<?php
	$shaded = "";
	foreach ($files as $file_id):
		$file = $this->File->get($file_id);
		
		if ($file['filesize']>10000):
			$size = round($file['filesize']/1024,1);
			$unit = "mb";
		else:
			$size = $file['filesize'];
			$unit = "kb";
		endif;
?>
			<tr class="<?=$shaded ?>">
				<td>
					<form name="file-edit-<?=$file['id'] ?>" action="<?=site_url("files/edit_commit") ?>" method="post" class="edit-<?=$file['id'] ?>">
						<input name="name" type="text" value="<?=$file['name'] ?>" />
						<input name="file_id" type="hidden" value="<?=$file['id'] ?>" />
						<br />
						<input name="submit" type="submit" value="Rename" />
						<input name="cancel" type="reset" value="Cancel" onclick="$('.edit-<?=$file['id'] ?>').toggle(200); return false;" />
					</form>
					<a href="#" onclick="$('.edit-<?=$file['id'] ?>').toggle(200); return false;" class="edit-<?=$file['id'] ?>"><?=$file['name'] ?></a>
				</td>
				<td><?=$file['clientname'] ?></td>
				<td><?=$file['filetype'] ?></td>
				<td><?=$size ?> <?=$unit ?></td>
				<td><?=date("n/j/Y, g:ia",strtotime($file['created_at'])) ?></td>
				<td><a href="<?=site_url('files/preview/'.$file['id']) ?>" target="_blank">Preview</a></td>
				<td><a href="<?=site_url("files/remove/".$file['id']) ?>">Remove</a></td>
			</tr>
<?php
		$shaded = ($shaded)? "":"shaded";
	endforeach;
?>
			</tbody>
		</table>
<?php
endif;
?>

	<p><a href="<?=site_url('jobs/add') ?>" class="faux-button">Submit a job</a></p>
</div>
