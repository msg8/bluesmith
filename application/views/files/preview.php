<?php
// from threejs.org's example "STL loader"
// https://github.com/mrdoob/three.js/blob/master/examples/webgl_loader_stl.html

// appearance
$plane = "999999";
$fog = "988675";
$material = "0680CD";
$shininess = 10;
$specular = "111111";
$light1 = "666699";
$light2 = "111133";
$shadow1 = "FFFFFF";
$shadow2 = "FFAA00";
$font = "FFFFFF";

?><!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bluesmith | STL File Preview</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<style>
			body {
				font-family: Monospace;
				background-color: #000000;
				margin: 0px;
				overflow: hidden;
			}
			#info {
				color: #<?=$font ?>;
				position: absolute;
				top: 10px;
				width: 100%;
				text-align: center;
				z-index: 100;
				display:block;
			}
			a { color: skyblue }
			.button { background:#999; color:#eee; padding:0.2em 0.5em; cursor:pointer }
			.highlight { background:orange; color:#fff; }
			span {
				display: inline-block;
				width: 60px;
				float: left;
				text-align: center;
			}
		</style>
	</head>
	<body>
		<div id="info">
			<?=$file['name'] ?><br />
			<?=$file['clientname'] ?><br />
			Zoom:
			<span onclick="zoom('in');">+</span>
			&nbsp;
			<span onclick="zoom('out');">-</span>
		</div>

		<script src="<?=base_url() ?>assets/three.js/build/three.min.js"></script>
		<script src="<?=base_url() ?>assets/three.js/examples/js/loaders/STLLoader.js"></script>
		<script src="<?=base_url() ?>assets/three.js/examples/js/Detector.js"></script>
		<script src="<?=base_url() ?>assets/three.js/examples/js/libs/stats.min.js"></script>

		<script>
			if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
			var container, stats;
			var camera, cameraTarget, scene, renderer;
			init();
			animate();
			
			function init() {
				container = document.createElement( 'div' );
				document.body.appendChild( container );
				camera = new THREE.PerspectiveCamera( 35, window.innerWidth / window.innerHeight, 1, 15 );
				camera.position.set( 3, 0.15, 3 );
				cameraTarget = new THREE.Vector3( 0, -0.25, 0 );
				scene = new THREE.Scene();
				scene.fog = new THREE.Fog( 0x<?=$fog ?>, 1, 15 );
				// Ground
				var plane = new THREE.Mesh(
					new THREE.PlaneBufferGeometry( 40, 40 ),
					new THREE.MeshPhongMaterial( { color: 0x<?=$plane ?>, specular: 0x<?=$specular ?> } )
				);
				plane.rotation.x = -Math.PI/2;
				plane.position.y = -0.5;
				scene.add( plane );
				plane.receiveShadow = true;
				
				var loader = new THREE.STLLoader();
				loader.load( "<?=base_url() ?>assets/files/<?=$file['filename'] ?>", function ( geometry ) {
					var material = new THREE.MeshPhongMaterial( { color: 0x<?=$material ?>, specular: 0x<?=$specular ?>, shininess: <?=$shininess ?> } );
					var mesh = new THREE.Mesh( geometry, material );
					mesh.position.set( 0, 0, 0 );
					mesh.rotation.set( 0, 0, 0 );
					mesh.scale.set( .01, .01, .01 );
					mesh.castShadow = true;
					mesh.receiveShadow = true;
					scene.add( mesh );
				} );
				
				// Lights
				scene.add( new THREE.HemisphereLight( 0x<?=$light1 ?>, 0x<?=$light2 ?> ) );
				addShadowedLight( 1, 1, 1, 0x<?=$shadow1 ?>, 1.35 );
				addShadowedLight( 0.5, 1, -1, 0x<?=$shadow2 ?>, 1 );
				// renderer
				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setClearColor( scene.fog.color );
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				renderer.gammaInput = true;
				renderer.gammaOutput = true;
				renderer.shadowMap.enabled = true;
				renderer.shadowMap.renderReverseSided = false;
				container.appendChild( renderer.domElement );
				// stats
				stats = new Stats();
				container.appendChild( stats.dom );
				//
				window.addEventListener( 'resize', onWindowResize, false );
			}
			function addShadowedLight( x, y, z, color, intensity ) {
				var directionalLight = new THREE.DirectionalLight( color, intensity );
				directionalLight.position.set( x, y, z );
				scene.add( directionalLight );
				directionalLight.castShadow = true;
				var d = 1;
				directionalLight.shadow.camera.left = -d;
				directionalLight.shadow.camera.right = d;
				directionalLight.shadow.camera.top = d;
				directionalLight.shadow.camera.bottom = -d;
				directionalLight.shadow.camera.near = 1;
				directionalLight.shadow.camera.far = 4;
				directionalLight.shadow.mapSize.width = 1024;
				directionalLight.shadow.mapSize.height = 1024;
				directionalLight.shadow.bias = -0.005;
			}
			function onWindowResize() {
				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();
				renderer.setSize( window.innerWidth, window.innerHeight );
			}
			function animate() {
				requestAnimationFrame( animate );
				render();
				stats.update();
			}
			function render() {
				var timer = Date.now() * 0.0005;
				camera.position.x = Math.cos( timer ) * 3;
				camera.position.y = Math.cos( timer ) * .3 + .5;
				camera.position.z = Math.sin( timer ) * 3;
				camera.lookAt( cameraTarget );
				renderer.render( scene, camera );
			}
		</script>
	</body>
</html>