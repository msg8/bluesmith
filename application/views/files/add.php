<div id="bluesmith-wrapper">
	<h2>Add files</h2>

	<form name="files-add" action="<?=site_url("files/add_commit") ?>" method="post" enctype="multipart/form-data">
		<div id="files-wrapper">
			<input name="names[]" type="text" placeholder="Name" /><input name="filenames[]" type="file" /><br />
		</div>
		<p><a href="#" onclick="addFile(); return false;">Add another</a></p>
		
		<input name="source" type="hidden" value="<?=$source ?>" />
		<input name="submit" type="submit" value="Upload" class="faux-button" />
	</form>
	
	<br />
	<hr />
	
	<p class="note">
		Name your file such that no PHI is included in the title.
		If you are concerned with HIPAA, and are uncomfortable uploading your file to
		OIT&rsquo;s server, upload your file instead to a Box folder and email a link
		to <a href="mailto:<?=$this->Setting->get('System email') ?>"><?=$this->Setting->get('System email') ?></a> for an estimate request.
	</p>
</div>