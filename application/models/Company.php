<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_MODEL {

	public function __construct() {

	}

	// get any users updated in the last hour and import the changes
	function users_import() {
		$this->load->model("User");

/*		
		$users = file_get_contents(USER_API);
		$users = json_decode($users,true);
		foreach ($users as $user_id):
			// get user info
			$user = file_get_contents(USER_API);
			$user = json_decode($user,true);
			
			// build the row
			$fields = array("id","netid","password","ldapkey","cardid","uniqueid","email","firstname",
				"lastname","nickname","title","phone","address","affiliation","association","career",
				"program","plan","ou","checksum","active","created_by","created_at","updated_at"
			);
			$row = array();
			foreach ($fields as $field):
				$row[$field] = $user[$field];
			endforeach;
			
			// check for local user
			if ($this->User->get($user_id)):
				// update the record
				$this->db->where("id",$user['id']);
				$this->db->limit(1);
				$this->db->update("users",$row);
			else:
				// make sure this is a valid user
				if (!empty($user['ldapkey'])):
					// add the record
					$row['role'] = "User";
					$this->db->insert("users",$row);
				endif;
			endif;
		endforeach;
*/
	}
	
	public function user_sync($user_id=false) {
		if (!is_numeric($user_id)):
			set_message("error","No user selected");
			return false;
		endif;
		$this->load->model("User");
		$user = $this->User->get($user_id);

/*		
		// get user info
		$user = file_get_contents(USER_API);
		if (empty($user)):
			set_message("error","Couldn't find that user!");
			return false;
		endif;
		$user = json_decode($user,true);
		
		// build the row
		$fields = array("id","netid","password","ldapkey","cardid","uniqueid","email","firstname",
			"lastname","nickname","title","phone","address","affiliation","association","career",
			"program","plan","ou","checksum","active","created_by","created_at","updated_at"
		);
		$row = array();
		foreach ($fields as $field):
			$row[$field] = $user[$field];
		endforeach;

		// check for local user
		if ($this->User->get($user_id)):
			// update the record
			$this->db->where("id",$user['id']);
			$this->db->limit(1);
			$this->db->update("users",$row);
			
			set_message("success","User updated from master record");
		else:
			// make sure this is a valid user
			if (!empty($user['ldapkey'])):
				// add the record
				$user['role'] = "User";
				$this->db->insert("users",$row);
				set_message("success","User added from master record");
			else:
				set_message("error","User has no LDAP entry");
				return false;
			endif;
		endif;
*/
		return true;
	}

}
