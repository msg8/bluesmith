<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_MODEL {

	public function __construct() {

	}
	// return a user
	public function get($user_id) {

		if (!is_numeric($user_id))
			return FALSE;		
		
		$this->db->select("*");
		$this->db->from("users");
		$this->db->where("id",$user_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
		// return a user ID by NetID
		public function get_by_netid($netid) {
			$netid = preg_replace('/[^a-z0-9]/i','',$netid);
			if (empty($netid))
				return FALSE;
			$this->db->select("id");
			$this->db->from("users");
			$this->db->where("netid",$netid);
			$this->db->order_by("active","desc");
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result))
				return FALSE;
			$result = $result[0];
			return $result['id'];
		}
		// return a user ID by UniqueID
		public function get_by_uniqueid($uniqueid) {
			if (empty($uniqueid))
				return FALSE;
			$this->db->select("id");
			$this->db->from("users");
			$this->db->where("uniqueid",$uniqueid);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result))
				return FALSE;
			return $result[0]['id'];
		}
		// return a user ID by checksum
		public function get_by_checksum($checksum) {
			if (empty($checksum))
				return FALSE;
			$this->db->select("id");
			$this->db->from("users");
			$this->db->where("checksum",$checksum);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result))
				return FALSE;
			return $result[0]['id'];
		}

	// return user's name
	public function name($user_id) {
		$user = $this->User->get($user_id);
		if (empty($user))
			return FALSE;
		if (!empty($user['nickname']))
			return $user['nickname']." ".$user['lastname'];
		else
			return $user['firstname']." ".$user['lastname'];
	}
			
	// return user's emails
	public function emails($user_id) {
		$this->db->select("email");
		$this->db->from("users_emails");
		$this->db->where("user_id",$user_id);
		$this->db->order_by("primary","desc");
		$query = $this->db->get();
		$result = $query->result_array();
		$emails = array();
		foreach ($result as $row)
			$emails[] = $row['email'];
		return $emails;
	}
	
	// return all users ever part of a job
	public function all() {
		$this->db->select("user_id");
		$this->db->from("jobs_users");
		$this->db->distinct();
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array(-1); //CodeIgniter where_in doesn't like empty arrays
		foreach ($result as $row)
			$ids[] = $row['user_id'];
		
		$this->db->select("*");
		$this->db->from("users");
		$this->db->where("active",1);
		$this->db->where_in("id",$ids);
		$this->db->order_by("lastname","asc");
		$this->db->order_by("firstname","asc");
		$query = $this->db->get();
		$result = $query->result_array();
		$users = array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
	
	// return jobs for a user
	public function jobs($user_id) {
		$this->db->select("job_id");
		$this->db->from("jobs_users");
		$this->db->where("user_id",$user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['job_id'];
		return $ids;	
	}
	
	// return files for a user
	public function files($user_id) {
		$this->db->select("id");
		$this->db->from("files");
		$this->db->where("created_by",$user_id);
		$this->db->where("status","Active");
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
	
	// return credit balance for a user, FALSE for none recorded
	public function balance($user_id) {
		$user = $this->User->get($user_id);
		if (empty($user))
			return FALSE;
		return $user['balance'];
	}
	
	public function impersonate($user_id) {
		unset($_SESSION['url']);
		$_SESSION['user_id'] = $user_id;
	}

}

