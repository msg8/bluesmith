<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_MODEL {

	public function __construct() {

	}

	// return a payment
	public function get($payment_id) {
		$this->db->select("*");
		$this->db->from("payments");
		$this->db->where("id",$payment_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
	
	// return payment type => display name
	public function types() {
		$types = array(
			"creditcard" => "Credit card",
			"flexaccount" => "FLEX account",
			"fundcode" => "Fundcode",
			"invoice" => "Med Center invoice",
			"bluechips" => "Bluechips credit"
		);
		return $types;
	}
}
