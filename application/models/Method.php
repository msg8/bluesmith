<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Method extends CI_MODEL {

	public function __construct() {

	}

	// return a method
	public function get($method_id) {
		$this->db->select("*");
		$this->db->from("methods");
		$this->db->where("id",$method_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
		// return a method
		public function get_by_name($name) {
			$this->db->select("*");
			$this->db->from("methods");
			$this->db->where("name",$name);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result))
				return FALSE;
			$result = $result[0];
			return $result;
		}
	
	public function name($method_id) {
		$method = $this->Method->get($method_id);
		return $method['name'];	
	}

	// return all method IDs
	public function all() {
		$this->db->select("id");
		$this->db->from("methods");
		$this->db->where("status","Active");
		$this->db->order_by("sortorder","asc");
		$this->db->order_by("name","asc");
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
	
	// return any material options for this method
	public function materials($method_id) {
		$this->db->select("id");
		$this->db->from("materials");
		$this->db->where("method_id",$method_id);
		$this->db->where("status","Active");
		$this->db->order_by("name","asc");
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
}
