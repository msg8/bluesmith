<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends CI_MODEL {

	public function __construct() {

	}

	// return a section
	public function get($section_id) {
		$this->db->select("*");
		$this->db->from("sections");
		$this->db->where("id",$section_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
		// return a section by its name
		public function get_by_name($name) {
			$this->db->select("*");
			$this->db->from("sections");
			$this->db->where("name",$name);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result))
				return FALSE;
			$result = $result[0];
			return $result;
		}
	
	public function name($section_id) {
		$section = $this->Section->get($section_id);
		return $section['name'];	
	}
	
	public function content($name) {
		$section = $this->Section->get_by_name($name);
		return $section['content'];
	}
	
	// return all section IDs
	public function all() {
		$this->db->select("id");
		$this->db->from("sections");
		$this->db->order_by("name","asc");
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
}
