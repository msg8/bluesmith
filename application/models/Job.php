<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job extends CI_MODEL {

	public function __construct() {

	}
/*
Job stages:
1 - Submitting
2 - Estimating
3 - Quoting
4 - Approving
5 - Printing
6 - Paying
7 - Receiving
8 - Archiving
*/

	// return a job
	public function get($job_id) {
		if (!is_numeric($job_id))
			return FALSE;		
		
		$this->db->select("*");
		$this->db->from("jobs");
		$this->db->where("id",$job_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
		public function name($job_id) {
			$job = $this->Job->get($job_id);
			return $job['name'];	
		}

	// return all job IDs
	public function all() {
		$this->db->select("id");
		$this->db->from("jobs");
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
		// return active job IDs
		public function active() {
			$this->db->select("id");
			$this->db->from("jobs");
			$this->db->where("status","Active");
			$query = $this->db->get();
			$result = $query->result_array();
			$ids = array();
			foreach ($result as $row)
				$ids[] = $row['id'];
			return $ids;	
		}
		
		// return job IDs for jobs awaiting staff inut
		public function actions() {
			$this->db->select("id");
			$this->db->from("jobs");
			$this->db->where("status","Active");
			$this->db->where_in("stage",array(2,3,5));
			$query = $this->db->get();
			$result = $query->result_array();
			$ids = array();
			foreach ($result as $row)
				$ids[] = $row['id'];
			return $ids;	
		}
	
	// return any users associated with this job
	public function users($job_id) {
		$this->db->select("user_id");
		$this->db->from("jobs_users");
		$this->db->where("job_id",$job_id);
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['user_id'];
		return $ids;	
	}
		// return any users associated with this job with email enabled
		public function recipients($job_id) {
			$this->db->select("user_id");
			$this->db->from("jobs_users");
			$this->db->where("job_id",$job_id);
			$this->db->where("email",1);
			$query = $this->db->get();
			$result = $query->result_array();
			$ids = array();
			foreach ($result as $row)
				$ids[] = $row['user_id'];
			return $ids;	
		}

	// return any files for this job
	public function files($job_id) {
		$this->db->select("file_id");
		$this->db->from("files_jobs");
		$this->db->where("job_id",$job_id);
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['file_id'];
		return $ids;	
	}

	// calculates cost for this job based on charges; optionally, the unpaid portion of the cost
	public function cost($job_id,$remaining = FALSE) {
		$this->load->model("Charge");
		$cost = 0;
		foreach ($this->Job->charges($job_id) as $charge_id):
			$charge = $this->Charge->get($charge_id);
			$cost += $charge['amount'];
		endforeach;
		
		if (!$remaining)
			return $cost;
			
		// less any payments
		foreach ($this->Job->payments($job_id) as $payment)
			$cost -= $payment['amount'];
		
		return $cost;
	}
	// return any charges for this job
	public function charges($job_id) {
		$this->db->select("id");
		$this->db->from("charges");
		$this->db->where("job_id",$job_id);
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}

	// return any payments for this job
	public function payments($job_id) {
		$this->db->select("id");
		$this->db->from("payments");
		$this->db->where("job_id",$job_id);
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}

	// return any comments for this job
	public function comments($job_id) {
		$limited = !has_access("proctor");
		
		$this->db->select("id");
		$this->db->from("comments");
		$this->db->where("job_id",$job_id);
		if ($limited)
			$this->db->where("internal",0);
		$this->db->order_by("created_at","asc");
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
		
}
