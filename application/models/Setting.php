<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_MODEL {

/*
Settings exist at three tiered levels: Session, User, Global
	Global settings cannot be overriden by users
	User settings automatically write back to the database to persist between sessions
	Session settings only last as long as a session
Gets return FALSE for missing/unmatched settings, and NULL or 0 for empty values
Sets use boolean FALSE to remove values, NULL or 0 for empty values
*/
	public function __construct() {

	}
	
	// get a setting - checks session, then user, then global
	public function get($name=FALSE) {
		if (empty($name))
			return FALSE;
		
		$scope = $this->Setting->scope($name);
		// global settings cannot be overridden
		if ($scope=="Global"):
			$content = $this->Setting->get_global($name);
			return $content;
		endif;
		
		$content = $this->Setting->get_session($name);
		if ($content!==FALSE)
			return $content;

		$content = $this->Setting->get_user($name);
		if ($content!==FALSE)
			return $content;

		$content = $this->Setting->get_global($name);
		return $content;
	}
		
		// check if there is a $_SESSION entry
		public function get_session($name=FALSE) {
			if (empty($name))
				return FALSE;
				
			return isset($_SESSION[$name])? $_SESSION[$name]:FALSE;
		}
		
		// checks the database for a user-defined setting
		public function get_user($name=FALSE, $user_id=FALSE) {
			if (empty($name))
				return FALSE;
			
			// make sure the setting exists
			$setting_id = $this->Setting->name2id($name);
			if (empty($setting_id))
				return FALSE;

			if (!is_numeric($user_id) && function_exists("me"))
				$user_id = me();
			if (!is_numeric($user_id))
				return FALSE;
				
			// check for a user setting
			$this->db->select("content");
			$this->db->from("settings_users");
			$this->db->where("setting_id",$setting_id);
			$this->db->where("user_id",$user_id);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result))
				return FALSE;
			return $result[0]['content'];
		}
		
		// checks the database for the default global value
		public function get_global($name=FALSE) {
			if (empty($name))
				return FALSE;
			
			$this->db->select("content");
			$this->db->from("settings");
			$this->db->where("name",$name);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result))
				return FALSE;
			return $result[0]['content'];
		}

	// change a setting
	// user scoped settings write back to database
	// global updates initial value in settings table
	public function set($name=FALSE,$content=FALSE) {
		if (empty($name))
			return FALSE;

		$scope = $this->Setting->scope($name);
		switch ($scope):
			case "Global":
				$this->Setting->set_global($name,$content);
			break;
			
			// user settings change both session and user (back to database)
			case "User":
				$this->Setting->set_session($name,$content);
				$this->Setting->set_user($name,$content);
			break;
			
			case "Session":
				$this->Setting->set_session($name,$content);
			break;
			
			// something borked
			default:
				return FALSE;
		endswitch;
		
		return TRUE;
	}
		// change a session setting
		public function set_session($name=FALSE,$content=FALSE) {
			if (empty($name))
				return FALSE;
			
			if ($content===FALSE)
				unset($_SESSION[$name]);
			else
				$_SESSION[$name] = $content;
		}

		// change a user setting, defaults to current user
		public function set_user($name=FALSE, $content=FALSE, $user_id=FALSE) {
			if (empty($name))
				return FALSE;
				
			// make sure the setting exists
			$setting_id = $this->Setting->name2id($name);
			if (empty($setting_id))
				return FALSE;
			
			if (!is_numeric($user_id) && function_exists("me"))
				$user_id = me();
			if (!is_numeric($user_id))
				return FALSE;
				
			// remove any existing setting content
			$this->db->where("user_id",$user_id);
			$this->db->where("setting_id",$setting_id);
			$this->db->delete("settings_users");
				
			// if content is strictly FALSE, we're done
			if ($content===FALSE)
				return TRUE;
				
			// build the row
			$row = array(
				"setting_id" => $setting_id,
				"user_id" => $user_id,
				"content" => $content
			);
			$this->db->insert("settings_users",$row);
		}

		// change a global setting (updates initial value in settings table)
		public function set_global($name=FALSE,$content=FALSE) {
			if (empty($name))
				return FALSE;
				
			// make sure the setting exists
			$setting_id = $this->Setting->name2id($name);
			if (empty($setting_id))
				return FALSE;
			
			// update settings table
			$this->db->where("id",$setting_id);
			$this->db->limit(1);
			$this->db->update("settings",array("content"=>$content));
		}

	// returns the scope for a setting
	public function scope($name=FALSE) {
		if (empty($name))
			return FALSE;

		// make sure the setting exists
		$setting_id = $this->Setting->name2id($name);
		if (empty($setting_id))
			return FALSE;

		$this->db->select("scope");
		$this->db->from("settings");
		$this->db->where("id",$setting_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		return $result[0]['scope'];
	}
	
	// convert a setting name to an ID - internal use only
	private function name2id($name=FALSE) {
		if (empty($name))
			return FALSE;
			
		$this->db->select("id");
		$this->db->from("settings");
		$this->db->where("name",$name);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			die("Attempted to get non-existent setting: ".$name);
		return $result[0]['id'];
	}

}
