<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends CI_MODEL {

	public function __construct() {

	}

	// return a comment
	public function get($comment_id) {
		if (!is_numeric($comment_id))
			return FALSE;		
		
		$this->db->select("*");
		$this->db->from("comments");
		$this->db->where("id",$comment_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
}
