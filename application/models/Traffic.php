<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Traffic extends CI_MODEL {

	public function __construct() {

	}
	
	public function get($traffic_id) {
		if (!is_numeric($traffic_id))
			return FALSE;
		$this->db->select("*");
		$this->db->from("traffic");
		$this->db->where("id",$traffic_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		return $result[0];
	}
	
	// search for a similar record to increase the tally
	public function similar($traffic) {
		// validate data
		if (empty($traffic['date']) OR empty($traffic['class']) OR empty($traffic['method']))
			return FALSE;
		
		$this->db->select("id");
		$this->db->from("traffic");
		$this->db->where("date",$traffic['date']);
		$this->db->where("class",$traffic['class']);
		$this->db->where("method",$traffic['method']);
		if (isset($traffic['item']))
			$this->db->where("item",$traffic['item']);
		if (isset($traffic['user_id']))
			$this->db->where("user_id",$traffic['user_id']);
		if (isset($traffic['ip_address']))
			$this->db->where("ip_address",$traffic['ip_address']);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return false;
		return $result[0]['id'];
	}
	
	// add a record, or increase the tally on an existing one
	public function record($traffic) {
		// validate data
		if (empty($traffic['class']) OR empty($traffic['method']))
			return FALSE;
		
		// complete the record
		$traffic['date'] = date("Y-m-d");
		$traffic['tally'] = 1;
		if (isset($traffic['ip_address']))
			$traffic['ip_address'] = ip2long($traffic['ip_address']);
		
		// check for an existing similar record
		if ($traffic_id = $this->Traffic->similar($traffic)):
			$traffic = $this->Traffic->get($traffic_id);
			$this->db->where("id",$traffic['id']);
			$this->db->update("traffic",array("tally"=>$traffic['tally']+1));
			return $traffic['id'];
		else:
			// perform the insert
			$this->db->insert("traffic",$traffic);
			return $this->db->insert_id();
		endif;
	}

}
