<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Material extends CI_MODEL {

	public function __construct() {

	}

	// return a material
	public function get($material_id) {
		$this->db->select("*");
		$this->db->from("materials");
		$this->db->where("id",$material_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
	
	public function name($material_id) {
		$material = $this->Material->get($material_id);
		return $material['name'];	
	}
	
	public function method($material_id) {
		$this->load->model("Method");
		$material = $this->Material->get($material_id);
		return $this->Method->name($material['method_id']);
	}
	
	// return all material IDs
	public function all() {
		$this->db->select("id");
		$this->db->from("materials");
		$this->db->where("status","Active");
		$this->db->order_by("sortorder","asc");
		$this->db->order_by("name","asc");
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
}
