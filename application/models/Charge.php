<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Charge extends CI_MODEL {

	public function __construct() {

	}

	// return a charge
	public function get($charge_id) {
		$this->db->select("*");
		$this->db->from("charges");
		$this->db->where("id",$charge_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
	
	public function name($charge_id) {
		$charge = $this->Charge->get($charge_id);
		return $charge['name'];	
	}

}
