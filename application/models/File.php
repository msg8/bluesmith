<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_MODEL {

	public function __construct() {

	}

	// return a file
	public function get($file_id) {
		if (!is_numeric($file_id))
			return FALSE;		
		
		$this->db->select("*");
		$this->db->from("files");
		$this->db->where("id",$file_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
	
	public function name($file_id) {
		$file = $this->File->get($file_id);
		return $file['name'];	
	}
	
	// return any jobs this file is a part of
	public function jobs($file_id) {
		$this->db->select("job_id");
		$this->db->from("files_jobs");
		$this->db->where("file_id",$file_id);
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['job_id'];
		return $ids;	
	}
}
