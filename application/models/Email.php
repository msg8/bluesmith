<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_MODEL {

	public function __construct() {

	}

	// return an email
	public function get($email_id) {
		if (!is_numeric($email_id))
			return FALSE;		
		
		$this->db->select("*");
		$this->db->from("emails");
		$this->db->where("id",$email_id);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		if (empty($result))
			return FALSE;
		$result = $result[0];
		return $result;
	}
		// return an email by its name
		public function get_by_name($name) {
			$this->db->select("*");
			$this->db->from("emails");
			$this->db->where("name",$name);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result))
				return FALSE;
			$result = $result[0];
			return $result;
		}
	
	// return all email IDs
	public function all() {
		$this->db->select("id");
		$this->db->from("emails");
		$this->db->order_by("name","asc");
		$query = $this->db->get();
		$result = $query->result_array();
		$ids = array();
		foreach ($result as $row)
			$ids[] = $row['id'];
		return $ids;	
	}
	
	public function name($email_id) {
		$email = $this->Email->get($email_id);
		return $email['name'];	
	}
	
	public function send($job_id,$name) {
		$email = $this->Email->get_by_name($name);
		if (empty($email))
			return FALSE;
		
		if (empty($email['target']))
			return FALSE;
			
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job))
			return FALSE;
		
		$this->load->model("User");
		$this->load->model("Method");
		$this->load->model("Material");
		
		$material = $this->Material->get($job['material_id']);
		$method = $this->Method->get($material['method_id']);
		
		$services = array();
		if ($job['premium'])
			$services[] = "Premium service";
		if ($job['inspection'])
			$services[] = "Inspection scan";
		
		// determine content based on email type
		$content = $email['content']."\r\n\r\n";
		switch ($email['name']):
			case "Approved":
			
			break;
			
			case "Cancelled":		
				$content .= "Reactivate job: ".site_url("jobs/cancel/".$job['id'])."\r\n";
			break;
			
			case "Declined":
				// include payment link
				$content .= "Pay now: ".site_url("jobs/pay/".$job['id'])."\r\n";
			break;
			
			case "Paid":
				$payments = $this->Job->payments($job['id']);
				$this->load->model("Payment");
				foreach ($payments as $payment_id):
					$payment = $this->Payment->get($payment_id);
					$pending = ($payment['type']=="creditcard" && empty($payment['processed_at']))? " (pending)":"";
					$content .= $this->User->name($payment['created_by'])." paid $".number_format($payment['amount'],2). " by ".$payment['type'].$pending.", reference #".$payment['id']."\r\n";
				endforeach;
				$content .= "\r\n";
			break;
			
			case "Purchase":
			case "Printed":
				// if job has not been paid for include payment links
				$payments = $this->Job->payments($job['id']);
				if (empty($payments)):
					$content .= "Pay now: ".site_url("jobs/pay/".$job['id'])."\r\n";
				endif;
				$content .= "\r\n";
			break;
			
			case "Quoted":
				$content .= "Review quote: ".site_url("jobs/approve/".$job['id'])."\r\n";
			break;
			
			case "Received":
			
			break;
		
			case "Reminder":
/*
Job stages:
1 - Submitting
2 - Estimating
3 - Quoting
4 - Approving
5 - Printing
6 - Paying
7 - Receiving
8 - Archiving
*/
				switch ($job['stage']):
					case 2:
					case 3:
						$content .= "This job needs charges set and a quote sent to the clients."."\r\n\r\n";
						$content .= "Adjust charges: ".site_url("charges/job/".$job['id'])."\r\n";
						$email['target'] = "staff";
					break;
					
					case 4:
						$content .= "Your job has been quoted and is awaiting your approval before processing can begin."."\r\n\r\n";
						$content .= "Review quote: ".site_url("jobs/approve/".$job['id'])."\r\n";
						$email['target'] = "client";
					break;
					
					case 5:
						$content .= "This job was approved ".timespan(strtotime($job['approved_at']),time())." ago. ";
						$content .= "Please begin printing, or contact the client with an update."."\r\n\r\n";
						$email['target'] = "staff";
					break;
					
					case 6:
						$content .= "Your job is done printing and is awaiting your payment."."\r\n\r\n";
						$content .= "Pay now: ".site_url("jobs/pay/".$job['id'])."\r\n";
						$email['target'] = "client";
					break;
					
					case 7:
						$content .= "Your job is ready for pickup. Please come by the Technology Engagement Center to receive it."."\r\n\r\n";
						$email['target'] = "client";
					break;
					
					default:
						return FALSE;

				endswitch;
				
			break;
			
			case "Submitted":
			
			break;
			
			case "Updated":
			
			break;
			
			case "Commented":
				// add content from most recent comment
				$this->load->model("Comment");
				$this->db->select("*");
				$this->db->from("comments");
				$this->db->where("job_id",$job['id']);
				$this->db->order_by("created_at", "desc");
				$this->db->limit(1);
				$query = $this->db->get();
				$result = $query->result_array();
				$comment = $result[0];
				
				// if comment was internal, add note and switch it only to send to staff
				if ($comment['internal']):
					$content .= "(This comment is hidden from the client.)"."\r\n\r\n";
					$email['target'] = "staff";
				endif;
				
				$content .= $comment['content']."\r\n";
				$content .= "-".$this->User->name($comment['created_by']);
				$content .= "\r\n\r\n";
				
			break;
		
			default:
				return FALSE;
		endswitch;
		
		if ($job['source']=="Purchase"):
			$content .= "===== Purchase details ====="."\r\n";
			$content .= "Reference #: ".$job['id']."\r\n";
			$content .= "Name: ".$job['name']."\r\n";
			$content .= "Charge: $".number_format($job['quote'],2)."\r\n";
			$content .= "Material: ".$this->Material->name($material['id'])."\r\n";
			$content .= "Submitted by: ".$this->User->name($job['created_by'])."\r\n";
			$content .= "Submitted at: ".date("n/j/Y, g:ia",strtotime($job['created_at']))."\r\n";
			$content .= "Notes: "."\r\n";
			$content .= $job['instructions'];
			
		else:
			$content .= "View job: ".site_url("jobs/show/".$job['id'])."\r\n\r\n";

			$content .= "===== Job details ====="."\r\n";
			$content .= "Reference #: ".$job['id']."\r\n";
			$content .= "Name: ".$job['name']."\r\n";
			if (!empty($job['quoted_at']))
				$content .= "Quote: $".number_format($job['quote'],2)."\r\n";
			$content .= "Method: ".$this->Method->name($method['id'])."\r\n";
			$content .= "Material: ".$this->Material->name($material['id'])."\r\n";
			$content .= "Services: ";
			if (empty($services))
				$content .= "No extras"."\r\n";
			else
				$content .= implode(", ",$services)."\r\n";
			$content .= "Submitted by: ".$this->User->name($job['created_by'])."\r\n";
			$content .= "Submitted at: ".date("n/j/Y, g:ia",strtotime($job['created_at']))."\r\n";
			$content .= "Last update: ".timespan(strtotime($job['updated_at']),time())."\r\n";
		endif;
		
		// determine email(s) to send to
		if ($email['target']=="staff"):
			$recipient = $this->Setting->get("System email");
			$cc = FALSE;
		elseif ($email['target']=="client"):
			// get client(s) for this job
			$users = $this->Job->recipients($job['id']);
			
			$recipient = array();
			foreach ($users as $user_id):
				$user = $this->User->get($user_id);
				if (filter_var($user['email'],FILTER_VALIDATE_EMAIL)):
					$recipient[] = $user['email'];
				endif;
			endforeach;
			$recipient = implode(", ",$recipient);
			$cc = $this->Setting->get("System email");;
		endif;
		
		if (empty($recipient))
			return FALSE;
		
		$this->load->library('email');
		$this->email->from($this->Setting->get("System email"));
		$this->email->to($recipient);
		if (!empty($cc))
			$this->email->cc($cc);
		$this->email->subject($this->Setting->get("System prefix").$email['subject']);
		$this->email->message($content);
		$result = $this->email->send();
		
		if (empty($result))
			return FALSE;
		
		// record it in the database
		$row = array(
			"email_id" => $email['id'],
			"job_id" => $job['id'],
			"recipient" => $recipient,
			"created_by" => me(),
			"created_at" => date("Y-m-d H:i:s")
		);
		$this->db->insert("emails_jobs",$row);
		
		return $row;
	}
	
}

