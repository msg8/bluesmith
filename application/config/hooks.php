<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['post_controller_constructor'][] = array(
	'class'    => null,
	'function' => 'url_forward',
	'filename' => 'auth_helper.php',
	'filepath' => 'helpers',
	'params'   => null
);
$hook['post_controller_constructor'][] = array(
	'class'    => null,
	'function' => 'auth',
	'filename' => 'auth_helper.php',
	'filepath' => 'helpers',
	'params'   => null
);
$hook['post_controller_constructor'][] = array(
	'class'    => null,
	'function' => 'traffic_record',
	'filename' => 'traffic_helper.php',
	'filepath' => 'helpers',
	'params'   => null
);