<?php
function cyber_profile_id() {
	return (CYBER_MODE_TEST)? CYBER_PROFILE_ID_TEST:CYBER_PROFILE_ID;
}
function cyber_access_key() {
	return (CYBER_MODE_TEST)? CYBER_ACCESS_KEY_TEST:CYBER_ACCESS_KEY;
}
function cyber_secret_key() {
	return (CYBER_MODE_TEST)? CYBER_SECRET_KEY_TEST:CYBER_SECRET_KEY;
}

function cyber_form_action() {
	return (CYBER_MODE_TEST)? "https://testsecureacceptance.cybersource.com/pay":"https://secureacceptance.cybersource.com/pay";
}



/***** functions from CyberSource *****/
function sign ($params) {
	return signData(buildDataToSign($params), cyber_secret_key());
}

function signData($data, $secretKey) {
	return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
	$signedFieldNames = explode(",",$params["signed_field_names"]);
	foreach ($signedFieldNames as $field)
		$dataToSign[] = $field . "=" . $params[$field];
	return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
	return implode(",",$dataToSign);
}
/**************************************/