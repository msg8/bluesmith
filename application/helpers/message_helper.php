<?php
// add a message to be displayed to the user
function set_message($level, $text) {
	$obj =& get_instance();
	
	// get any preexisting messages
	$messages = $obj->session->userdata("messages");
	if (empty($messages))
		$messages = array();

	// add this message to the queue
	$messages[] = array($level=>$text);
	$messages = $obj->session->set_userdata("messages",$messages);
	
	return true;
}

// load any messages for the current session
function get_messages() {
	$obj =& get_instance();
	
	// get any existing messages
	$messages = $obj->session->userdata("messages");
	
	// clear the messages
	$obj->session->set_userdata("messages",array());
	
	return $messages;
}

?>