<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// write out include paths for relevant CSS & JavaScript files
function include_assets() {
	$obj =& get_instance();
	$obj->load->helper('file');

	// figure out the base path	
	$base_url = $obj->config->item('base_url');
	$asset_location = "assets/";
	$js_dir = $asset_location."js/";
	$css_dir = $asset_location."css/";
	
	$js_assets = array();
	$css_assets = array();
	
	
	// load the global assets
	if (is_dir($js_dir."application")):
		foreach (get_filenames($js_dir."application/") as $asset):
			$js_assets[] = $js_dir."application/".$asset;
		endforeach;
	endif;
	if (is_dir($css_dir."application")):
		foreach (get_filenames($css_dir."application/") as $asset):
			$css_assets[] = $css_dir."application/".$asset;
		endforeach;
	endif;
	
	// if we have an active class, try loading assets for that class
	$class = $obj->router->class;
	$method = $obj->router->method;

	if (is_dir($js_dir.$class)):
		// include the generic class file
		$js_assets[] = $js_dir.$class."/".$class.".js";
		// include the method class file
		$js_assets[] = $js_dir.$class."/".$method.".js";
	endif;
	if (is_dir($css_dir.$class)):
		// include the generic class file
		$css_assets[] = $css_dir.$class."/".$class.".css";
		// include the method css file
		$css_assets[] = $css_dir.$class."/".$method.".css";
	endif;
	
	// check for theme
	$theme = $obj->Setting->get("User theme");
	if (!is_file($css_dir."themes/".$theme.".css"))
		$theme = "Default";
	$css_assets[] = $css_dir."themes/".$theme.".css";
	
	foreach ($js_assets as $file):
		if (is_file($file))
			echo "	<script type='text/javascript' src='${base_url}${file}?".filemtime($file)."'></script>\r\n";
//			echo "	<script type='text/javascript' src='${base_url}${file}'></script>\r\n";
	endforeach;
	foreach ($css_assets as $file):
		if (is_file($file))
			echo "	<link rel='stylesheet' href='${base_url}${file}?".filemtime($file)."' type='text/css' />\r\n";
//			echo "	<link rel='stylesheet' href='${base_url}${file}' type='text/css' />\r\n";
	endforeach;
}