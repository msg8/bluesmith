<?php
// given two datetimes, make them display nice
// e.g.: "5/27, 11:30am - 2:30pm", or "7/1, 10 - 11:15pm"
function nicetime($date1,$date2) {
	// start with the first date
	$display = date("l n/j",strtotime($date1));
	
	// check if the dates span years
	if (date("Y",strtotime($date1))!=date("Y",strtotime($date2)))
		$display .= date("/Y",strtotime($date1));

	$display .= ", ";
	
	// add the hour
	$display .= date("g",strtotime($date1));

	// check if we need minutes
	if (date("i",strtotime($date1))!="00")
		$display .= date(":i",strtotime($date1));

	// check if it spans am-pm
	if (date("Y-m-d a",strtotime($date1))!=date("Y-m-d a",strtotime($date2)))
		$display .= date("a",strtotime($date1));

	$display .= "-";

	// check if the dates span days
	if (date("Y-m-d",strtotime($date1))!=date("Y-m-d",strtotime($date2))):
		$display .= date("n/j",strtotime($date2));

		// check if the dates span years
		if (date("Y",strtotime($date1))!=date("Y",strtotime($date2)))
			$display .= date("/Y",strtotime($date2));

		$display .= ", ";
	endif;
	
	// add the hour
	$display .= date("g",strtotime($date2));

	// check if we need minutes
	if (date("i",strtotime($date2))!="00")
		$display .= date(":i",strtotime($date2));

	// always have am/pm on second date
	$display .= date("a",strtotime($date2));
	
	return $display;
}

// given two dates, determine the appropriate "chunk" of time for date()
// used primarily for graph axes
function timechunk($date1,$date2) {
	$diff = abs(strtotime($date2)-strtotime($date1));

	// less than 12 days? give day of week
	if ($diff<60*60*24*12)
		return "D";

	// less than 1 year? give month day
	if ($diff<60*60*24*365)
		return "M j";

	// less than 2 years? give month
	if ($diff<60*60*24*365*2)
		return "M";

	// all the rest? give month/year
	return "n/y";
}

// modified from the original so as only to display 2 units max - my code at bottom
function timespan($seconds = 1, $time = '')
{
	$CI =& get_instance();
	$CI->lang->load('date');

	if ( ! is_numeric($seconds))
	{
		$seconds = 1;
	}

	if ( ! is_numeric($time))
	{
		$time = time();
	}

	if ($time <= $seconds)
	{
		$seconds = 1;
	}
	else
	{
		$seconds = $time - $seconds;
	}

	$str = '';
	$years = floor($seconds / 31536000);

	if ($years > 0)
	{
		$str .= $years.' '.$CI->lang->line((($years	> 1) ? 'date_years' : 'date_year')).', ';
	}

	$seconds -= $years * 31536000;
	$months = floor($seconds / 2628000);

	if ($years > 0 OR $months > 0)
	{
		if ($months > 0)
		{
			$str .= $months.' '.$CI->lang->line((($months	> 1) ? 'date_months' : 'date_month')).', ';
		}

		$seconds -= $months * 2628000;
	}

	$weeks = floor($seconds / 604800);

	if ($years > 0 OR $months > 0 OR $weeks > 0)
	{
		if ($weeks > 0)
		{
			$str .= $weeks.' '.$CI->lang->line((($weeks	> 1) ? 'date_weeks' : 'date_week')).', ';
		}

		$seconds -= $weeks * 604800;
	}

	$days = floor($seconds / 86400);

	if ($months > 0 OR $weeks > 0 OR $days > 0)
	{
		if ($days > 0)
		{
			$str .= $days.' '.$CI->lang->line((($days	> 1) ? 'date_days' : 'date_day')).', ';
		}

		$seconds -= $days * 86400;
	}

	$hours = floor($seconds / 3600);

	if ($days > 0 OR $hours > 0)
	{
		if ($hours > 0)
		{
			$str .= $hours.' '.$CI->lang->line((($hours	> 1) ? 'date_hours' : 'date_hour')).', ';
		}

		$seconds -= $hours * 3600;
	}

	$minutes = floor($seconds / 60);

	if ($days > 0 OR $hours > 0 OR $minutes > 0)
	{
		if ($minutes > 0)
		{
			$str .= $minutes.' '.$CI->lang->line((($minutes	> 1) ? 'date_minutes' : 'date_minute')).', ';
		}

		$seconds -= $minutes * 60;
	}

	if ($str == '')
	{
		$str .= $seconds.' '.$CI->lang->line((($seconds	> 1) ? 'date_seconds' : 'date_second')).', ';
	}

/** my code **/
	$str = explode(",",$str,3);
	// drop all units besides the first two
	if (!empty($str[2]))
		unset($str[2]);
	$str = implode(" ",$str);
	return strtolower(trim($str));
/** end my code **/

}
