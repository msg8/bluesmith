<?php
/*
Job stages:
1 - Submitting
2 - Estimating
3 - Quoting
4 - Approving
5 - Printing
6 - Paying
7 - Receiving
8 - Archiving
*/
function stage_display($stage) {
	switch ($stage):
		case "1": return "Submitting"; break;
		case "2": return "Estimating"; break;
		case "3": return "Quoting"; break;
		case "4": return "Approving"; break;
		case "5": return "Printing"; break;
		case "6": return "Paying"; break;
		case "7": return "Receiving"; break;
		case "8": return "Archiving"; break;
		default: return false;
	endswitch;
}