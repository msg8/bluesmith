<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// return whether user is logged in
function is_authed() {
	$obj =& get_instance();
	if (empty($_SESSION[SYSTEM_PREFIX.'_logged_in']))
		return FALSE;
	else
		return $_SESSION[SYSTEM_PREFIX.'_logged_in'];
}

// check for login, then for cookie, then load user login page
function auth($depth = LOGIN_CASUAL) {
	$obj =& get_instance();

	// ignore auth requests if already on the auth page
	if ($obj->router->class=="auth")
		return FALSE;
	// don't check auth for payment returns
	if ($obj->router->class=="payments" && $obj->router->method=="creditcard_commit")
		return FALSE;
	// don't sections/index or sections/options
	if ($obj->router->class=="" || $obj->router->class=="sections")
		return FALSE;
	
	// check if the user is already logged in
	if (is_authed()):
		return TRUE;
	
	// don't check auth for automated tasks
	elseif ($obj->input->is_cli_request()):
		return TRUE;
	
	// send to Shib for authentication
	else:
		$obj->session->set_userdata("url",current_url());
		header("Location: /auth");
		die();
	endif;
}

// called from hooks, intercepts forward requests and redirects
function url_forward() {
	$obj =& get_instance();
	
	// check for a forward request
	@$url = $_SESSION["url"];
	if (filter_var($url, FILTER_VALIDATE_URL) && $obj->router->class!="auth"):
		unset($_SESSION["url"]);
		redirect($url);
		die();
	endif;
}

function logout() {
	$obj =& get_instance();
	
	if (is_authed()):
		$obj->session->sess_destroy();
		return TRUE;
	else:
		return FALSE;
	endif;
}
// return the current user ID
function me() {
	$obj =& get_instance();

	if (!is_authed())
		return FALSE;
	else
		return $obj->session->userdata("user_id");
}

// check if current user has specified rights
function has_access($request,$option=FALSE) {
	$obj =& get_instance();
	$obj->load->model("User");
	
	// check for a CLI cron request
	if ($request=="run task" && $obj->input->is_cli_request())
		return TRUE;
				
	// make sure user is authed
	if ($user_id = me()):
		$user = $obj->User->get($user_id);
		
		switch ($request):
			// always FALSE
			case "FALSE":
				return FALSE;
				break;
				
			case "show job":
				if (has_access("proctor")): return TRUE; endif;
				
				// check if user is associated with this job
				$jobs = $obj->User->jobs(me());
				return in_array($option,$jobs);
			break;
				
			case "jvs":
				if (has_access("administration")): return TRUE; endif;
				
				// check for individual user permission
				$obj->db->select("*");
				$obj->db->from("permissions");
				$obj->db->where("user_id",$user_id);
				$obj->db->where("permission","jvs");
				$query = $obj->db->get();
				$result = $query->result_array();
				return !empty($result);
			break;
				
			case "download file":
				if (has_access("proctor")): return TRUE; endif;
				
				// check if file belongs to this user
				$obj->load->model("File");
				$file = $obj->File->get($option);
				if ($file['created_by']==me()): return TRUE; endif;
				
				// check if file is part of a job user has access to
				$jobs = $obj->File->jobs($option);
				foreach ($jobs as $job_id):
					if (has_access("show job",$job_id)): return TRUE; endif;
				endforeach;

				return FALSE;
			break;
				
			// if user is admin or has priority set
			case "priority":
				if (has_access("administration")): return TRUE; endif;
				
				return $user['priority'];
			break;
				
			// TRUE for proctors or administrators
			case "proctor":
				if ($user['role']=="Administrator"): return TRUE; endif;
				return $user['role']=="Proctor";
			break;
						
			// only TRUE for administrators
			case "administration":				
				return $user['role']=="Administrator";
			break;
						
			// TRUE for admins; cannot impersonate root
			case "impersonate":
				if (!has_access("administration")): return FALSE; endif;
				
				$obj->db->select("*");
				$obj->db->from("permissions");
				$obj->db->where("user_id",$option);
				$obj->db->where("permission","root");
				$query = $obj->db->get();
				$result = $query->result_array();
				return empty($result);
			break;
		endswitch;
	else:
		return FALSE;
	endif;
	
	return FALSE;
}
