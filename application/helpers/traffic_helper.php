<?php
function traffic_record() {
	$obj =& get_instance();
	$obj->load->model("Traffic");

	$traffic['user_id'] = me();
	$traffic['ip_address'] = $obj->input->ip_address();
	$traffic['class'] = $obj->router->class;
	$traffic['method'] = $obj->router->method;
	$traffic['item'] = $obj->uri->segment(3);
	$traffic['created_at'] = date("Y-m-d H:i:s");

	$obj->Traffic->record($traffic);
}