<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function login($checksum=FALSE)
	{
		if (is_authed())
			redirect("main");
		elseif (isset($_SERVER['REMOTE_USER']))
			redirect("auth/login_commit");
		else
			header("Location: /auth");
			
	}

		public function login_commit()
		{
			if (is_authed()):
				redirect("main");
			elseif ( ! isset($_SERVER['REMOTE_USER'])):
				header("Location: /auth");
				return;
			endif;

			// validate NetID from Shibboleth
			$netid = explode("@",$_SERVER['REMOTE_USER']);
			if (count($netid)!=2):
				$data['heading'] = "Login failure";
				$data['message'] = "<p>Invalid NetID returned from Shibboleth: ".$_SERVER['REMOTE_USER'].". Please contact OIT for assistance.</p>";

				unset($_SESSION['url']);
				$this->load->view("errors/html/error_general",$data);
				return;
			endif;
			$netid = $netid[0];

			// make sure this user has an active account
			$this->load->model("User");
			$user_id = $this->User->get_by_netid($netid);
			$user = $this->User->get($user_id);
		
			if (empty($user)):
				$data['heading'] = "Login failure";
				$data['message'] = "<p>You don't have an account.</p>";

				unset($_SESSION['url']);
				$this->load->view("errors/html/error_general",$data);
				return;
			elseif ($user['active']==0):
				$data['heading'] = "Login failure";
				$data['message'] = "<p>Your account has been deactivated.</p>";

				unset($_SESSION['url']);
				$this->load->view("errors/html/error_general",$data);
				return;
			else:
				$_SESSION['bluesmith_logged_in'] = true;
				$_SESSION['user_id'] = $user['id'];

				set_message("notice","Welcome back, ".$this->User->name($user['id']));
		
				// log it
				$row = array();
				$row['user_id'] = $user['id'];
				$row['agent'] = $this->input->user_agent();
				$row['ip_address'] = ip2long($_SERVER['REMOTE_ADDR']);
			
				$this->db->insert("logins",$row);
			endif;
	
			redirect("sections");
		}
	
	public function logout()
	{
		session_destroy();

		echo "<p>You are now logged out</p>".PHP_EOL;
		echo "<p><a href='".base_url()."'>Return</a></p>".PHP_EOL;
	}
}

