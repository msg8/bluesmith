<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public function index($page=1) {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		
		// get default values
		$data['items_per_page'] = $this->Setting->get("Items per page");
		$data['active'] = isset($_SESSION["users_active"])? $_SESSION["users_active"]:1;
		$data['client'] = isset($_SESSION["users_client"])? $_SESSION["users_client"]:1;
		$data['role'] = isset($_SESSION["users_role"])? $_SESSION["users_role"]:"";
		$data['sort'] = isset($_SESSION["users_sort"])? $_SESSION["users_sort"]:"lastname";
		$data['order'] = isset($_SESSION["users_order"])? $_SESSION["users_order"]:"asc";
		$search = isset($_SESSION["users_search"])? $_SESSION["users_search"]:"";
		$data['search'] = $search;
		
		// if limiting to current clients, load them
		if ($data['client']):
			$this->db->select("user_id");
			$this->db->from("jobs_users");
			$this->db->distinct();
			$query = $this->db->get();
			$result = $query->result_array();
			$user_ids = array(-1); //CodeIgniter where_in doesn't like empty arrays
			foreach ($result as $row):
				$user_ids[] = $row['user_id'];
			endforeach;
		endif;			

		// load all user count
		$this->db->select("id");
		$this->db->from("users");
		if ($data['client'])
			$this->db->where_in("id",$user_ids);
		if (is_numeric($data['active']))
			$this->db->where("active",$data['active']);
		if (!empty($data['role']))
			$this->db->where("role",$data['role']);
		// parse the search term
		if (is_numeric($search)):
			$this->db->where("(id=${search} OR uniqueid LIKE '%${search}%' OR cardid LIKE '%${search}%')");
		elseif (strpos($search," ")):
			$names = explode(" ",$search);
			$first = current($names);
			$last = end($names);
			$this->db->like("firstname",$first);
			$this->db->like("lastname",$last);
		elseif (!empty($search)):
			$this->db->where("(netid LIKE '%${search}%' OR firstname LIKE '%${search}%' OR lastname LIKE '%${search}%')");		
		endif;		
		$total_rows = $this->db->count_all_results();
		$data['total_rows'] = $total_rows;
		
		// load all users
		$this->db->select("id,netid,firstname,lastname,email,role,affiliation,association");
		$this->db->from("users");
		if ($data['client'])
			$this->db->where_in("id",$user_ids);
		if (is_numeric($data['active']))
			$this->db->where("active",$data['active']);
		if (!empty($data['role']))
			$this->db->where("role",$data['role']);
		// parse the search term
		if (is_numeric($search)):
			$this->db->where("(id=${search} OR uniqueid LIKE '%${search}%' OR cardid LIKE '%${search}%')");
		elseif (strpos($search," ")):
			$names = explode(" ",$search);
			$first = current($names);
			$last = end($names);
			$this->db->like("firstname",$first);
			$this->db->like("lastname",$last);
		elseif (!empty($search)):
			$this->db->where("(netid LIKE '%${search}%' OR firstname LIKE '%${search}%' OR lastname LIKE '%${search}%')");		
		endif;
		$this->db->order_by($data['sort'],$data['order']);
		$this->db->order_by("lastname","asc");
		$this->db->order_by("firstname","asc");
		$this->db->limit($data['items_per_page'],$data['items_per_page']*($page-1));
		$query = $this->db->get();
		$result = $query->result_array();
		$users = array();
		foreach ($result as $row)
			$users[] = $row;

/* enable for auto-show on one matching user
		if (count($users)==1)
			redirect("users/show/".$users[0]['id']);
*/
		$data['users'] = $users;
		
		// prepare pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url("users/index");
		$config['uri_segment'] = 3;
		$config['total_rows'] = $total_rows;
		$config['use_page_numbers'] = true;
		$config['per_page'] = $data['items_per_page'];
		$config['num_links'] = 4;
		$config['cur_tag_open'] = "<span>";
		$config['cur_tag_close'] = "</span>";
		$this->pagination->initialize($config); 

		$this->load->model("User");
		$this->load->model("Job");
		
		$this->load->view("header",array("title"=>"Browse users"));
		$this->load->view("users/browse",$data);
		$this->load->view("footer");
	}
	
		public function active($bool) {
			$active = urldecode($bool);
			
			if (!is_numeric($active)):
				$_SESSION["users_active"] = "";
				redirect("users/index");
			endif;
				
			$_SESSION["users_active"] = $active;		
			redirect("users/index");
		}
		public function client($bool) {
			$client = urldecode($bool);
			
			if ($client)
				$_SESSION["users_client"] = 1;
			else
				$_SESSION["users_client"] = 0;		
			redirect("users/index");
		}
		public function role($role) {
			$role = urldecode($role);
			
			if ( ! in_array($role,array("User","Proctor","Administrator"))):
				$_SESSION["users_role"] = "";
				redirect("users/index");
			endif;
				
			$_SESSION["users_role"] = $role;		
			redirect("users/index");
		}
		public function search() {
			$search = $this->input->post("search");
			
			// keep it secret, keep it safe
			$search = addslashes(trim($search));
			
			$_SESSION["users_search"] = $search;
			redirect("users/index");
		}
		public function sort($sort) {
			if (!in_array($sort,array("firstname","lastname","netid","role","affiliation","association")))
				redirect("users/index");

			if ($sort==$_SESSION["users_sort"]):
				if ($_SESSION["users_order"]=="asc"):
					$_SESSION["users_order"] = "desc";
				else:
					$_SESSION["users_order"] = "asc";
				endif;
			else:
				$_SESSION["users_sort"] = $sort;
				$_SESSION["users_order"] = "asc";
			endif;

			redirect("users/index");
		}
	
	public function show($user_id=false) {
		if (!is_numeric($user_id)):
			set_message("error","No user selected");
			redirect("users");
			die();
		endif;
		$this->load->model("User");
		$user = $this->User->get($user_id);
		if (empty($user)):
			set_message("error","Couldn't find that user!");
			redirect("users");
			die();
		endif;
		if ($user['id']!=me() && ! has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
		endif;
		$data['user'] = $user;
		$data['emails'] = $this->User->emails($user['id']);
				
		$this->load->view("header",array("title"=>$this->User->name($user['id'])));
		$this->load->view("users/show",$data);
		$this->load->view("footer");
	}

		public function role_commit($user_id=false) {
			if (!has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("sections");
			endif;
			$user_id = $this->input->post("user_id");
			if (!is_numeric($user_id)):
				set_message("error","No user specified");
				redirect("users");
			endif;
			$this->load->model("User");		
			$user = $this->User->get($user_id);
			if (empty($user)):
				set_message("error","Couldn't find that user!");
				redirect("users");
			endif;
			$role = $this->input->post("role");
			if (!in_array($role,array("User","Proctor","Administrator"))):
				set_message("error","Invalid role: ".$role);
				redirect("users/show/".$user['id']);
			endif;
		
			$this->db->where("id",$user['id']);
			$this->db->limit(1);
			$this->db->update("users",array("role"=>$role));
			set_message("success","User role changed to ".$role);
			
			redirect("users/show/".$user['id']);
		}
		
		// toggle priority tag for a user
		public function priority($user_id,$val=false) {
			if (!has_access("administration"))
				die("You don't have permission to do that.");

			if (!is_numeric($user_id))
				die("No user specified");

			$this->load->model("User");		
			$user = $this->User->get($user_id);
			if (empty($user))
				die("Couldn't find that user!");

			$val = ($val=='true')? 1:0;
		
			$this->db->where("id",$user['id']);
			$this->db->limit(1);
			$this->db->update("users",array("priority"=>$val));
		}
	
	public function sync($user_id=false) {
		if (!is_numeric($user_id)):
			set_message("error","No user selected");
			redirect("users");
			die();
		endif;
		$this->load->model("User");
		$user = $this->User->get($user_id);
		
		$this->load->model("Company");
		$result = $this->Company->user_sync($user['id']);
		if ($result!==TRUE)
			set_message("error","Unable to sync user.");		
		redirect("users/show/".$user['id']);
	}
	
		public function credit_commit() {
			if (!has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("sections");
				die();
			endif;
			$user_id = $this->input->post("user_id");
			if (empty($user_id)):
				set_message("error","You did not select a user.");
				redirect("users");
				die();
			endif;
			$this->load->model("User");
			$user = $this->User->get($user_id);
			if (empty($user)):
				set_message("error","That user does not exist.");
				redirect("users");
				die();
			endif;
			$amount = $this->input->post("amount");
			if (!is_numeric($amount) OR $amount<=0):
				set_message("error","Invalid amount");
				redirect("users/show/".$user['id']);
				die();
			endif;
			
			// record the credit
			$row = array(
				"user_id" => $user_id,
				"amount" => $amount,
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s")
			);
			$this->db->insert("credits",$row);
			
			// check for existing credit
			$balance = $this->User->balance($user_id);
			if ($balance===FALSE):
				// set to the credit amount
				$row = array(
					"user_id" => $user_id,
					"amount" => $amount
				);
				$this->db->insert("balances",$row);
				
				$balance = $amount;
			else:
				// add to the existing balance
				$balance += $amount;
				$this->db->where("id",$user_id);
				$this->db->limit(1);
				$this->db->update("users",array("balance"=>$balance));
			endif;
			set_message("success","Added $".number_format($amount,2)." for ".$this->User->name($user_id)."; new balance is $".number_format($balance,2));
			redirect("users/show/".$user_id);
		}

	public function impersonate($user_id) {
		auth();
		if (!has_access("impersonate",$user_id)):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;

		$this->load->model("User");
		$user = $this->User->get($user_id);
		if (empty($user)):
			set_message("error","Couldn't load that user to impersonate!");
		else:
			$this->User->impersonate($user['id']);
			set_message("notice","You are now impersonating ".$this->User->name($user['id']));
		endif;
		
		unset($_SESSION['url']);
		redirect("sections");
	}
}