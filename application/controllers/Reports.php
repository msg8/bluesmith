<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function index() {
		if (!has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
		endif;
		
		$this->load->view("header");
		$this->load->view("reports/index");
		$this->load->view("footer");
	}
		public function request_commit() {
			if (!has_access("proctor")):
				set_message("error","You don't have permission to do that.");
				redirect("jobs");
			endif;
			$request = $this->input->post("request");
			if (empty($request)):
				set_message("error","You must fill out the content of your report request.");
				redirect("reports");
			endif;
			
			$content = "New report request received."."\r\n";
			$content .= "Submitted by: ".$this->User->name(me())."\r\n";
			$content .= "Submitted at: ".date("n/j/Y, g:ia")."\r\n";
			$content .= "Request content:"."\r\n\r\n";
			$content .= $request;

			$this->load->library('email');
			$this->email->from(SYSTEM_EMAIL);
			$this->email->to("matt.gatner@duke.edu");
			$this->email->subject(SYSTEM_PREFIX."Report request");
			$this->email->message($content);
			$result = $this->email->send();
			
			set_message("success","Your request has been submitted for review.");
			redirect("reports");
		}
	
	public function payments_table() {
		if (!has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
		endif;
		
		$this->load->model("Payment");
			
		// check for post submission
		if ($form = $this->input->post()):
			$data['form'] = $form;
			
			// validate data
			$flag = FALSE;
			if (strtotime($form['start'])===FALSE):
				$flag = true;
				set_message("error","Invalid start date");
			endif;
			if (strtotime($form['end'])===FALSE):
				$flag = true;
				set_message("error","Invalid end date");
			endif;
			if (!in_array($form['metric'],array("processed_at","created_at"))):
				$flag = true;
				set_message("error","Invalid metric");
			endif;
			$types = $this->input->post("types");
			if (empty($types)):
				$flag = true;
				set_message("error","You must select at least one type");
			endif;
			
			if ($flag===FALSE):
				$start = date("Y-m-d",strtotime($form['start']));
				$end = date("Y-m-d",strtotime("+1 day",strtotime($form['end']))); //pad to make end inclusive
				
				$this->db->select("*");
				$this->db->from("payments");
				$this->db->where("processed_at IS NOT NULL");
				$this->db->where($form['metric']." >",$start);
				$this->db->where($form['metric']." <",$end);
				$this->db->where_in("type",$types);
				$this->db->order_by($form['metric'],"asc");
				$query = $this->db->get();
				$data['payments'] = $query->result_array();
			endif;
			
			$this->load->model("Job");
			$this->load->model("User");
		else:
			$data = array();
			// set some default values
			$data['form'] = array(
				"start" => date("m/d/Y",strtotime("-1 month")),
				"end" => date("m/d/Y"),
				"metric" => "processed_at",
				"types" => array_keys($this->Payment->types())
			);
		endif;
			
		$this->load->view("header");
		$this->load->view("reports/payments_table",$data);
		$this->load->view("footer");		
	}
	
	public function payments_export() {
		if (!has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
		endif;
		
		$this->load->model("Payment");
			
		// check for post submission
		if ($form = $this->input->post()):
			$data['form'] = $form;
			
			// validate data
			$flag = FALSE;
			if (strtotime($form['start'])===FALSE):
				$flag = true;
				set_message("error","Invalid start date");
			endif;
			if (strtotime($form['end'])===FALSE):
				$flag = true;
				set_message("error","Invalid end date");
			endif;
			if (!in_array($form['metric'],array("processed_at","created_at"))):
				$flag = true;
				set_message("error","Invalid metric");
			endif;
			$types = $this->input->post("types");
			if (empty($types)):
				$flag = true;
				set_message("error","You must select at least one type");
			endif;
			
			if ($flag===FALSE):
				$start = date("Y-m-d",strtotime($form['start']));
				$end = date("Y-m-d",strtotime("+1 day",strtotime($form['end']))); //pad to make end inclusive
				
				$this->db->select("*");
				$this->db->from("payments");
				$this->db->where("processed_at IS NOT NULL");
				$this->db->where($form['metric']." >",$start);
				$this->db->where($form['metric']." <",$end);
				$this->db->where_in("type",$types);
				$this->db->order_by($form['metric'],"asc");
				$query = $this->db->get();
				$data['payments'] = $query->result_array();
			endif;
			
			$this->load->model("Job");
			$this->load->model("User");
			
			if (!empty($data['payments'])):
				$this->load->view("reports/payments_export",$data);
				return;
			endif;
		else:
			$data = array();
			// set some default values
			$data['form'] = array(
				"start" => date("m/d/Y",strtotime("-1 month")),
				"end" => date("m/d/Y"),
				"metric" => "processed_at",
				"types" => array_keys($this->Payment->types())
			);
		endif;
			
		$this->load->view("header");
		$this->load->view("reports/payments_table",$data);
		$this->load->view("footer");		
	}
		
	
}