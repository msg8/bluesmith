<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	
	public function index()	{
		$this->load->view("header");
		$this->load->view("main");
		$this->load->view("footer");		
	}
	

	
	public function admin()	{
		auth();
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("main");
			die();
		endif;
		
		// load users
		$this->load->model("User");
		
		$this->db->select("*");
		$this->db->from("permissions");
		$query = $this->db->get();
		$result = $query->result_array();
		$names = array();
		$permissions = array();
		foreach ($result as $row):
			$names[$row['user_id']] = $this->User->name($row['user_id']);
			$permissions[$row['user_id']] = $row['permission'];
		endforeach;
		asort($names);
		$data['names'] = $names;
		$data['permissions'] = $permissions;

		$this->load->view("header");
		$this->load->view("admin",$data);
		$this->load->view("footer");		
	}
		public function permission($user_id=FALSE,$action=FALSE) {
			auth();
			if (!has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("main");
				die();
			endif;
			$this->load->model("User");
			
			if ($netid = $this->input->post("netid")):
				$user_id = $this->User->get_by_netid($netid);
				$user = $this->User->get($user_id);
				if (empty($user)):
					set_message("error","Could not find that NetID.");
					redirect("main/admin");
					die();
				endif;
				
				$access = $this->input->post("access");
				if (!in_array($access,array("proctor","administrator"))):
					set_message("error","Invalid access level.");
					redirect("main/admin");
					die();
				endif;
				
				$row = array(
					"user_id" => $user['id'],
					"permission" => $access,
					"created_by" => me()
				);
				$this->db->insert("permissions",$row);
				
				set_message("success",ucfirst($access)." access granted to ".$this->User->name($user['id']));
				redirect("main/admin");
				die();
			endif;
			
			if (is_numeric($user_id)):
				$user = $this->User->get($user_id);
				if (empty($user)):
					set_message("error","Could not find that NetID.");
					redirect("main/admin");
					die();
				endif;
				
				switch ($action):
					case "remove":
						$this->db->where("user_id",$user['id']);
						$this->db->delete("permissions");
						set_message("success","Access removed for ".$this->User->name($user['id']));
						redirect("main/admin");
					break;
				endswitch;
			endif;
			
			set_message("error","Unknown error occurred.");
			redirect("main/admin");
			die();
		}
	
	public function impersonate()	{
		die("disabled");
	}
	
	public function token($token=FALSE)	{
		if (empty($token)):
			set_message("warning","No token supplied.");
			redirect("main");
			die();
		endif;
		$this->load->model("Invoice");
		$invoice_id = $this->Invoice->get_by_token($token);
		$invoice = $this->Invoice->get($invoice_id);
		if (empty($invoice)):
			set_message("warning","No matching invoice found.");
			redirect("main");
			die();		
		endif;
		
		if (!is_authed()):
			$this->load->model("User");
			$this->session->set_userdata("invoke_logged_in",true);
			$this->session->set_userdata("user_id",$invoice['user_id']);
			set_message("notice","Welcome back, ".$this->User->name($invoice['user_id']));
		
			// log it
			$row = array();
			$row['user_id'] = $invoice['user_id'];
			$row['agent'] = $this->input->user_agent();
			$row['ip_address'] = ip2long($_SERVER['REMOTE_ADDR']);
			$this->db->insert("logins",$row);
		endif;

		redirect("invoices/show/".$invoice['id']);
	}
	
	public function items_per_page($items_per_page=10) {
		$this->Setting->set("Items per page",$items_per_page);
		return;
	}
	
}
