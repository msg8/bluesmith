<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materials extends CI_Controller {

	public function add() {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		$this->load->model("Method");
		$methods = $this->Method->all();
		if (empty($methods)):
			set_message("error","You must add a print method before adding materials.");
			redirect("sections/cms/methods");
			die();
		endif;
		
		$data["methods"] = $methods;
		
		$this->load->view("header");
		$this->load->view("materials/add",$data);
		$this->load->view("footer");
	}

		public function add_commit() {
			if (!has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("sections");
				die();
			endif;
			
			$row = array(
				"method_id" => $this->input->post("method_id"),
				"name" => $this->input->post("name"),
				"description" => $this->input->post("description"),
				"status" => "Active",
				"sortorder" => $this->input->post("sortorder"),
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s")
			);
			
			if (empty($row['name'])):
				set_message("error","You must supply a name.");
				redirect("materials/add");
				die();
			endif;
			
			$this->db->insert("materials",$row);
			set_message("success","Added new print material '".$row['name']."'");
			redirect("sections/cms/Materials");
		}

	public function edit($material_id=FALSE) {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		if (!is_numeric($material_id)):
			set_message("error","You must specify a material.");
			redirect("sections/cms/Materials");
			die();
		endif;
		$this->load->model("Material");
		$material = $this->Material->get($material_id);
		if (empty($material)):
			set_message("error","Could not find that material.");
			redirect("sections/cms/Materials");
			die();
		endif;
		$data['material'] = $material;
		
		$this->load->model("Method");
		$data['methods'] = $this->Method->all();
		
		$this->load->view("header");
		$this->load->view("materials/edit",$data);
		$this->load->view("footer");
	}

		public function edit_commit() {
			if (!has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("sections");
				die();
			endif;
			$material_id = $this->input->post("material_id");
			if (!is_numeric($material_id)):
				set_message("error","You must specify a material.");
				redirect("sections/cms/Materials");
				die();
			endif;
			$this->load->model("Material");
			$material = $this->Material->get($material_id);
			if (empty($material)):
				set_message("error","Could not find that material.");
				redirect("sections/cms/Materials");
				die();
			endif;
						
			$row = array(
				"method_id" => $this->input->post("method_id"),
				"name" => $this->input->post("name"),
				"description" => $this->input->post("description"),
				"status" => "Active",
				"sortorder" => $this->input->post("sortorder")
			);
			
			if (empty($row['name'])):
				set_message("error","You must supply a name.");
				redirect("materials/edit/".$material['id']);
				die();
			endif;
			
			$this->db->where("id",$material['id']);
			$this->db->limit(1);
			$this->db->update("materials",$row);
			set_message("success","Updated print material '".$row['name']."'");
			redirect("sections/cms/Materials");
		}
	
	public function remove($material_id) {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		if (!is_numeric($material_id)):
			set_message("error","You must specify a material.");
			redirect("sections/cms/Materials");
			die();
		endif;
		$this->load->model("Material");
		$material = $this->Material->get($material_id);
		if (empty($material)):
			set_message("error","Could not find that material.");
			redirect("sections/cms/Materials");
			die();
		endif;
					
		$this->db->where("id",$material['id']);
		$this->db->limit(1);
		$this->db->update("materials",array("status"=>"Archived"));
		set_message("success","Archived print material '".$material['name']."'");
		redirect("sections/cms/Materials");
	}
}