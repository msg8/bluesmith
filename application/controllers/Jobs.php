<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends CI_Controller {

	public function index($page=1) {
		$this->load->model("User");
		$this->load->model("Job");
		$this->load->helper("stage");

		// get default values
		$data['items_per_page'] = $this->Setting->get("Items per page");
		$data['order'] = isset($_SESSION["jobs_order"])? $_SESSION["jobs_order"]:"asc";
		$data['user_id'] = isset($_SESSION["jobs_user"])? $_SESSION["jobs_user"]:FALSE;
		$data['status'] = isset($_SESSION["jobs_status"])? $_SESSION["jobs_status"]:"Active";
		$search = isset($_SESSION["jobs_search"])? $_SESSION["jobs_search"]:"";
		$data['search'] = $search;

		// sort defaults to name for users and stage for proctors
		if (isset($_SESSION["jobs_sort"])):
			$data['sort'] = $_SESSION["jobs_sort"];
		elseif (has_access("proctor")):
			$data['sort'] = "stage";
		else:
			$data['sort'] = "name";
		endif;
		
		// users can only see their own jobs
		if (!has_access("proctor")):
			$_SESSION["jobs_user"] = me();
			$data['user_id'] = me();
		endif;
		
		// if filtering by user, preload user's jobs
		if (!empty($data['user_id'])):
			$user_jobs = $this->User->jobs($data['user_id']);
			$user_jobs[] = -1; //padded to prevent syntax failures on empty arrays
		endif;
			
		// load job count
		$this->db->select("id");
		$this->db->from("jobs");
		
		if (is_numeric($search)):
			$this->db->where("id",$search);
		else:
			if (!empty($data['user_id'])):
				$this->db->where_in("id",$user_jobs);
			endif;
			if (!empty($data['status'])):
				$this->db->where("status",$data['status']);
			endif;
			if (!empty($search)):
				$this->db->like("name",$search);
			endif;
		endif;
		$total_rows = $this->db->count_all_results();
		$data['total_rows'] = $total_rows;
		
		// load jobs
		$this->db->select("*");
		$this->db->from("jobs");
		if (is_numeric($search)):
			$this->db->where("id",$search);
		else:
			if (!empty($data['user_id'])):
				$this->db->where_in("id",$user_jobs);
			endif;
			if (!empty($data['status'])):
				$this->db->where("status",$data['status']);
			endif;
			if (!empty($search)):
				$this->db->like("name",$search);
			endif;
		endif;
		$this->db->order_by($data['sort'],$data['order']);
		$this->db->order_by("created_at","desc");
		$this->db->limit($data['items_per_page'],$data['items_per_page']*($page-1));
		$query = $this->db->get();
		$result = $query->result_array();
		$jobs = array();
		foreach ($result as $row)
			$jobs[] = $row;
		$data['jobs'] = $jobs;
		
		// prepare pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url("jobs/index/");
		$config['uri_segment'] = 3;
		$config['total_rows'] = $total_rows;
		$config['use_page_numbers'] = true;
		$config['per_page'] = $data['items_per_page'];
		$config['num_links'] = 4;
		$config['cur_tag_open'] = "<span>";
		$config['cur_tag_close'] = "</span>";
		$this->pagination->initialize($config); 

		$this->load->model("User");
		$this->load->model("Method");
		$this->load->model("Material");
		
		$this->load->view("header");
		$this->load->view("jobs/browse",$data);
		$this->load->view("footer");
	}
		public function user($user_id) {
			$user_id = urldecode($user_id);
			
			if (empty($user_id)):
				$_SESSION["jobs_user"] = FALSE;
				redirect("jobs/index");
			endif;

			$_SESSION["jobs_user"] = $user_id;		
			redirect("jobs/index");
		}
		public function status($status) {
			$status = urldecode($status);
			
			if (empty($status)):
				$_SESSION["jobs_status"] = FALSE;
				redirect("jobs/index");
			endif;

			$_SESSION["jobs_status"] = $status;		
			redirect("jobs/index");
		}
		public function search() {
			$search = $this->input->post("search");
			
			// keep it secret, keep it safe
			$search = addslashes($search);
			
			$_SESSION["jobs_search"] = $search;
			redirect("jobs/index");
		}
		public function sort($sort) {
			if (!in_array($sort,array("name","stage","quote","created_by","created_at","updated_at")))
				redirect("jobs/index");

			if ($sort==$_SESSION["jobs_sort"]):
				if ($_SESSION["jobs_order"]=="asc"):
					$_SESSION["jobs_order"] = "desc";
				else:
					$_SESSION["jobs_order"] = "asc";
				endif;
			else:
				$_SESSION["jobs_sort"] = $sort;
				$_SESSION["jobs_order"] = "asc";
			endif;

			redirect("jobs/index");
		}
		// reset job options
		public function browse() {
			unset($_SESSION["jobs_sort"]);
			unset($_SESSION["jobs_order"]);
			unset($_SESSION["jobs_user"]);
			unset($_SESSION["jobs_status"]);

			redirect("jobs/index");
		}

	public function add() {
		$this->load->model("User");
		$this->load->model("Job");
		$this->load->model("File");
		$this->load->model("Method");
		$this->load->model("Material");
		$this->load->model("Section");
		
		$data['files'] = $this->User->files(me());
		$data['methods'] = $this->Method->all();
		
		$this->load->view("header");
		$this->load->view("jobs/add",$data);
		$this->load->view("footer");	
	}
		public function add_commit() {
			$this->load->model("User");
			$this->load->model("Job");
			
			// load post data
			$row = array(
				"source" => $this->input->post("source"),
				"name" => $this->input->post("name"),
				"instructions" => $this->input->post("instructions"),
				"premium" => $this->input->post("premium")? 1:0,
				"inspection" => $this->input->post("inspection")? 1:0,
				"confidential" => $this->input->post("confidential")? 1:0,
				"status" => "Active",
				"stage" => 2,
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s")
			);
			
			// check for Med Center options
			if (has_access("priority")):
				$row['invoiced'] = $this->input->post("invoiced")? 1:0;
				$row['priority'] = $this->input->post("priority")? 1:0;
			endif;
			
			// determine material selected
			$method_id = $this->input->post("method_id");
			$material_id = $this->input->post("material_".$method_id);
			$row['material_id'] = $material_id;
			
			// load files
			$files = $this->input->post("files");
			// load users
			$users = $this->input->post("users");
			$emails = $this->input->post("emails");
						
			// basic validation
			if (empty($row['name'])):
				set_message("error","You must supply a descriptive name for your job.");
				redirect("jobs/add");
				die();
			endif;
			if (empty($row['material_id'])):
				set_message("error","You must select a material for your job.");
				redirect("jobs/add");
				die();
			endif;
			if ($row['source']=="Upload" && empty($files)):
				set_message("error","You must select at least one file for your job.");
				redirect("jobs/add");
				die();
			endif;
			if (empty($users)):
				set_message("error","You must select at least one client.");
				redirect("jobs/add");
			endif;
			if (empty($emails)):
				set_message("error","At least one client must have email notices enabled.");
				redirect("jobs/add");
			endif;
			
			// create the job
			$this->db->insert("jobs",$row);
			$job_id = $this->db->insert_id();
			set_message("success","New job created successfully. Reference #".$job_id);
						
			// associate the files
			foreach ($files as $file_id):
				$row = array(
					"file_id" => $file_id,
					"job_id" => $job_id,
					"created_at" => date("Y-m-d H:i:s")
				);
				$this->db->insert("files_jobs",$row);
			endforeach;

			// associate users
			foreach ($users as $user_id):
				$email = in_array($user_id,$emails)? 1:0;
				$row = array(
					"job_id" => $job_id,
					"user_id" => $user_id,
					"email" => $email,
					"created_at" => date("Y-m-d H:i:s")
				);
				$this->db->insert("jobs_users",$row);
			endforeach;
/*
			// check for additional user request
			if ($netid = $this->input->post("netid")):
				$user_id = $this->User->get_by_netid($netid);
				if (empty($user_id)):
					set_message("warning","No match found on user '${netid}' - skipped.");
				// skip requests to add oneself
				elseif ($user_id==me()):
				else:
					$row = array(
						"job_id" => $job_id,
						"user_id" => $user_id,
						"created_at" => date("Y-m-d H:i:s")
					);
					$this->db->insert("jobs_users",$row);
				endif;
			endif;
*/

			$this->load->model("Email");
			$result = $this->Email->send($job_id,"Submitted");
			if (empty($result)):
				set_message("warning","Email failed to send; staff will follow up directly.");
			else:
				set_message("success","Email notice sent to ".$result['recipient']);
			endif;		
			
			redirect("jobs/show/".$job_id);
		}

		// returns table rows to append based on search term
		public function users($search="") {
			$search = urldecode($search);
			
			// load all users
			$this->db->select("*");
			$this->db->from("users");
			$this->db->where("active",1);

			// parse the search term
			if (is_numeric($search)):
				$this->db->where("(id=${search} OR uniqueid LIKE '%${search}%' OR cardid LIKE '%${search}%')");
			elseif (strpos($search," ")):
				$names = explode(" ",$search);
				$first = current($names);
				$last = end($names);
				$this->db->like("firstname",$first);
				$this->db->like("lastname",$last);
			else:
				$this->db->where("(netid LIKE '%${search}%' OR firstname LIKE '%${search}%' OR lastname LIKE '%${search}%')");		
			endif;
			$this->db->order_by("lastname","asc");
			$this->db->order_by("firstname","asc");
			$this->db->limit(5);
			$query = $this->db->get();
			$result = $query->result_array();
			$users = array();
			foreach ($result as $row)
				$users[] = $row;
			$data['users'] = $users;

			$this->load->view("jobs/users",$data);			
		}
	
	public function show($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		
		$this->load->model("File");
		$this->load->model("User");
		$this->load->model("Method");
		$this->load->model("Material");
		$this->load->model("Charge");
		$this->load->model("Payment");
		$this->load->model("Comment");
				
		$data['job'] = $job;
		$data['users'] = $this->Job->users($job['id']);
		$data['files'] = $this->Job->files($job['id']);
		$data['charges'] = $this->Job->charges($job['id']);
		$data['payments'] = $this->Job->payments($job['id']);
		$data['comments'] = $this->Job->comments($job['id']);
		
		$data['material'] = $this->Material->get($job['material_id']);
		$data['method'] = $this->Method->get($data['material']['method_id']);
		
		// display a note on confidential jobs
		if ($job['confidential'])
			set_message("warning","This job is marked confidential.");
		// display a note on priority jobs
		if ($job['priority'])
			set_message("warning","This job is a high priority clinic request.");
			
		$this->load->view("header");
		$this->load->view("jobs/show",$data);
		$this->load->view("footer");
	}
	
	public function clients($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
		endif;
		
		$this->load->model("User");
				
		$data['job'] = $job;
		$data['users'] = $this->Job->users($job['id']);
		
		$this->load->view("header");
		$this->load->view("jobs/clients",$data);
		$this->load->view("footer");
	}
		public function clients_commit() {
			$job_id = $this->input->post("job_id");
			if (!is_numeric($job_id)):
				set_message("error","You did not specify a job.");
				redirect("jobs");
			endif;
			$this->load->model("Job");
			$job = $this->Job->get($job_id);
			if (empty($job)):
				set_message("error","That job could not be found.");
				redirect("jobs");
			endif;
			if (!has_access("show job",$job['id'])):
				set_message("error","You don't have permission to do that.");
				redirect("jobs");
			endif;
			
			// load users
			$users = $this->input->post("users");
			$emails = $this->input->post("emails");
						
			// validation
			if (empty($users)):
				set_message("error","You must select at least one client.");
				redirect("jobs/clients");
			endif;
			if (empty($emails)):
				set_message("error","At least one client must have email notices enabled.");
				redirect("jobs/clients");
			endif;

			// associate users
			$current = $this->Job->users($job['id']);
			
			foreach ($users as $user_id):
				$email = in_array($user_id,$emails)? 1:0;
				
				// if user is already aprt of the job, update email settings
				if (in_array($user_id,$current)):
					$this->db->where("job_id",$job['id']);
					$this->db->where("user_id",$user_id);
					$this->db->limit(1);
					$this->db->update("jobs_users",array("email"=>$email));
					
				// otherwise, add the user
				else:
					$row = array(
						"job_id" => $job_id,
						"user_id" => $user_id,
						"email" => $email,
						"created_at" => date("Y-m-d H:i:s")
					);
					$this->db->insert("jobs_users",$row);
				endif;
			endforeach;

			// remove any other users
			$this->db->where("job_id",$job['id']);
			$this->db->where_not_in("user_id",$users);
			$this->db->delete("jobs_users");
			
			set_message("success","Updated clients for job #".$job['id']);
			
			redirect("jobs/show/".$job_id);
		}
	
	// general invoice with limited information anyone can see (linked from SAP JV)
	public function invoice($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['printed_at'])):
			set_message("error","Your job must be printed before the invoice is available.");
			redirect("jobs/show/".$job['id']);
			die();
		endif;

		$this->load->model("User");
		$this->load->model("Method");
		$this->load->model("Material");
		$this->load->model("Charge");
		$this->load->model("Payment");
				
		$data['job'] = $job;
		$data['users'] = $this->Job->users($job['id']);
		$data['charges'] = $this->Job->charges($job['id']);
		$data['payments'] = $this->Job->payments($job['id']);
		
		$data['material'] = $this->Material->get($job['material_id']);
		$data['method'] = $this->Method->get($data['material']['method_id']);
		
		$this->load->view("header");
		$this->load->view("jobs/invoice",$data);
		$this->load->view("footer");
	}
	
	// zip all files for a job and download it
	public function download($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
		endif;
		$files = $this->Job->files($job['id']);
		if (empty($files)):
			set_message("error","This job has no files associated with it.");
			redirect("jobs/show/".$job['id']);
		endif;
		
		$zip = new ZipArchive();
		$name = $this->Job->name($job['id'])." files.zip";
		$path = sys_get_temp_dir();
		$zipfile = $path."/".$name;

		if ($zip->open($zipfile, ZipArchive::CREATE)!==TRUE):
			set_message("error","Cannot open <${zipfile}> for writing.");
			redirect("jobs/show/".$job['id']);
		endif;
		
		// add files
		$this->load->model("File");
		foreach ($files as $file_id):
			$file = $this->File->get($file_id);
			
			// load file
			$path = "assets/files/".$file['filename'];
			if (file_exists($path)):
				// add it to the archive
				$zip->addFile($path,$file['filename']);

				// log it
				$row = array();
				$row["file_id"] = $file['id'];
				$row["created_by"] = me();
				$row["created_at"] = date("Y-m-d H:i:s");
				$this->db->insert("downloads",$row);
			else:
				set_message("error","File ${file['filename']} doesn't exist!");
				redirect("jobs/show/".$job['id']);
			endif;
		endforeach;
		
		$zip->close();
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$zipfile.'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($zipfile));
		readfile($zipfile);
		
		unlink($zipfile);
		exit;
	}
		
	public function view($job_id=FALSE) {
		redirect("jobs/show/".$job_id);
	}

	public function audit($job_id=FALSE) {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		$data['job'] = $job;
		
		$this->load->model("User");
		$this->load->model("Charge");
		$this->load->model("Payment");
		$this->load->model("File");
		$this->load->model("Comment");
		$this->load->model("Email");
		
		$times = array(); $users = array(); $events = array();
		
		// get job info
		foreach (array("created","quoted","approved","printed","paid","received") as $action):
			if (!empty($job[$action."_at"])):
				$times[] = strtotime($job[$action."_at"]);
				$users[] = $job[$action."_by"];
				switch ($action):
					case "created":
						$events[] = "Initial job submitted";
					break;
					case "quoted":
						$events[] = "Estimate quoted to clients at $".number_format($job['quote'],2);
					break;
					case "approved":
						$events[] = "Estimate approved";
					break;
					case "printed":
						$events[] = "Printing completed";
					break;
					case "paid":
						$events[] = "Payment submitted";
					break;
					case "received":
						$events[] = "Printed items picked up";
					break;
				endswitch;
			endif;
		endforeach;
		
		// charges
		foreach ($this->Job->charges($job['id']) as $charge_id):
			$charge = $this->Charge->get($charge_id);

			$times[] = strtotime($charge['created_at']);
			$users[] = $charge['created_by'];
			$events[] = "Charge added for ".$charge['name'].": $".number_format($charge['amount'],2);
		endforeach;
		
		// comments
		foreach ($this->Job->comments($job['id']) as $comment_id):
			$comment = $this->Comment->get($comment_id);

			$times[] = strtotime($comment['created_at']);
			$users[] = $comment['created_by'];
			$events[] = "Comment added: ".substr($comment['content'],0,10)."...";
		endforeach;
		
		// downloads
		if ($files = $this->Job->files($job['id'])):
			$this->db->select("*");
			$this->db->from("downloads");
			$this->db->where_in("file_id",$files);
			$query = $this->db->get();
			$result = $query->result_array();
			foreach ($result as $row):
				$times[] = strtotime($row['created_at']);
				$users[] = $row['created_by'];
				$events[] = "File '".$this->File->name($row['file_id'])."' downloaded";
			endforeach;
		endif;
		
		// emails
		$this->db->select("*");
		$this->db->from("emails_jobs");
		$this->db->where("job_id",$job['id']);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach ($result as $row):
			$times[] = strtotime($row['created_at']);
			$users[] = $row['created_by'];
			$events[] = "Sent '".$this->Email->name($row['email_id'])."' email to ".$row['recipient'];
		endforeach;

		// payments
		foreach ($this->Job->payments($job['id']) as $payment_id):
			$payment = $this->Payment->get($payment_id);
			$pending = ($payment['processed_at'])? "Payment":"Pending payment";
			$times[] = strtotime($payment['created_at']);
			$users[] = $payment['created_by'];
			$events[] = "${pending} reference #".$payment['reference_id']." submitted by ".$payment['type'].": $".number_format($payment['amount'],2);
		endforeach;
		
		asort($times);
		$data['times'] = $times;
		$data['users'] = $users;
		$data['events'] = $events;
		
		$this->load->view("header");
		$this->load->view("jobs/audit",$data);
		$this->load->view("footer");
	}

	public function edit($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (!empty($job['printed_at'])):
			set_message("error","This job has already been printed.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;
		
		$this->load->model("User");
		$this->load->model("File");
		$this->load->model("Material");
		$this->load->model("Method");
		
		$data['job'] = $job;
		$data['material'] = $this->Material->get($job['material_id']);
		$data['method'] = $this->Method->get($data['material']['method_id']);
		$data['files'] = $this->Job->files($job['id']);
		$data['methods'] = $this->Method->all();

		// load full user association (for email column)
		$this->db->select("*");
		$this->db->from("jobs_users");
		$this->db->where("job_id",$job['id']);
		$query = $this->db->get();
		$result = $query->result_array();
		$data['users'] = array();
		foreach ($result as $row)
			$data['users'][] = $row;
		
		// make sure file list includes other users' files associated with this job
		$myfiles = $this->User->files(me());
		$myfiles = array_unique(array_merge($myfiles,$data['files']));
		$data['myfiles'] = $myfiles;

		if (!empty($job['quoted_at'])):
			set_message("warning","Updating a job in progress will require a new estimate.");
		endif;
				
		$this->load->view("header");
		$this->load->view("jobs/edit",$data);
		$this->load->view("footer");
	}

		public function edit_commit() {
			$job_id = $this->input->post("job_id");
			if (!is_numeric($job_id)):
				set_message("error","You did not specify a job.");
				redirect("jobs");
				die();
			endif;
			$this->load->model("Job");
			$job = $this->Job->get($job_id);
			if (empty($job)):
				set_message("error","That job could not be found.");
				redirect("jobs");
				die();
			endif;
			if (!has_access("show job",$job['id'])):
				set_message("error","You don't have permission to do that.");
				redirect("jobs");
				die();
			endif;
			$this->load->model("User");
			
			// load post data
			$row = array(
				"name" => $this->input->post("name"),
				"instructions" => $this->input->post("instructions"),
				"premium" => $this->input->post("premium")? 1:0,
				"inspection" => $this->input->post("inspection")? 1:0,
				"confidential" => $this->input->post("confidential")? 1:0,
				"status" => "Active",
				"stage" => 2
			);
			
			// determine material selected
			$method_id = $this->input->post("method_id");
			$material_id = $this->input->post("material_".$method_id);
			$row['material_id'] = $material_id;
			
			// load files
			$files = $this->input->post("files");
			// load users
			$users = $this->input->post("users");
			$emails = $this->input->post("emails");
						
			// basic validation
			if (empty($row['name'])):
				set_message("error","You must supply a descriptive name for your job.");
				redirect("jobs/edit/".$job['id']);
			endif;
			if (empty($row['material_id'])):
				set_message("error","You must select a material for your job.");
				redirect("jobs/edit/".$job['id']);
			endif;
			if (empty($files)):
				set_message("error","You must select at least one file for your job.");
				redirect("jobs/edit/".$job['id']);
			endif;
			if (empty($users)):
				set_message("error","You must select at least one client.");
				redirect("jobs/edit/".$job['id']);
			endif;
			if (empty($emails)):
				set_message("error","At least one client must have email notices enabled.");
				redirect("jobs/edit/".$job['id']);
			endif;

			// update the job
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			
			// remove current file associations
			$this->db->where("job_id",$job['id']);
			$this->db->delete("files_jobs");
			
			// associate new files
			foreach ($files as $file_id):
				$row = array(
					"file_id" => $file_id,
					"job_id" => $job_id,
					"created_at" => date("Y-m-d H:i:s")
				);
				$this->db->insert("files_jobs",$row);
			endforeach;
			
			// remove current user associations
			$this->db->where("job_id",$job['id']);
			$this->db->delete("jobs_users");

			// associate users
			foreach ($users as $user_id):
				$email = in_array($user_id,$emails)? 1:0;
				$row = array(
					"job_id" => $job_id,
					"user_id" => $user_id,
					"email" => $email,
					"created_at" => date("Y-m-d H:i:s")
				);
				$this->db->insert("jobs_users",$row);
			endforeach;
			
			set_message("success","Job updated successfully. Reference #".$job['id']);

			// check if there was a quote set
			if (!empty($job['quoted_at'])):
				// remove quote
				$row = array(
					"quote" => null,
					"quoted_by" => null,
					"quoted_at" => null,
					"approved_by" => null,
					"approved_at" => null
				);
				$this->db->where("id",$job['id']);
				$this->db->limit(1);
				$this->db->update("jobs",$row);
				
				// notify staff
				$this->load->model("Email");
				$this->Email->send($job['id'],"Updated");
				set_message("notice","Staff notified to send an updated quote.");
			endif;

			redirect("jobs/show/".$job_id);
		}

	public function quote($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		$charges = $this->Job->charges($job['id']);
		if (empty($charges)):
			set_message("error","You must set charges before sending a quote.");
			redirect("jobs/charges/".$job['id']);
			die();
		endif;		
		$cost = $this->Job->cost($job['id']);
		if ($cost<0):
			set_message("error","You cannot quote a negative cost.");
			redirect("jobs/charges/".$job['id']);
			die();		
		endif;
		if (!empty($job['printed_at'])):
			set_message("error","This job has already been printed.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;
		
		// update the database
		$row = array(
			"stage" => 4,
			"quote" => $cost,
			"quoted_at" => date("Y-m-d H:i:s"),
			"quoted_by" => me()
		);
		$this->db->where("id",$job['id']);
		$this->db->limit(1);
		$this->db->update("jobs",$row);
		set_message("success","Job quoted at $".number_format($cost,2));
		
		// send the email
		$this->load->model("Email");
		$result = $this->Email->send($job['id'],"Quoted");
		if (empty($result)):
			set_message("warning","Email failed to send to clients; try again or contact them directly.");
		else:
			set_message("success","Email notice sent to ".$result['recipient']);
		endif;
		
		redirect("jobs/show/".$job['id']);
	}

	public function approve($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['quoted_at'])):
			set_message("error","This job has not been quoted yet.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;
		if (!empty($job['printed_at'])):
			set_message("error","This job has already been printed.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;
		
		$data['job'] = $job;
		$data['cost'] = $this->Job->cost($job['id']);
		$data['charges'] = $this->Job->charges($job['id']);
		
		$this->load->model("Section");
		$this->load->model("Material");
		
		$this->load->view("header");
		$this->load->view("jobs/approve",$data);
		$this->load->view("footer");
	}

		public function approve_commit() {
			$job_id = $this->input->post("job_id");
			if (!is_numeric($job_id)):
				set_message("error","You did not specify a job.");
				redirect("jobs");
				die();
			endif;
			$this->load->model("Job");
			$job = $this->Job->get($job_id);
			if (empty($job)):
				set_message("error","That job could not be found.");
				redirect("jobs");
				die();
			endif;
			if (!has_access("show job",$job['id'])):
				set_message("error","You don't have permission to do that.");
				redirect("jobs");
				die();
			endif;
			if (empty($job['quoted_at'])):
				set_message("error","This job has not been quoted yet.");
				redirect("jobs/show/".$job['id']);
				die();		
			endif;
			$agree = $this->input->post("agree");
			if (empty($agree)):
				set_message("error","You must select 'agree' to approve the quote.");
				redirect("jobs/approve/".$job['id']);
				die();		
			endif;
			if (!empty($job['printed_at'])):
				set_message("error","This job has already been printed.");
				redirect("jobs/show/".$job['id']);
				die();		
			endif;

			// update the database
			$row = array(
				"stage" => 5,
				"approved_at" => date("Y-m-d H:i:s"),
				"approved_by" => me()
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			set_message("success","Job has been approved.");
		
			// send the email
			$this->load->model("Email");
			$result = $this->Email->send($job['id'],"Approved");
			if (empty($result)):
				set_message("warning","Email failed to send; staff will follow up directly.");
			else:
				set_message("success","Email notice sent to ".$result['recipient']);
			endif;
		
			redirect("jobs/show/".$job['id']);
		}

	public function rescind($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (!empty($job['printed_at'])):
			set_message("error","This job has already been printed.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;

		// update the database
		$row = array(
			"stage" => 4,
			"approved_at" => null,
			"approved_by" => null
		);
		$this->db->where("id",$job['id']);
		$this->db->limit(1);
		$this->db->update("jobs",$row);
		set_message("success","Job approval has been rescinded.");
	
		redirect("jobs/show/".$job['id']);
	}
	
	// mark a job as printed
	public function printed($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;

		// mark as printed
		if (empty($job['printed_at'])):
			// update the database
			$row = array(
				"stage" => 6,
				"printed_at" => date("Y-m-d H:i:s"),
				"printed_by" => me()
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			set_message("success","Job marked as printed");
		
			// send the email
			$this->load->model("Email");
			$result = $this->Email->send($job['id'],"Printed");
			if (empty($result)):
				set_message("warning","Email failed to send to clients; try again or contact them directly.");
			else:
				set_message("success","Email notice sent to ".$result['recipient']);
			endif;
			
			// check for a pre-approved invoice job
			if ($job['invoiced']):
				// update the job (paid)
				$row = array(
					"stage" => 7,
					"paid_by" => $job['created_by'],
					"paid_at" => date("Y-m-d H:i:s"),
					"updated_at" => date("Y-m-d H:i:s")
				);
				$this->db->where("id",$job['id']);
				$this->db->limit(1);
				$this->db->update("jobs",$row);
			
				// add the payment
				$row = array(
					"job_id" => $job['id'],
					"amount" => $this->Job->cost($job['id']),
					"type" => "invoice",
					"created_by" => $job['created_by'],
					"created_at" => date("Y-m-d H:i:s")
				);
				$this->db->insert("payments",$row);
				$payment_id = $this->db->insert_id();
	
				// set reference ID to be the payment ID - might change this later
				$this->db->where("id",$payment_id);
				$this->db->limit(1);
				$this->db->update("payments",array("reference_id"=>$payment_id));
		
				set_message("success","Payment added to invoice (prepaid) for job #${job['id']}, ".$this->Job->name($job['id']));
		
				// send the email
				$this->load->model("Email");
				$result = $this->Email->send($job['id'],"Paid");
			
			endif;
			
		// return to incomplete
		else:
			// update the database
			$row = array(
				"stage" => 5,
				"printed_at" => null,
				"printed_by" => null
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			set_message("success","Job marked as in progress");		
		endif;
		
		redirect("jobs/show/".$job['id']);
	}

	public function pay($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['approved_at'])):
			set_message("error","You cannot pay for a job that is awaiting approval.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;
		
		$data['job'] = $job;
		$data['cost'] = $this->Job->cost($job['id']);
		$data['charges'] = $this->Job->charges($job['id']);
		$data['payments'] = $this->Job->payments($job['id']);
		
		$this->load->model("Section");
		$this->load->model("Material");
		$this->load->model("Payment");
		$this->load->model("User");
		
		$this->load->view("header");
		$this->load->view("payments/add",$data);
		$this->load->view("footer");
	}
	
	public function receive($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['printed_at'])):
			set_message("error","Your job must be printed before marking it received.");
			redirect("jobs/show/".$job['id']);
			die();
		endif;
		
		if (empty($job['received_at'])):
			// update the database
			$row = array(
				"received_by" => me(),
				"received_at" => date("Y-m-d H:i:s"),
				"status" => "Archived",
				"stage" => 8
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			set_message("success","Job marked as received and moved to archive.");
		
			// send the email
			$this->load->model("Email");
			$result = $this->Email->send($job['id'],"Received");
			if (empty($result)):
				set_message("warning","Email failed to send to clients; try again or contact them directly.");
			else:
				set_message("success","Email notice sent to ".$result['recipient']);
			endif;
			
		// return active, unreceived
		else:
			// update the database
			$row = array(
				"received_by" => null,
				"received_at" => null,
				"status" => "Active",
				"stage" => 7
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			set_message("success","Job marked as unreceived.");
		endif;
		
		redirect("jobs/show/".$job['id']);
	}
	
	public function cancel($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;	
		
		if ($job['status']=="Archived"):
			set_message("error","You cannot cancel an archived job.");
			redirect("jobs/show/".$job['id']);
			die();		
		elseif ($job['status']=="Active"):
			// update the database
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",array("status"=>"Cancelled"));
			set_message("warning","Job has been cancelled.");

			// send the email
			$this->load->model("Email");
			$result = $this->Email->send($job['id'],"Cancelled");
			if (empty($result)):
				set_message("warning","Email failed to send to clients; try again or contact them directly.");
			else:
				set_message("success","Email notice sent to ".$result['recipient']);
			endif;
		// return to active
		elseif ($job['status']=="Cancelled"):
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",array("status"=>"Active"));
			set_message("success","Job returned to active.");
		endif;
		
		redirect("jobs/show/".$job['id']);
	}

	public function json($job_id=FALSE) {
		if (!is_numeric($job_id))
			die("You did not specify a job.");
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job))
			die("That job could not be found.");
		
		$data['array'] = $job;
		$this->load->view("json",$data);
	}

}