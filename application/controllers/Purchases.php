<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases extends CI_Controller {

	public function add() {
		$this->load->model("User");
		$this->load->model("Method");
		$this->load->model("Material");
		
		$method = $this->Method->get_by_name("Materials Purchase");
		$data['materials'] = $this->Method->materials($method['id']);
		
		$this->load->view("header");
		$this->load->view("purchases/add",$data);
		$this->load->view("footer");	
	}
		public function add_commit() {
			$this->load->model("User");
			$this->load->model("Job");
			
			// look up user first
			$netid = $this->input->post("netid");
			if (empty($netid)):
				set_message("error","You must enter a valid NetID.");
				redirect("purchases/add");
			endif;
			$user_id = $this->User->get_by_netid($netid);
			if (!is_numeric($user_id)):
				set_message("error","Unable to match NetID to a user: ".$netid);
				redirect("purchases/add");
			endif;
			
			// start the row
			$job = array(
				"source" => "Purchase",
				"status" => "Purchased",
				"stage" => 6,
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s"),
				"quoted_by" => me(),
				"quoted_at" => date("Y-m-d H:i:s"),
				"approved_by" => me(),
				"approved_at" => date("Y-m-d H:i:s"),
			);
						
			// load post data
			foreach (array("name","instructions","material_id","quote") as $key)
				$job[$key] = $this->input->post($key);
			
			if (!is_numeric($job['quote'])):
				set_message("error","Invalid charge amount submitted: ".$job['quote']);
				redirect("purchases/add");
			endif;

			// create the job
			$this->db->insert("jobs",$job);
			$job_id = $this->db->insert_id();
			set_message("success","New purchase created successfully. Reference #".$job_id);
			$job['id'] = $job_id;
			
			// associate user
			$row = array(
				"job_id" => $job_id,
				"user_id" => $user_id,
				"email" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$this->db->insert("jobs_users",$row);
			
			// add charge
			$row = array(
				"job_id" => $job_id,
				"name" => $job['name'],
				"amount" => $job['quote'],
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s")
			);
			$this->db->insert("charges",$row);
			
			$this->load->model("Email");
			$result = $this->Email->send($job_id, "Purchase");
			if (empty($result)):
				set_message("warning","Email failed to send; please follow up directly.");
			else:
				set_message("success","Email notice sent to ".$result['recipient']);
			endif;		
			
			redirect("purchases/add");
		}

}