<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends CI_Controller {
	
	public function add_commit() {
		auth();
		$job_id = $this->input->post("job_id");
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("User");
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
	
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		
		$row = array();
		$row['job_id'] = $job['id'];
		$row['created_by'] = me();
		$row['created_at'] = date("Y-m-d H:i:s");
		$row['content'] = $this->input->post("content");
		$row['internal'] = $this->input->post("internal")? 1:0;
		$this->db->insert("comments",$row);

		// email the listserv (and user, if necessary)
		$system_email = $this->Setting->get("System email");
		
		$content = "JOB".$job['id']." has been been updated by ".$this->User->name(me())."\r\n";
		$content = "The following comment was added:"."\r\n";
		$content .= $this->input->post("content")."\r\n";
		$content .= "\r\n"."View the request here:"."\r\n";
		$content .= site_url("jobs/show/".$job['id'])."\r\n";
		
		$this->load->model('Email');
		$this->Email->send($job['id'], "Commented");
	
		set_message("success","Comment added.");
		redirect("jobs/show/".$job['id']);
	}

}