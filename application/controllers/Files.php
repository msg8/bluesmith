<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends CI_Controller {
	
	public function index() {
		$this->load->model("User");
		$this->load->model("File");
		
		$data['files'] = $this->User->files(me());
		
		$this->load->view("header");
		$this->load->view("files/index",$data);
		$this->load->view("footer");
	}
		public function edit_commit() {
			$file_id = $this->input->post("file_id");
			if (!is_numeric($file_id)):
				set_message("error","You did not specify a file.");
				redirect("files");
				die();
			endif;
			$this->load->model("File");
			$file = $this->File->get($file_id);
			if (empty($file)):
				set_message("error","That file could not be found.");
				redirect("files");
				die();
			endif;
			if ($file['created_by']!=me() && !has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("files");
				die();
			endif;

			$name = $this->input->post("name");
			if (empty($name)):
				set_message("error","Name cannot be blank.");
				redirect("files");
				die();
			endif;
			
			$this->db->where("id",$file['id']);
			$this->db->limit(1);
			$this->db->update("files",array("name"=>$name));

			set_message("success","File renamed to '${name}'.");
			redirect("files");
		}
	
	public function add($source="browse") {
		$data['source'] = $source;
		$this->load->view("header");
		$this->load->view("files/add",$data);
		$this->load->view("footer");
	}
				
		public function add_commit() {
			$source = $this->input->post("source");
			
			$names = $this->input->post("names");
			if (empty($names)):
				set_message("error","You must include at least one file and name to upload.");
				redirect("files/add");
				die();
			endif;
			
			// upload files
			if (is_array($_FILES["filenames"]["name"])):
				$this->load->model("File");
				$config = array();
				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT']."/assets/files/";
				$config['encrypt_name'] = true;
				$config['allowed_types'] = '*';
				$this->load->library('upload');
				$files = $_FILES['filenames'];

				$row = array(
					"created_by" => me(),
					"created_at" => date("Y-m-d H:i:s"),
					"status" => "Active"
				);
				$tally = 0;
				foreach ($files["name"] as $i=>$name):
					unset($_FILES);
					if (strpos($name,".php")!==FALSE):
						set_message("error","Disallowed filetype: ${name}");
					elseif (is_uploaded_file($files["tmp_name"][$i])):
						$_FILES['myfile']['name'] = $files['name'][$i];
						$_FILES['myfile']['type'] = $files['type'][$i];
						$_FILES['myfile']['tmp_name'] = $files['tmp_name'][$i];
						$_FILES['myfile']['error'] = $files['error'][$i];
						$_FILES['myfile']['size'] = $files['size'][$i];

						$this->upload->initialize($config);
						if (!$this->upload->do_upload("myfile")):
							set_message("error",$this->upload->display_errors('',''));
						else:
							// file uploaded; create the DB entry
							$upload = $this->upload->data();
							
							if (empty($names[$i])):
								$row['name'] = $upload['client_name'];
							else:
								$row['name'] = $names[$i];
							endif;
							$row['clientname'] = $upload['client_name'];
							$row['filename'] = $upload['file_name'];
							$row['filetype'] = $upload['file_type'];
							$row['filesize'] = $upload['file_size'];
							$row['created_at'] = date("Y-m-d H:i:s");
							$this->db->insert("files",$row);
							
							$tally++;
						endif;
					endif;
				endforeach;
			else:
				set_message("error","File upload failed: no data.");
				redirect("files/add");
				die();				
			endif;
			
			$s = ($tally==1)? "":"s";
			set_message("success","Uploaded ${tally} file${s}.");
			redirect("files");
		}

		public function remove($file_id) {
			if (!is_numeric($file_id)):
				set_message("error","You did not specify a file.");
				redirect("files");
				die();
			endif;
			$this->load->model("File");
			$file = $this->File->get($file_id);
			if (empty($file)):
				set_message("error","That file could not be found.");
				redirect("files");
				die();
			endif;
			if ($file['created_by']!=me() && !has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("files");
				die();
			endif;

			$this->db->where("id",$file['id']);
			$this->db->limit(1);
			$this->db->update("files",array("status"=>"Archived"));

			set_message("warning","File '".$file['name']."' removed.");
			redirect("files");
		}

	public function preview($file_id) {
		if (!is_numeric($file_id)):
			set_message("error","You did not specify a file.");
			redirect("files");
			die();
		endif;
		$this->load->model("File");
		$file = $this->File->get($file_id);
		if (empty($file)):
			set_message("error","That file could not be found.");
			redirect("files");
			die();
		endif;
		if ($file['created_by']!=me() && !has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("files");
			die();
		endif;
		$extension = explode(".",$file['clientname']);
		$extension = end($extension);
		if (strtolower($extension)!="stl"):
			set_message("error","Cannot view files of that type.");
			redirect("files");
			die();
		endif;
		$data['file'] = $file;
		
		$this->load->view("files/preview",$data);		
	}
		
	public function download($file_id=FALSE) {
		if (!is_numeric($file_id)):
			set_message("error","You did not specify a file.");
			redirect("sections");
			die();
		endif;
		$this->load->model("File");
		$file = $this->File->get($file_id);
		if (empty($file)):
			set_message("error","That file could not be found.");
			redirect("sections");
			die();
		endif;
		if (!has_access("download file",$file['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		
		// load file
		$path = "assets/files/".$file['filename'];
		if (file_exists($path)):
			// log it
			$row = array();
			$row["file_id"] = $file['id'];
			$row["created_by"] = me();
			$row["created_at"] = date("Y-m-d H:i:s");
			$this->db->insert("downloads",$row);

			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$file['clientname'].'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path));
			readfile($path);
			exit;
		else:
			die("File doesn't exist!");
		endif;
	}
}