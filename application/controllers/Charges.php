<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Charges extends CI_Controller {

	public function job($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You must select a job to add charges to.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","Could not find that job.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs/show/".$job['id']);
			die();
		endif;
		$data["job"] = $job;
		
		$this->load->model("Charge");
		$this->load->model("Method");
		$this->load->model("Material");
		
		$data['charges'] = $this->Job->charges($job['id']);
		$data['material'] = $this->Material->get($job['material_id']);
		$data['method'] = $this->Method->get($data['material']['method_id']);
		
		$this->load->view("header");
		$this->load->view("charges/job",$data);
		$this->load->view("footer");
	}

		public function add_commit() {
			$job_id = $this->input->post("job_id");
			if (!is_numeric($job_id)):
				set_message("error","You must select a job to add charges to.");
				redirect("jobs");
				die();
			endif;
			$this->load->model("Job");
			$job = $this->Job->get($job_id);
			if (empty($job)):
				set_message("error","Could not find that job.");
				redirect("jobs");
				die();
			endif;
			if (!has_access("proctor")):
				set_message("error","You don't have permission to do that.");
				redirect("jobs/show/".$job['id']);
				die();
			endif;
			$data["job"] = $job;
			
			$names = $this->input->post("names");
			$amounts = $this->input->post("amounts");

			$row = array(
				"job_id" => $this->input->post("job_id"),
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s")
			);
			$tally = 0;
			foreach ($names as $i=>$name):
				$row['name'] = $names[$i];
				$row['amount'] = $amounts[$i];
				if (empty($row['name'])):
					set_message("warning","You didn't supply a description for charge #".($i+1)." - skipping");
				elseif (empty($row['amount'])):
					set_message("warning","You didn't supply an amount for charge #".($i+1)." - skipping");
				else:
					$this->db->insert("charges",$row);
					$tally++;
				endif;
			endforeach;
			
			if ($tally==1):
				set_message("success","Added new charge for '".$row['name']."'");
			elseif ($tally>1):
				set_message("success","Added ${tally} new charges");
			endif;
			
			// set the job to stage 3
			$row = array(
				"stage" => 3,
				"updated_at" => date("Y-m-d H:i:s")
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			
			if ($this->input->post("email")):
				redirect("jobs/quote/".$job['id']);
			else:
				redirect("jobs/show/".$job['id']);			
			endif;
		}

	public function remove($charge_id) {
		if (!has_access("proctor")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		if (!is_numeric($charge_id)):
			set_message("error","You must specify a charge.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Charge");
		$charge = $this->Charge->get($charge_id);
		if (empty($charge)):
			set_message("error","Could not find that charge.");
			redirect("jobs");
			die();
		endif;
		
		$this->db->where("id",$charge['id']);
		$this->db->limit(1);
		$this->db->delete("charges");
		set_message("success","Removed charge for '".$charge['name']."'");
		redirect("charges/job/".$charge['job_id']);
	}

}