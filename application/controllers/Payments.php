<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {

	// test card = 4111 1111 1111 1111
	public function creditcard($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['approved_at'])):
			set_message("error","You cannot pay for a job that has not been approval.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;
		
		// load user info
		$this->load->model("User");
		$user = $this->User->get(me());
		
		// create a new payment
		$cost = $this->Job->cost($job['id']);
		$row = array(
			"job_id" => $job['id'],
			"amount" => $cost,
			"type" => "creditcard",
			"created_by" => me(),
			"created_at" => date("Y-m-d H:i:s")
		);
		$this->db->insert("payments",$row);
		$payment_id = $this->db->insert_id();
		
		// set reference ID to be the payment ID - might change this later
		$this->db->where("id",$payment_id);
		$this->db->limit(1);
		$this->db->update("payments",array("reference_id"=>$payment_id));
		
		// build paramaters for CyberSource
		$this->load->helper("cybersource");
		$data['signed'] = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_email,bill_to_forename,bill_to_surname,merchant_defined_data5,item_0_unit_price,item_0_quantity,item_0_name";
		$data['params'] = array(
			"access_key" => cyber_access_key(),
			"profile_id" => cyber_profile_id(),
			"transaction_uuid" => uniqid(),
			"signed_field_names" => $data['signed'],
			"unsigned_field_names" => "",
			"signed_date_time" => gmdate("Y-m-d\TH:i:s\Z"),
			"locale" => "en-US",
			"transaction_type" => "sale",
			"reference_number" => "Bluesmith Payment ".$payment_id,
			"amount" => $cost,
			"currency" => "USD",
			"bill_to_email" => $user['email'],
			"bill_to_forename" => $user['firstname'],
			"bill_to_surname" => $user['lastname'],
			"merchant_defined_data5" => "Bluesmith creditcard payment",
			"item_0_unit_price" => $cost,
			"item_0_quantity" => 1,
			"item_0_name" => $job['name']
		);
		
		$this->load->view("header");
		$this->load->view("payments/creditcard",$data);
		$this->load->view("footer");		
	}
	
		public function creditcard_commit() {
			$data = $this->input->post();
		
			// get the payment
			$this->load->model("Payment");
			$payment_id = $this->input->post("req_reference_number");
			// trim off branding
			$payment_id = explode(" ",$payment_id);
			$payment_id = end($payment_id);
			
			$payment = $this->Payment->get($payment_id);
			if (empty($payment)):
				$from = $this->Setting->get("System email");
				$prefix = $this->Setting->get("System prefix");
				$content = "CyberSource response received for unidentifiable payment:"."\r\n\r\n";
				$content .= print_r($data,true);
				$this->load->library('email');
				$this->email->from($from);
				$this->email->to("matt.gatner@duke.edu");
				$this->email->subject($prefix."CyberSource error");
				$this->email->message($content);
				$result = $this->email->send();
				return;
			endif;
		
			// load job info
			$this->load->model("Job");
			$job = $this->Job->get($payment['job_id']);

			// DEBUG send email
			$from = $this->Setting->get("System email");
			$prefix = $this->Setting->get("System prefix");
			$content = "Payment response from CyberSource"."\r\n\r\n";
			$content .= "==== JOB ===="."\r\n";
			$content .= print_r($job,true);
			$content .= "==== PAYMENT ===="."\r\n";
			$content .= print_r($payment,true);
			$content .= "==== POST DATA ===="."\r\n";
			$content .= print_r($data,true);
			$this->load->library('email');
			$this->email->from($from);
			$this->email->to("admin@company.org");
			$this->email->subject($prefix."CyberSource response");
			$this->email->message($content);
			$result = $this->email->send();

			$decision = $data["decision"]; // ACCEPT, DECLINE, or CANCEL
		
			// record the response
			$row = array(
				"job_id" => $job['id'],
				"payment_id" => $payment['id'],
				"status" => $decision,
				"content" => $this->input->post("message")
			);
			$this->db->insert("responses",$row);

			$this->load->model("Email");
			// if payment was accepted, mark the payment as processed and notify client
			if ($decision=="ACCEPT"):
				// update the payment
				$this->db->where("id",$payment['id']);
				$this->db->limit(1);
				$this->db->update("payments",array("processed_at"=>date("Y-m-d H:i:s")));
				
				// update the job
				$row = array(
					"stage" => 7,
					"paid_by" => $payment['created_by'],
					"paid_at" => date("Y-m-d H:i:s"),
					"updated_at" => date("Y-m-d H:i:s")
				);
				$this->db->where("id",$job['id']);
				$this->db->limit(1);
				$this->db->update("jobs",$row);
				
				// notify clients
				$this->Email->send($job['id'],"Paid");
				
			// payment was not accepted - remove payment and notify user if necessary
			else:
				$this->db->where("id",$payment['id']);
				$this->db->limit(1);
				$this->db->delete("payments");
				
				if ($decision=="DECLINE"):
					$this->Email->send($job['id'],"Declined");
				endif;
			endif;
		}
		
		public function creditcard_cancel() {
			$this->load->view("header");
			$this->load->view("payments/creditcard_cancel");
			$this->load->view("footer");
		}
		
		public function creditcard_return() {
			$this->load->view("header");
			$this->load->view("payments/creditcard_return");
			$this->load->view("footer");
		}

	public function fundcode($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['approved_at'])):
			set_message("error","You cannot pay for a job that has not been approval.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;

		$data['job'] = $job;
		$data['cost'] = $this->Job->cost($job['id']);
		
		if ($code = $this->input->post("code")):
			$code = urlencode($code);
			$result = file_get_contents(FUNDCODE_API);
			if (empty($result)):
				set_message("warning","Unable to validate fund code - please try again later.");
			else:
				$result = json_decode($result,true);
				if ($result['status']!="success"):
					set_message("warning","Invalid fundcode submitted: ".$result['message']);
				else:
					$fundcode_id = $result['fundcode_id'];
					$fundcode = file_get_contents(FUNDCODE_API);
					$fundcode = json_decode($fundcode,true);
					$data['fundcode'] = $fundcode;
				endif;
			endif;
		endif;
		$data['code'] = $code;
		
		$this->load->view("header");
		$this->load->view("payments/fundcode",$data);
		$this->load->view("footer");
	}
	
		public function fundcode_commit() {
			$job_id = $this->input->post("job_id");
			if (!is_numeric($job_id)):
				set_message("error","You did not specify a job.");
				redirect("jobs");
				die();
			endif;
			$this->load->model("Job");
			$job = $this->Job->get($job_id);
			if (empty($job)):
				set_message("error","That job could not be found.");
				redirect("jobs");
				die();
			endif;
			if (!has_access("show job",$job['id'])):
				set_message("error","You don't have permission to do that.");
				redirect("jobs");
				die();
			endif;
			if (empty($job['approved_at'])):
				set_message("error","You cannot pay for a job that has not been approval.");
				redirect("jobs/show/".$job['id']);
				die();		
			endif;
			$cost = $this->Job->cost($job['id']);
			if (empty($cost) OR $cost<0):
				set_message("error","Cannot pay for a job with zero or negative cost.");
				redirect("jobs/show/".$job['id']);
				die();			
			endif;
			// validate data
			if (!($this->input->post("agree"))):
				set_message("warning","You must check 'agree' to use this fundcode.");
				redirect("payments/fundcode/".$job['id']);
				die();
			endif;
			
			// update the job
			$row = array(
				"stage" => 7,
				"paid_by" => me(),
				"paid_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s")
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			$data['job'] = $this->Job->get($job['id']);
			
			// add the payment
			$fundcode_id = $this->input->post("fundcode_id");
			$row = array(
				"job_id" => $job['id'],
				"amount" => $cost,
				"type" => "fundcode",
				"reference_id" => $fundcode_id,
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s")
			);
			$this->db->insert("payments",$row);
			set_message("success","Payment received for job #${job['id']}, ".$this->Job->name($job['id']));
			
			// send the email
			$this->load->model("Email");
			$result = $this->Email->send($job['id'],"Paid");
			if (empty($result)):
				set_message("warning","Email failed to send; staff will follow up directly.");
			else:
				set_message("success","Email notice sent to ".$result['recipient']);
			endif;
			
			$this->load->view("header");
			$this->load->view("payments/fundcode_commit",$data);
			$this->load->view("footer");
		}

	public function bluechips($job_id=FALSE) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['approved_at'])):
			set_message("error","You cannot pay for a job that has not been approval.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;

		$data['job'] = $job;
		$data['cost'] = $this->Job->cost($job['id']);
		$data['balance'] = $this->User->balance(me());
		
		$this->load->view("header");
		$this->load->view("payments/bluechips",$data);
		$this->load->view("footer");
	}
		public function bluechips_commit() {
			$job_id = $this->input->post("job_id");
			if (!is_numeric($job_id)):
				set_message("error","You did not specify a job.");
				redirect("jobs");
				die();
			endif;
			$this->load->model("Job");
			$job = $this->Job->get($job_id);
			if (empty($job)):
				set_message("error","That job could not be found.");
				redirect("jobs");
				die();
			endif;
			if (!has_access("show job",$job['id'])):
				set_message("error","You don't have permission to do that.");
				redirect("jobs");
				die();
			endif;
			if (empty($job['approved_at'])):
				set_message("error","You cannot pay for a job that has not been approval.");
				redirect("jobs/show/".$job['id']);
				die();		
			endif;
			$cost = $this->Job->cost($job['id']);
			if (empty($cost) OR $cost<0):
				set_message("error","Cannot pay for a job with zero or negative cost.");
				redirect("jobs/show/".$job['id']);
				die();			
			endif;
			// validate data
			if (!($this->input->post("agree"))):
				set_message("warning","You must check 'agree' to use your Bluechips account.");
				redirect("payments/bluechips/".$job['id']);
				die();
			endif;
			$data['job'] = $job;
			
			// load user info
			$this->load->model("User");
			$user = $this->User->get(me());
			$data['user'] = $user;
			$balance = $this->User->balance($user['id']);
			
			// check for sufficient funds
			if ($balance<$cost):
				set_message("error","Insufficient funds available in Bluechips allocation.");
				redirect("payments/bluechips/".$job['id']);
			endif;
			
			// update the job
			$row = array(
				"stage" => 7,
				"paid_by" => me(),
				"paid_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s")
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
			
			
			// add the payment
			$row = array(
				"job_id" => $job['id'],
				"amount" => $cost,
				"type" => "bluechips",
				"processed_at" => date("Y-m-d H:i:s"),
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s")
			);
			$this->db->insert("payments",$row);
			$payment_id = $this->db->insert_id();
		
			// set reference ID to be the payment ID - might change this later
			$this->db->where("id",$payment_id);
			$this->db->limit(1);
			$this->db->update("payments",array("reference_id"=>$payment_id));
		
			// update user balance
			$balance = $balance-$cost;
			$this->db->where("id",me());
			$this->db->limit(1);
			$this->db->update("users",array("balance"=>$balance));
			$data['balance'] = $balance;
			
			set_message("success","Payment received for job #${job['id']}, ".$this->Job->name($job['id']));
			
			// send the email
			$this->load->model("Email");
			$result = $this->Email->send($job['id'],"Paid");
			if (empty($result)):
				set_message("warning","Email failed to send; staff will follow up directly.");
			else:
				set_message("success","Email notice sent to ".$result['recipient']);
			endif;
			
			$this->load->view("header");
			$this->load->view("payments/bluechips_commit",$data);
			$this->load->view("footer");
		}
		
	public function invoice($job_id) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("invoice")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['approved_at'])):
			set_message("error","You cannot pay for a job that has not been approval.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;
		$cost = $this->Job->cost($job['id']);
		
		// update the job
		$row = array(
			"stage" => 7,
			"paid_by" => me(),
			"paid_at" => date("Y-m-d H:i:s"),
			"updated_at" => date("Y-m-d H:i:s")
		);
		$this->db->where("id",$job['id']);
		$this->db->limit(1);
		$this->db->update("jobs",$row);
		
		
		// add the payment
		$row = array(
			"job_id" => $job['id'],
			"amount" => $cost,
			"type" => "invoice",
			"created_by" => me(),
			"created_at" => date("Y-m-d H:i:s")
		);
		$this->db->insert("payments",$row);
		$payment_id = $this->db->insert_id();
	
		// set reference ID to be the payment ID - might change this later
		$this->db->where("id",$payment_id);
		$this->db->limit(1);
		$this->db->update("payments",array("reference_id"=>$payment_id));
		
		set_message("success","Payment added to invoice for job #${job['id']}, ".$this->Job->name($job['id']));
		
		// send the email
		$this->load->model("Email");
		$result = $this->Email->send($job['id'],"Paid");
		if (empty($result)):
			set_message("warning","Email failed to send; staff will follow up directly.");
		else:
			set_message("success","Email notice sent to ".$result['recipient']);
		endif;
		
		redirect("jobs/show/".$job['id']);
	}

	public function manual($job_id) {
		if (!is_numeric($job_id)):
			set_message("error","You did not specify a job.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($job_id);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		if (empty($job['approved_at'])):
			set_message("error","You cannot pay for a job that has not been approval.");
			redirect("jobs/show/".$job['id']);
			die();		
		endif;
		$cost = $this->Job->cost($job['id']);
		
		// update the job
		$row = array(
			"stage" => 7,
			"paid_by" => me(),
			"paid_at" => date("Y-m-d H:i:s"),
			"updated_at" => date("Y-m-d H:i:s")
		);
		$this->db->where("id",$job['id']);
		$this->db->limit(1);
		$this->db->update("jobs",$row);
		
		
		// add the payment
		$row = array(
			"job_id" => $job['id'],
			"amount" => $cost,
			"type" => "manual",
			"processed_at" => date("Y-m-d H:i:s"),
			"created_by" => me(),
			"created_at" => date("Y-m-d H:i:s")
		);
		$this->db->insert("payments",$row);
		$payment_id = $this->db->insert_id();
	
		// set reference ID to be the payment ID - might change this later
		$this->db->where("id",$payment_id);
		$this->db->limit(1);
		$this->db->update("payments",array("reference_id"=>$payment_id));
		
		set_message("success","Payment skipped for job #${job['id']}, ".$this->Job->name($job['id']));
		
		// send the email
		$this->load->model("Email");
		$result = $this->Email->send($job['id'],"Paid");
		if (empty($result)):
			set_message("warning","Email failed to send; staff will follow up directly.");
		else:
			set_message("success","Email notice sent to ".$result['recipient']);
		endif;
		
		redirect("jobs/show/".$job['id']);
	}
				
	public function remove($payment_id=FALSE) {
		if (!is_numeric($payment_id)):
			set_message("error","You did not specify a payment.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Payment");
		$payment = $this->Payment->get($payment_id);
		if (empty($payment)):
			set_message("error","That payment could not be found.");
			redirect("jobs");
			die();
		endif;
		$this->load->model("Job");
		$job = $this->Job->get($payment['job_id']);
		if (empty($job)):
			set_message("error","That job could not be found.");
			redirect("jobs");
			die();
		endif;
		if (!has_access("show job",$job['id'])):
			set_message("error","You don't have permission to do that.");
			redirect("jobs");
			die();
		endif;
		
		// remove the payment
		$this->db->where("id",$payment['id']);
		$this->db->limit(1);
		$this->db->delete("payments");
		
		// if there are no other payments, set the job as unpaid
		$payments = $this->Job->payments($job['id']);
		if (empty($payments)):
			// update the job
			$row = array(
				"paid_by" => null,
				"paid_at" => null
			);
			$this->db->where("id",$job['id']);
			$this->db->limit(1);
			$this->db->update("jobs",$row);
		endif;

		set_message("warning","Payment canceled for job #${job['id']}, ".$this->Job->name($job['id']));
		
		redirect("jobs/pay/".$job['id']);
	}
		
}