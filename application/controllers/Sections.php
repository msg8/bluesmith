<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sections extends CI_Controller {

	public function index() {
		$this->load->model("Section");
		
		$this->load->view("header");
		$this->load->view("sections/overview");
		$this->load->view("footer");
	}
	
	public function options() {
		$this->load->model("Section");
		$this->load->model("Method");
		$this->load->model("Material");
		
		$data['methods'] = $this->Method->all();
		
		$this->load->view("header");
		$this->load->view("sections/options",$data);
		$this->load->view("footer");
	}
	
	public function cms($tab="methods") {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		
		$this->load->model("Method");
		$this->load->model("Material");
		$this->load->model("Section");
		$this->load->model("Email");
		
		$data['methods'] = $this->Method->all();
		$data['materials'] = $this->Material->all();
		$data['emails'] = $this->Email->all();
		
		// set active tab
		switch ($tab):
			case "Methods": $data['tab'] = 0; break;
			case "Materials": $data['tab'] = 1; break;
			case "Sections": $data['tab'] = 2; break;
			case "Emails": $data['tab'] = 3; break;
			default: $data['tab'] = 0;
		endswitch;
		
		$this->load->view("header");
		$this->load->view("sections/cms",$data);
		$this->load->view("footer");
	}

		public function edit_commit() {
			if (!has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("sections");
				die();
			endif;
			$section_id = $this->input->post("section_id");
			if (!is_numeric($section_id)):
				set_message("error","You must specify a section.");
				redirect("sections/cms/Sections");
				die();
			endif;
			$this->load->model("Section");
			$section = $this->Section->get($section_id);
			if (empty($section)):
				set_message("error","Could not find that section.");
				redirect("sections/cms/Sections");
				die();
			endif;
					
			$row = array(
				"content" => $this->input->post("content"),
				"updated_by" => me()
			);
		
			$this->db->where("id",$section['id']);
			$this->db->limit(1);
			$this->db->update("sections",$row);
			set_message("success","Updated content for ".$this->Section->name($section['id']));
			redirect("sections/cms/Sections");
		}
		
	public function stylesheet() {
		$this->load->model("Section");
		header("Content-type: text/css; charset: UTF-8");
		echo $this->Section->content("Stylesheet");
	}
	
}
