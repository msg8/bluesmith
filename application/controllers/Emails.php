<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails extends CI_Controller {

	public function edit_commit() {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		$email_id = $this->input->post("email_id");
		if (!is_numeric($email_id)):
			set_message("error","You must specify an email.");
			redirect("sections/cms/Emails");
			die();
		endif;
		$this->load->model("Email");
		$email = $this->Email->get($email_id);
		if (empty($email)):
			set_message("error","Could not find that email.");
			redirect("sections/cms/Emails");
			die();
		endif;
				
		$row = array(
			"target" => $this->input->post("target"),
			"subject" => $this->input->post("subject"),
			"content" => $this->input->post("content"),
			"updated_by" => me()
		);
	
		$this->db->where("id",$email['id']);
		$this->db->limit(1);
		$this->db->update("emails",$row);
		set_message("success","Updated email for ".$this->Email->name($email['id']));
		redirect("sections/cms/Emails");
	}
	
}