<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends CI_Controller {
	public function cron() {
		if (!has_access("run task"))
			die("unauthorized");
		ini_set('memory_limit', '512M');
		
		$this->reminders();

		// run user update check every hour
		if (date("i")==5):
			$this->users();
		endif;
		
		// process fundcode JV the 25th of each month (7-7:04am)
		if (date("j")==25 && date("G")==7 && date("i")<5):
			$this->fundcodes_jv();
		endif;
		
		// process Med Center invoice JV the 25th of each month (8-8:04am)
		if (date("j")==25 && date("G")==8 && date("i")<5):
			$this->invoices_jv();
		endif;
		
		// check for Med Center payments to reconcile the 15th of each month (8-8:04am)
		if (date("j")==15 && date("G")==8 && date("i")<5):
			$this->reconcile();
		endif;				
	}
	
	public function reminders() {
		$threshhold = strtotime("-3 days");
		
		$this->load->model("Job");
		$this->load->model("Email");
		
		// get active jobs that haven't been updated since $threshhold
		$this->db->select("id");
		$this->db->from("jobs");
		$this->db->where("status","Active");
		$this->db->where("updated_at <",date("Y-m-d H:i:s",$threshhold));
		$query = $this->db->get();
		$result = $query->result_array();
		foreach ($result as $row):
			$job_id = $row['id'];
			
			// make sure there haven't been any emails since the job was updated (e.g. aleady reminded)
			$this->db->select("id");
			$this->db->from("emails_jobs");
			$this->db->where("job_id",$job_id);
			$this->db->where("created_at >=",date("Y-m-d H:i:s",$threshhold));
			$query = $this->db->get();
			$result = $query->result_array();
			if (empty($result)):
				// send reminder email
				$this->Email->send($job_id,"Reminder");
			endif;
		endforeach;
	}
		
	// call site-specific user import script
	function users() {
		$this->load->model("Company");
		$this->Company->users_import();
	}

	// mark any unprocessed fundcode payments as processed (generates JV email to Sammy for SAP)
	public function fundcodes_jv() {
		if (!has_access("run task"))
			die("unauthorized");

		// check for pending fundcode payments
		$this->db->select("*");
		$this->db->from("payments");
		$this->db->where("type","fundcode");
		$this->db->where("processed_at IS NULL");
		$this->db->order_by("created_at","desc");
		$query = $this->db->get();
		$result = $query->result_array();
		$payments = array();
		if (empty($result)):
			echo "There are no unprocessed fundcode payments".PHP_EOL;
		else:
			foreach ($result as $payment):
				// get job
				$job = $this->Job->get($payment['job_id']);

				// make sure job hasn't been canceled			
				if ($job['status']=="Cancelled"):
					echo "WARNING: unprocessed fundcode for cancelled job; payment #".$payment['id'];
					exit;
				else:
					$payments[] = $payment['id'];
				endif;
			endforeach;
			
			// update the database
			$stamp = time();
			$this->db->where_in("id",$payments);
			$this->db->update("payments",array("processed_at"=>date("Y-m-d H:i:s",$stamp)));
		endif;
				
		// send the email notice
		$this->load->library('email');
		$this->email->from("bluesmith@duke.edu");
		//$this->email->to("matt.gatner@duke.edu");
		$this->email->to("sammy.gitau@duke.edu");
		$this->email->cc("matt.gatner@duke.edu, chip.bobbert@duke.edu");
		$this->email->subject("[Bluesmith] Fundcode JV");
		
		if (count($payments)==1):
			$message = "One new fundcode payment marked as processed.".PHP_EOL;
			$message .= "Please use the following link to download the JV.".PHP_EOL;
			$message .= site_url("reports/fundcodes_jv/".$stamp);
		elseif (count($payments)>1):
			$message = count($payments)." new fundcode payments marked as processed.".PHP_EOL;
			$message .= "Please use the following link to download the JV.".PHP_EOL;
			$message .= site_url("reports/fundcodes_jv/".$stamp);
		else:
			$message = "There are no new fundcode payments available for a JV.";
		endif;

		$message .= PHP_EOL.base_url().PHP_EOL;
		
		$this->email->message($message);
		$result = $this->email->send();
	}

	// mark any unprocessed invoice payments as processed (generates JV email to Susan for SAP)
	public function invoices_jv() {
		if (!has_access("run task"))
			die("unauthorized");

		// check for pending invoice payments
		$this->db->select("*");
		$this->db->from("payments");
		$this->db->where("type","invoice");
		$this->db->where("processed_at IS NULL");
		$this->db->order_by("created_at","desc");
		$query = $this->db->get();
		$result = $query->result_array();
		$payments = array();
		if (empty($result)):
			echo "There are no unprocessed invoice payments".PHP_EOL;
		else:
			foreach ($result as $payment):
				// get job
				$job = $this->Job->get($payment['job_id']);

				// make sure job hasn't been canceled			
				if ($job['status']=="Cancelled"):
					echo "WARNING: unprocessed fundcode for cancelled job; payment #".$payment['id'];
					exit;
				else:
					$payments[] = $payment['id'];
				endif;
			endforeach;
			
			// update the database
			$stamp = time();
			$this->db->where_in("id",$payments);
			$this->db->update("payments",array("processed_at"=>date("Y-m-d H:i:s",$stamp)));
		endif;
				
		// send the email notice
		$this->load->library('email');
		$this->email->from("bluesmith@duke.edu");
		//$this->email->to("matt.gatner@duke.edu");
		$this->email->to("susan.churchill@duke.edu, mark.hemric@duke.edu");
		$this->email->cc("matt.gatner@duke.edu, chip.bobbert@duke.edu, sammy.gitau@duke.edu");
		$this->email->subject("[Bluesmith] Med Center JV");
		
		if (count($payments)==1):
			$message = "One new invoiced payment marked as processed.".PHP_EOL;
			$message .= "Please use the following link to download the JV.".PHP_EOL;
			$message .= site_url("reports/invoices_jv/".$stamp);
		elseif (count($payments)>1):
			$message = count($payments)." new invoiced payments marked as processed.".PHP_EOL;
			$message .= "Please use the following link to download the JV.".PHP_EOL;
			$message .= site_url("reports/invoices_jv/".$stamp);
		else:
			$message = "There are no new invoiced payments available for a JV.";
		endif;

		$message .= PHP_EOL.base_url().PHP_EOL;
		
		echo "Sending email: ".PHP_EOL;
		echo $message;
		
		$this->email->message($message);
		$result = $this->email->send();
	}

	// check for Med Center payments that haven't been reconciled; sends email notice if found
	public function reconcile() {
		if (!has_access("run task"))
			die("unauthorized");

		// check for unreconciled invoice payments
		$this->db->select("*");
		$this->db->from("payments");
		$this->db->where("type","invoice");
		$this->db->where("processed_at IS NOT NULL");
		$this->db->where("reconciled_at IS NULL");
		$this->db->order_by("created_at","desc");
		$query = $this->db->get();
		$payments = $query->result_array();

		// send the email notice
		$this->load->library('email');
		$this->email->from("bluesmith@duke.edu");
		//$this->email->to("matt.gatner@duke.edu");
		$this->email->to("sammy.gitau@duke.edu");
		$this->email->cc("matt.gatner@duke.edu, chip.bobbert@duke.edu, ");
		$this->email->subject("[Bluesmith] Med Center reconciliation");
		
		if (count($payments)==1):
			$message = "One payment awaits reconciliation.".PHP_EOL;
			$message .= "Please use the following link to update.".PHP_EOL;
		elseif (count($payments)>1):
			$message = count($payments)." unreconciled payments.".PHP_EOL;
			$message .= "Please use the following link to update.".PHP_EOL;
		else:
			$message = "All payments have been reconciled; no action required.";
		endif;

		$message .= site_url("payments/reconcile");
		
		$this->email->message($message);
		$result = $this->email->send();
	}
}
