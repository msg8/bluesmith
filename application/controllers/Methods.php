<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Methods extends CI_Controller {

	public function add() {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		
		$this->load->view("header");
		$this->load->view("methods/add");
		$this->load->view("footer");
	}

		public function add_commit() {
			if (!has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("sections");
				die();
			endif;
			
			$row = array(
				"name" => $this->input->post("name"),
				"fullname" => $this->input->post("fullname"),
				"description" => $this->input->post("description"),
				"status" => "Active",
				"sortorder" => $this->input->post("sortorder"),
				"created_by" => me(),
				"created_at" => date("Y-m-d H:i:s")
			);
			
			if (empty($row['name'])):
				set_message("error","You must supply a name.");
				redirect("methods/add");
				die();
			endif;
			
			$this->db->insert("methods",$row);
			set_message("success","Added new print method '".$row['name']."'");
			redirect("sections/cms/Methods");
		}

	public function edit($method_id=FALSE) {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		if (!is_numeric($method_id)):
			set_message("error","You must specify a method.");
			redirect("sections/cms/Methods");
			die();
		endif;
		$this->load->model("Method");
		$method = $this->Method->get($method_id);
		if (empty($method)):
			set_message("error","Could not find that method.");
			redirect("sections/cms/Methods");
			die();
		endif;
		$data['method'] = $method;
		
		$this->load->view("header");
		$this->load->view("methods/edit",$data);
		$this->load->view("footer");
	}

		public function edit_commit() {
			if (!has_access("administration")):
				set_message("error","You don't have permission to do that.");
				redirect("sections");
				die();
			endif;
			$method_id = $this->input->post("method_id");
			if (!is_numeric($method_id)):
				set_message("error","You must specify a method.");
				redirect("sections/cms/Methods");
				die();
			endif;
			$this->load->model("Method");
			$method = $this->Method->get($method_id);
			if (empty($method)):
				set_message("error","Could not find that method.");
				redirect("sections/cms/Methods");
				die();
			endif;
						
			$row = array(
				"name" => $this->input->post("name"),
				"fullname" => $this->input->post("fullname"),
				"description" => $this->input->post("description"),
				"status" => "Active",
				"sortorder" => $this->input->post("sortorder")
			);
			
			if (empty($row['name'])):
				set_message("error","You must supply a name.");
				redirect("methods/edit/".$method['id']);
				die();
			endif;
			
			$this->db->where("id",$method['id']);
			$this->db->limit(1);
			$this->db->update("methods",$row);
			set_message("success","Updated print method '".$row['name']."'");
			redirect("sections/cms/Methods");
		}
		
	public function remove($method_id) {
		if (!has_access("administration")):
			set_message("error","You don't have permission to do that.");
			redirect("sections");
			die();
		endif;
		if (!is_numeric($method_id)):
			set_message("error","You must specify a method.");
			redirect("sections/cms/Methods");
			die();
		endif;
		$this->load->model("Method");
		$method = $this->Method->get($method_id);
		if (empty($method)):
			set_message("error","Could not find that method.");
			redirect("sections/cms/Methods");
			die();
		endif;
					
		$this->db->where("id",$method['id']);
		$this->db->limit(1);
		$this->db->update("methods",array("status"=>"Archived"));
		set_message("success","Archived print method '".$method['name']."'");
		redirect("sections/cms/Methods");
	}
}